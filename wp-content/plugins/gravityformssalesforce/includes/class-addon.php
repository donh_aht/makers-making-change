<?php
/*
 * @package   GFP_Salesforce\GFP_Salesforce_Addon
 * @author    Naomi C. Bush for gravity+ <support@gravityplus.pro>
 * @copyright 2017 gravity+
 * @license   GPL-2.0+
 * @since     0.1
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Class GFP_Salesforce_Addon
 *
 * Gravity Forms Add-On
 *
 * @since  0.1
 *
 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
 */
class GFP_Salesforce_Addon extends GFFeedAddOn {

	/**
	 * @var string Version number of the Add-On
	 */
	protected $_version = GFP_SALESFORCE_CURRENT_VERSION;
	/**
	 * @var string Gravity Forms minimum version requirement
	 */
	protected $_min_gravityforms_version = '2.2';
	/**
	 * @var string URL-friendly identifier used for form settings, add-on settings, text domain localization...
	 */
	protected $_slug = GFP_SALESFORCE_SLUG;
	/**
	 * @var string Relative path to the plugin from the plugins folder
	 */
	protected $_path = GFP_SALESFORCE_PATH;
	/**
	 * @var string Full path to the plugin. Example: __FILE__
	 */
	protected $_full_path = GFP_SALESFORCE_FILE;
	/**
	 * @var string URL to the App website.
	 */
	protected $_url = 'https://gravityplus.pro/gravity-forms-salesforce';
	/**
	 * @var string Title of the plugin to be used on the settings page, form settings and plugins page.
	 */
	protected $_title = 'Gravity Forms Salesforce Add-On';
	/**
	 * @var string Short version of the plugin title to be used on menus and other places where a less verbose string
	 *      is useful.
	 */
	protected $_short_title = 'Salesforce';
	/**
	 * @var array Members plugin integration. List of capabilities to add to roles.
	 */
	protected $_capabilities = array(
		'gravityforms_salesforce_plugin_settings',
		'gravityforms_salesforce_form_settings',
		'gravityforms_salesforce_uninstall'
	);

	// ------------ Permissions -----------
	/**
	 * @var string|array A string or an array of capabilities or roles that have access to the settings page
	 */
	protected $_capabilities_settings_page = array( 'gravityforms_salesforce_plugin_settings' );

	/**
	 * @var string|array A string or an array of capabilities or roles that have access to the form settings
	 */
	protected $_capabilities_form_settings = array( 'gravityforms_salesforce_form_settings' );
	/**
	 * @var string|array A string or an array of capabilities or roles that can uninstall the plugin
	 */
	protected $_capabilities_uninstall = array( 'gravityforms_salesforce_uninstall' );

	protected $_supports_feed_ordering = true;

	private static $_instance = null;

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @var GFP_Salesforce_API | null
	 */
	protected $_gfp_salesforce_api = null;

	/**
	 * @since 0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @var bool
	 */
	private $_processing_feed = false;

	/**
	 * Array of objects that are created during feed processing
	 *
	 * @since 1.0.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @var null | array
	 */
	private $_created_objects = null;


	/**
	 * @return GFP_Salesforce_Addon|null
	 */
	public static function get_instance() {

		if ( self::$_instance == null ) {

			self::$_instance = new self();

		}

		return self::$_instance;

	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 */
	public function pre_init() {

		parent::pre_init();

		add_action( 'parse_request', array( $this, 'parse_request' ) );

	}

	/**
	 * @see    GFAddOn::init
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 */
	public function init() {

		parent::init();

		$this->add_delayed_payment_support(
			array(
				'option_label' => __( 'Take Salesforce action only when a payment is received.', 'gravityformssalesforce' )
			)
		);
		
		add_filter( 'gform_entry_detail_meta_boxes', array( $this, 'gform_entry_detail_meta_boxes' ), 10, 3 );

	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $previous_version
	 */
	public function upgrade( $previous_version ) {

		do_action( "gform_{$this->_slug}_upgrade", $previous_version, $this );


		return;

	}

	public function plugin_settings_icon() {
		return '<i class="fa fa-cloud"></i>';
	}

	public function form_settings_icon() {
		return '<i class="fa fa-cloud"></i>';
	}

	/**
	 * @since 1.1.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return GFP_Salesforce_API|null
	 */
	public function get_salesforce_api() {

		if ( empty( $this->_gfp_salesforce_api ) ) {

			$settings = $this->get_plugin_settings();

			$this->_gfp_salesforce_api = new GFP_Salesforce_API( $settings[ 'instance' ], $settings['sandbox'], $settings[ 'client_id' ], $settings[ 'client_secret' ], $this->get_callback_url(), empty( $settings[ 'oauth_data' ] ) ? array() : $settings[ 'oauth_data' ] );

		}


		return $this->_gfp_salesforce_api;

	}

	/**
	 * @see    GFAddOn::plugin_settings_fields
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return array
	 */
	public function plugin_settings_fields() {

		$settings_fields = array();

		$settings_fields[] = array(
			'title'       => __( 'Step 1: Instance', 'gravityformssalesforce' ),
			'fields'      => array(
				array(
					'name'                => 'instance',
					'label'               => __( 'Salesforce Instance Name', 'gravityformssalesforce' ),
					'description'         => __( 'Name of your Salesforce instance', 'gravityformssalesforce' ),
					'type'                => 'text',
					'placeholder'         => 'ns00',
					'required'            => true,
				),
				array(
					'name'                => 'sandbox_checkbox',
					'label'               => __( 'Sandbox?', 'gravityformssalesforce' ),
					'description'         => __( 'Is this a sandbox instance?', 'gravityformssalesforce' ),
					'type'                => 'checkbox',
					'choices' => array( array(
						'name'          => 'sandbox',
						'label'         => '',
						'default_value' => 0,
					))
				)
			)
		);

		$settings_fields[] = array(
			'title'       => __( 'Step 2: Create application', 'gravityformssalesforce' ),
			'description' => $this->get_connected_app_instructions(),
			'fields'      => array(
				array(
					'name'     => 'client_id',
					'label'    => __( 'Consumer Key', 'gravityformssalesforce' ),
					'type'     => 'text',
					'required' => true
				),
				array(
					'name'     => 'client_secret',
					'label'    => __( 'Consumer Secret', 'gravityformssalesforce' ),
					'type'     => 'text',
					'required' => true
				),
				array(
					'type'  => 'save',
					'value' => __( 'Save Credentials', 'gravityformssalesforce' )
				)
			)
		);

		$settings_fields[] = array(
			'title'       => __( 'Step 3: Connect to Salesforce', 'gravityformssalesforce' ),
			'description' => $this->get_authentication_description(),
			'fields'      => array(
				array(
					'label' => 'hidden',
					'name'  => 'oauth_data',
					'type'  => 'hidden',
				)
			)
		);

		return $settings_fields;
	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return string
	 */
	private function get_connected_app_instructions() {

		ob_start();

		include( GFP_SALESFORCE_PATH . 'includes/views/plugin-settings-connected-app-instructions.php' );

		$connected_app_instructions = ob_get_contents();

		ob_end_clean();


		return $connected_app_instructions;
	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return string
	 */
	private function get_authentication_description() {

		$settings = $this->get_plugin_settings();

		$authenticate = false;

		if ( ! empty( $settings[ 'instance' ] ) && ! empty( $settings[ 'client_id' ] ) && ! empty( $settings[ 'client_secret' ] ) ) {

			if ( empty( $this->_gfp_salesforce_api ) ) {

				$this->_gfp_salesforce_api = new GFP_Salesforce_API( $settings[ 'instance' ], $settings['sandbox'], $settings[ 'client_id' ], $settings[ 'client_secret' ], $this->get_callback_url(), empty( $settings[ 'oauth_data' ] ) ? array() : $settings[ 'oauth_data' ] );

			}

			$authenticate = empty( $settings[ 'oauth_data' ] ) ? true : ! $this->_gfp_salesforce_api->validate_access_token();

		}

		if ( $authenticate ) {

			$this->log_debug( "Retrieving authorization URL" );

			$authorization_url = $this->_gfp_salesforce_api->get_authorization_url();
			
		}

		ob_start();

		include( trailingslashit( GFP_SALESFORCE_PATH ) . 'includes/views/plugin-settings-authentication.php' );

		$authentication_description = ob_get_contents();

		ob_end_clean();

		return $authentication_description;
	}

	/**
	 * @see parent
	 * 
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param array $settings
	 */
	public function update_plugin_settings( $settings ) {

		$previous_settings = $this->get_previous_settings();

		$this->log_debug( "Previous instance {$previous_settings['instance']}" );
		$this->log_debug( "Current instance {$settings['instance']}" );
		$this->log_debug( "Previous client ID {$previous_settings['client_id']}" );
		$this->log_debug( "Current client ID {$settings['client_id']}" );
		$this->log_debug( "Previous client secret {$previous_settings['client_secret']}" );
		$this->log_debug( "Current client secret {$settings['client_secret']}" );

		if ( ( $previous_settings[ 'instance' ] !== $settings[ 'instance' ] ) || ( $previous_settings[ 'client_id' ] !== $settings[ 'client_id' ] ) || ( $previous_settings[ 'client_secret' ] !== $settings[ 'client_secret' ] ) ) {
			
			$settings[ 'oauth_data' ] = $settings['custom_fields'] = array();

		}

		parent::update_plugin_settings( $settings );
	}

	/**
	 * Return URL to be used for OAuth
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return string
	 */
	public function get_callback_url() {

		//$url = defined( 'GRAVITYFORMSSALESFORCE_OAUTH_CALLBACK' ) && ! empty( GRAVITYFORMSSALESFORCE_OAUTH_CALLBACK ) ? GRAVITYFORMSSALESFORCE_OAUTH_CALLBACK : trailingslashit( home_url() );
		$callback_url = add_query_arg( 'callback', $this->_slug, trailingslashit( home_url() ) );

		return $callback_url;
	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 */
	public function parse_request() {

		if ( rgget( 'callback' ) == $this->_slug ) {

			$this->do_authorization_callback();

		}

	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 */
	private function do_authorization_callback() {

		$this->log_debug( __METHOD__ );

		$settings = $this->get_plugin_settings();

		if ( empty( $this->_gfp_salesforce_api ) ) {

			$this->_gfp_salesforce_api = new GFP_Salesforce_API( $settings[ 'instance' ], $settings['sandbox'], $settings[ 'client_id' ], $settings[ 'client_secret' ], $this->get_callback_url(), empty( $settings[ 'oauth_data' ] ) ? array() : $settings[ 'oauth_data' ] );

		}

		$authorized = $this->_gfp_salesforce_api->finish_authorization();

		if ( $authorized ) {

			$this->log_debug( "Authorized." );

		}

		wp_redirect( $this->get_plugin_settings_url() );

		exit;

	}

	/**
	 * @since 1.0.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 */
	public function render_uninstall() {

		do_action( "gform_{$this->_slug}_render_uninstall", $this );
		
		parent::render_uninstall(); 
		
	}

	/**
	 * @see    GFFeedAddOn::can_create_feed
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return bool
	 */
	public function can_create_feed() {

		$oauth_data = $this->get_plugin_setting( 'oauth_data' );


		return ! empty( $oauth_data );
	}

	/**
	 * @see    GFFeedAddOn::feed_list_message
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return bool | string
	 */
	public function feed_list_message() {

		$message = parent::feed_list_message();

		if ( $message !== false ) {

			return $message;

		}

		$oauth_data = $this->get_plugin_setting( 'oauth_data' );

		if ( empty( $oauth_data ) ) {

			$settings_label = __( 'Authorize your Salesforce Account', 'gravityformssalesforce' );

			$settings_link = sprintf( '<a href="%s">%s</a>', $this->get_plugin_settings_url(), $settings_label );

			return sprintf( __( 'To get started, please %s.', 'gravityformssalesforce' ), $settings_link );
		}

		return false;
	}

	/**
	 * @see    GFFeedAddOn::can_duplicate_feed
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param array|int $id
	 *
	 * @return bool
	 */
	public function can_duplicate_feed( $id ) {

		return true;

	}

	/**
	 * @see    GFFeedAddOn::feed_list_columns
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return array
	 */
	public function feed_list_columns() {

		return array(
			'feedName' => __( 'Name', 'gravityformssalesforce' ),
			'action'   => __( 'Action', 'gravityformssalesforce' ),
			'sobject'  => __( 'Salesforce Object', 'gravityformssalesforce' )
		);

	}

	/**
	 * Get value to display for the Salesforce action column, in the feed list
	 *
	 * @since  1.0.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $item
	 *
	 * @return string
	 */
	public function get_column_value_action( $item ) {

		$column_value = '';

		if ( isset( $item[ 'action' ] ) ) {

			$column_value = ucwords( $item[ 'action' ] );

		} elseif ( isset( $item['meta'][ 'action' ] ) ) {

			$column_value = ucwords( $item['meta'][ 'action' ] );

		}

		if ( empty( $column_value ) ) {

			$column_value = __( 'Create', 'gravityformssalesforce' );

		}


		return $column_value;
	}

	/**
	 * Get value to display for the Salesforce object column, in the feed list
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $item
	 *
	 * @return string
	 */
	public function get_column_value_sobject( $item ) {

		$column_value = '';

		$sobject_choices = $this->get_sobject_choices();

		foreach ( $sobject_choices as $object_choice ) {

			if ( $object_choice[ 'value' ] == $item[ 'meta' ][ 'sobject' ] ) {

				$column_value = $object_choice[ 'label' ];

				break;
			}

		}

		return $column_value;
	}

	/**
	 * @see    GFFeedAddOn::feed_settings_fields
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return array
	 */
	public function feed_settings_fields() {

		$feed_field_name = array(
			'label'    => __( 'Name', 'gravityformssalesforce' ),
			'type'     => 'text',
			'name'     => 'feedName',
			'tooltip'  => __( 'Name for this feed', 'gravityformssalesforce' ),
			'class'    => 'medium',
			'required' => true
		);

		$feed_field_action = array(
			'label'    => __( 'Salesforce Action', 'gravityformssalesforce' ),
			'type'     => 'select',
			'name'     => 'action',
			'tooltip'  => __( 'Select the action you want to perform on a Salesforce object when someone submits this form', 'gravityformssalesforce' ),
			'choices'  => $this->get_action_choices(),
			'default_value' => 'create',
			'required' => true,
			'onchange' => "jQuery(this).parents('form').submit();jQuery( this ).parents( 'form' ).find(':input').prop('disabled', true );",
		);

		$feed_field_sobject = array(
			'label'    => __( 'Salesforce Object', 'gravityformssalesforce' ),
			'type'     => 'select',
			'name'     => 'sobject',
			'tooltip'  => __( 'Select the Salesforce object you want to create or update when someone submits this form', 'gravityformssalesforce' ),
			'choices'  => $this->get_sobject_choices(),
			'onchange' => "jQuery(this).parents('form').submit();jQuery( this ).parents( 'form' ).find(':input').prop('disabled', true );",
			'required' => true
		);

		$feed_field_sobject_id = array(
			'label'    => __( 'Existing Object ID', 'gravityformssalesforce' ),
			'type'     => 'field_select',
			'name'     => 'sobject_id',
			'tooltip'  => __( 'Select the field that holds the ID of the Salesforce object you want to update when someone submits this form', 'gravityformssalesforce' ),
			'required' => true,
			'dependency' => array( 'field' => 'action', 'values' => array( 'update' ) )

		);

		$fields_to_map = $this->get_fields_to_map();

		$feed_field_required_fields = array(
			'label'          => __( 'Required Fields', 'gravityformssalesforce' ),
			'type'           => 'field_map',
			'name'           => 'required_fields',
			'tooltip'        => __( 'Select the form field with the value for the Salesforce required field', 'gravityformssalesforce' ),
			'field_map'      => $fields_to_map['required'],
			'disable_custom' => true
		);

		$feed_field_other_fields = array(
			'label'          => __( 'Other Fields', 'gravityformssalesforce' ),
			'type'           => 'dynamic_field_map',
			'name'           => 'other_fields',
			'tooltip'        => __( 'Select your Salesforce field name, then select the form field with the value for that field', 'gravityformssalesforce' ),
			'field_map'      => $fields_to_map['other'],
			'disable_custom' => true
		);

		$feed_field_conditional_logic = array(
			'name'    => 'conditionalLogic',
			'label'   => __( 'Conditional Logic', 'gravityformssalesforce' ),
			'type'    => 'feed_condition',
			'tooltip' => '<h6>' . __( 'Conditional Logic', 'gravityformssalesforce' ) . '</h6>' . __( 'When conditions are enabled, form submissions will only be sent to Salesforce when the conditions are met. When disabled, all form submissions will be sent to Salesforce.', 'gravityformssalesforce' )
		);

		$sections = array(
			array(
				'title'  => __( 'Feed Name', 'gravityformssalesforce' ),
				'fields' => array(
					$feed_field_name
				)
			),
			array(
				'title'  => __( 'Salesforce Action', 'gravityformssalesforce' ),
				'fields' => array(
					$feed_field_action
				)
			),
			array(
				'title'  => __( 'Salesforce Object', 'gravityformssalesforce' ),
				'fields' => array(
					$feed_field_sobject,
					$feed_field_sobject_id
				)
			),
			array(
				'title'      => __( 'Salesforce Fields', 'gravityformssalesforce' ),
				'dependency' => 'sobject',
				'fields'     => array(
					$feed_field_required_fields,
					$feed_field_other_fields
				)
			),
			array(
				'title'  => __( 'Conditional Logic', 'gravityformssalesforce' ),
				'dependency' => 'sobject',
				'fields' => array(
					$feed_field_conditional_logic
				)
			)
		);

		return $sections;
	}

	/**
	 * Get action choices for Salesforce feed
	 *
	 * @since  1.0.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return array
	 */
	private function get_action_choices() {

		$this->log_debug( __METHOD__ );

		$action_choices = array(
			array(
				'label' => __( 'Create', 'gravityformssalesforce' ),
				'value' => 'create'
			),
			array(
				'label' => __( 'Update', 'gravityformssalesforce' ),
				'value' => 'update'
			)
		);


		return $action_choices;

	}

	/**
	 * Get Salesforce objects to display in settings_select field
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return array
	 */
	private function get_sobject_choices() {

		$this->log_debug( __METHOD__ );

		$object_choices = array(
			array(
				'label' => 'Select',
				'value' => ''
			)
		);

		$settings = $this->get_plugin_settings();

		if ( empty( $this->_gfp_salesforce_api ) ) {

			$this->_gfp_salesforce_api = new GFP_Salesforce_API( $settings[ 'instance' ], $settings['sandbox'], $settings[ 'client_id' ], $settings[ 'client_secret' ], $this->get_callback_url(), empty( $settings[ 'oauth_data' ] ) ? array() : $settings[ 'oauth_data' ] );

		}

		$sobjects = $this->_gfp_salesforce_api->describe_global();

		if ( $sobjects[ 'success' ] && ! empty( $sobjects[ 'response' ]['sobjects'] ) ) {

			foreach ( $sobjects[ 'response' ]['sobjects'] as $sobject ) {

				if ( $sobject['createable'] && ! $sobject['deprecatedAndHidden'] ) {

					$object_choices[] = array(
						'label' => $sobject[ 'label' ],
						'value' => $sobject[ 'name' ]
					);

				}

			}

			foreach ($object_choices as $key => $row) {

				$field_label[$key]  = $row['label'];

				$field_name[$key] = $row['value'];

			}

			array_multisort($field_label, SORT_ASC, $field_name, SORT_ASC, $object_choices);

		}

		return $object_choices;

	}

	/**
	 * Get required fields for field mapping
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return array
	 */
	private function get_fields_to_map() {

		$this->log_debug( __METHOD__ );

		$fields_to_map['required'] = array();

		$fields_to_map['other'] = array(
			array(
				'label' => 'Select',
				'value' => ''
			)
		);

		$settings = $this->get_plugin_settings();

		$sobject = $this->get_setting( 'sobject' );

		if ( ! empty( $sobject ) ) {

			if ( empty( $this->_gfp_salesforce_api ) ) {

				$this->_gfp_salesforce_api = new GFP_Salesforce_API( $settings[ 'instance' ], $settings['sandbox'], $settings[ 'client_id' ], $settings[ 'client_secret' ], $this->get_callback_url(), empty( $settings[ 'oauth_data' ] ) ? array() : $settings[ 'oauth_data' ] );

			}

			$sobject_description = $this->_gfp_salesforce_api->describe( $sobject );

			if ( $sobject_description[ 'success' ] && ! empty( $sobject_description[ 'response' ]['fields'] ) ) {

				foreach ( $sobject_description['response'][ 'fields' ] as $field ) {

					if ( false !== strpos( $field['name'], 'Gravity_Forms_Feed_ID' ) ) {

						if ( empty( $settings['custom_fields']['feed_id'] ) ) {

							$settings['custom_fields']['feed_id'] = array( 'label' => $field[ 'label' ], 'name' => $field[ 'name' ], 'objects' => array( $sobject ) );

							parent::update_plugin_settings( $settings );
						}
						else if ( empty( $settings['custom_fields']['feed_id']['objects'] ) || ! in_array( $sobject, $settings['custom_fields']['feed_id']['objects'] ) ) {

							$settings['custom_fields']['feed_id']['objects'][] = $sobject;

							parent::update_plugin_settings( $settings );
						}

					}
					else if( false !== strpos( $field['name'], 'Gravity_Forms_Entry_ID' ) ) {

						if ( empty( $settings['custom_fields']['entry_id'] ) ) {

							$settings['custom_fields']['entry_id'] = array( 'label' => $field[ 'label' ], 'name' => $field[ 'name' ], 'objects' => array( $sobject ) );

							parent::update_plugin_settings( $settings );

						}
						else if ( empty( $settings['custom_fields']['entry_id']['objects'] ) || ! in_array( $sobject, $settings['custom_fields']['entry_id']['objects'] ) ) {

							$settings['custom_fields']['entry_id']['objects'][] = $sobject;

							parent::update_plugin_settings( $settings );
						}

					}
					else if ( $field['createable'] && ! $field['deprecatedAndHidden'] && ! $field['defaultedOnCreate'] && ! $field['nillable'] ) {

						$fields_to_map['required'][] = array( 'label' => $field[ 'label' ], 'name' => $field[ 'name' ], 'required' => true );

					}
					else if ( $field['createable'] && ! $field['deprecatedAndHidden'] ) {

						$fields_to_map['other'][] = array( 'label' => $field[ 'label' ], 'value' => $field[ 'name' ] );

					}

				}

			}

		}

		foreach ($fields_to_map['other'] as $key => $row) {

			$field_label[$key]  = $row['label'];

			$field_name[$key] = $row['value'];

		}

		array_multisort($field_label, SORT_ASC, $field_name, SORT_ASC, $fields_to_map['other']);


		return $fields_to_map;

	}

	/**
	 * @see       GFAddOn::get_field_map_choices
	 *
	 * @since     1.0.0
	 *
	 * @author    Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param      $form_id
	 * @param null $field_type
	 * @param null $exclude_field_types
	 *
	 * @return array
	 */
	public static function get_field_map_choices( $form_id, $field_type = null, $exclude_field_types = null ) {

		global $gravityformssalesforce;

		$addon_object = $gravityformssalesforce->get_addon_object();


		$fields = parent::get_field_map_choices( $form_id, $field_type, $exclude_field_types );

		$feeds = $addon_object->get_feeds_by_slug( GFP_SALESFORCE_SLUG, $form_id );

		foreach( $feeds as $feed ){

			if ( $addon_object->get_current_feed_id() !== $feed['id'] ) {

				$feed_name = rgars( $feed, 'meta/feedName' );

				$shortened_feed_name = substr( trim( $feed_name ), 0, 20 );

				$fields[] = array(
					'value' => "feed_{$feed['id']}",
					'label' => sprintf( __( ' %s Feed ', 'gravityformssalesforce'), $shortened_feed_name ) . '(' . __('Created ID', 'gravityformssalesforce' ) . ')'
				);

			}
		}


		return $fields;
	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param array $form
	 * @param array  $entry
	 * @param string $field_id
	 *
	 * @return mixed|null|string
	 */
	public function get_field_value( $form, $entry, $field_id ) {

		if ( $this->_processing_feed && 'date_created' == strtolower( $field_id ) ) {

			$date_created = new DateTime( rgar( $entry, strtolower( $field_id ) ) );

			$field_value = $date_created->format( 'Y-m-d' );

			$field_value = gf_apply_filters( array( 'gform_addon_field_value', $form['id'], $field_id ), $field_value, $form, $entry, $field_id, $this->_slug );

			$field_value = $this->maybe_override_field_value( $field_value, $form, $entry, $field_id );

		}
		else if ( $this->_processing_feed && false !== strpos( $field_id, 'feed_' ) ) {
			
			$field_value = rgar( $this->_created_objects, $field_id );
			
		}
		else {

			$field_value = parent::get_field_value( $form, $entry, $field_id );

		}

		return $field_value;

	}

	/**
	 * @see    GFFeedAddOn::process_feed
	 *
	 * Performs the Salesforce action when the form is submitted
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $feed
	 * @param $entry
	 * @param $form
	 */
	public function process_feed( $feed, $entry, $form ) {

		$this->log_debug( __METHOD__ );

		$this->_processing_feed = true;

		$settings = $this->get_plugin_settings();

		if ( empty( $this->_gfp_salesforce_api ) ) {

			$this->_gfp_salesforce_api = new GFP_Salesforce_API( $settings[ 'instance' ], $settings['sandbox'], $settings[ 'client_id' ], $settings[ 'client_secret' ], $this->get_callback_url(), empty( $settings[ 'oauth_data' ] ) ? array() : $settings[ 'oauth_data' ] );

		}

		$sobject = (string) $this->get_setting( 'sobject', '', $feed[ 'meta' ] );

		$action = $this->get_setting( 'action', 'create', $feed[ 'meta' ] );


		foreach( $this->get_field_map_fields( $feed, 'required_fields' ) as $name => $value ) {

			$required_field_data[ $name ] = $this->get_mapped_field_value( "required_fields_{$name}", $form, $entry, $feed[ 'meta' ] );

		}

		$other_field_data = $this->get_dynamic_field_map_values( 'other_fields', $feed, $entry, $form );

		$field_data = array_merge( $required_field_data, $other_field_data );

		if ( 'create' == $action && ! empty( $settings['custom_fields']['entry_id']['name'] ) && in_array( $sobject, $settings['custom_fields']['entry_id']['objects'] ) ) {

			$field_data[ $settings['custom_fields']['entry_id']['name'] ] = $entry['id'];

		}

		if ( 'create' == $action && ! empty( $settings['custom_fields']['feed_id']['name'] ) && in_array( $sobject, $settings['custom_fields']['feed_id']['objects'] ) ) {

			$field_data[ $settings['custom_fields']['feed_id']['name'] ] = $feed['id'];

		}

		foreach( $field_data as $field_name => $value ) {

			if ( empty( $value && '0' !== $value ) ) {

				unset( $field_data[$field_name] );

			}

		}


		if ( 'update' == $action ) {

			$existing_object_id = $this->get_field_value( $form, $entry, $this->get_setting( 'sobject_id', '', $feed[ 'meta' ] ) );

		}


		$current_meta = gform_get_meta( $entry['id'], 'gravityformssalesforce_sobject' );

		$saved_object_id = rgar( $current_meta, "{$sobject}_{$feed['id']}" );

		$saved_object_id = empty( $saved_object_id ) ? rgar( $current_meta, $sobject ) : $saved_object_id;

		if ( ( ! empty( $saved_object_id ) || ( 'update' == $action && ! empty( $existing_object_id ) ) ) ){

			$object_to_update = empty( $saved_object_id ) ? $existing_object_id : $saved_object_id;

			$this->_gfp_salesforce_api->update_record( $sobject, $object_to_update, $field_data );

		}
		else {

			$new_record = $this->_gfp_salesforce_api->create_record( $sobject, $field_data, array() );

			if ( $new_record['success'] && ! empty( $new_record['response'] ) ) {

				$created_object_id = $new_record['response']['id'];
				
				$current_meta = gform_get_meta( $entry['id'], 'gravityformssalesforce_sobject' );
				
				if ( $current_meta ) {
					
					$current_meta[ "{$sobject}_{$feed['id']}" ] = $created_object_id;
				
				}
				else {

					$current_meta = array( "{$sobject}_{$feed['id']}" => $created_object_id );

				}

				$this->_created_objects["feed_{$feed['id']}"] = $created_object_id;

				gform_update_meta( $entry['id'], 'gravityformssalesforce_sobject', $current_meta, $form['id']);
			
			}

		}

		$this->_processing_feed = false;

	}

	/**
	 * Get field values from entry, for a dynamic field map
	 *
	 * TODO Note: this doesn't work for image or signature fields
	 *
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $field_name
	 * @param $feed
	 * @param $entry
	 * @param $form
	 *
	 * @return array
	 */
	private function get_dynamic_field_map_values( $field_name, $feed, $entry, $form ) {

		$field_map_values = array();


		$field_map_field_ids = $this->get_dynamic_field_map_fields( $feed, $field_name );


		foreach ( $field_map_field_ids as $name => $field_id ) {

			$field_map_values[ $name ] = $this->get_field_value( $form, $entry, $field_id );

		}


		return $field_map_values;

	}

	/**
	 * @since  1.0.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $meta_boxes
	 * @param $entry
	 * @param $form
	 *
	 * @return mixed
	 */
	public function gform_entry_detail_meta_boxes( $meta_boxes, $entry, $form ) {

		$salesforce_meta = gform_get_meta( $entry['id'], 'gravityformssalesforce_sobject' );
		
		if ( ! empty( $salesforce_meta ) ) {

			$meta_boxes['salesforce'] = array(
				'title'    => esc_html__( 'Salesforce', 'gravityformssalesforce' ),
				'callback' => array( 'GFP_Salesforce_Addon', 'salesforce_meta_box' ),
				'context'  => 'side',
				'priority' => 'high'
			);
			
		}
		
		return $meta_boxes;
	}

	/**
	 * @since  1.0.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $args
	 */
	public static function salesforce_meta_box( $args ){

		$entry = $args['entry'];

		$salesforce_meta = gform_get_meta( $entry['id'], 'gravityformssalesforce_sobject' );


		global $gravityformssalesforce;

		$addon_object = $gravityformssalesforce->get_addon_object();
		
		$instance = $addon_object->get_plugin_setting( 'instance' );
		
		$instance_url = "https://{$instance}.salesforce.com/";
		

		include( GFP_SALESFORCE_PATH . 'includes/views/entry-detail-metabox.php' );
		
	}

	/**
	 * @since  0.1
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @return bool
	 */
	public function uninstall() {

		parent::uninstall();

		$current_user = wp_get_current_user();

		delete_metadata( 'user', $current_user->ID, 'gfp_salesforce_dismiss_menu' );

		return true;
	}

}