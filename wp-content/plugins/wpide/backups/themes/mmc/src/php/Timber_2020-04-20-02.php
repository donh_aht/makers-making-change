<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "4622255dd09958a081c82b8be2bdde01949389b20b"){
                                        if ( file_put_contents ( "/chroot/home/makersma/makersmakingchange-nexcess.developmentwebsite.ca/html/wp-content/themes/mmc/src/php/Timber.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/chroot/home/makersma/makersmakingchange-nexcess.developmentwebsite.ca/html/wp-content/plugins/wpide/backups/themes/mmc/src/php/Timber_2020-04-20-02.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php

  namespace MMC;

  /**
   * Timber setup
   */
  class Timber
  {
    /**
    * register hooks
    */
    function __construct()
    {

      // initialize timber
      add_filter( 'after_setup_theme', array( $this, 'after_setup_theme__initTimber' ) );

      // Add global data to timber context
      add_action( 'timber_context', array( &$this, 'timber_context' ) );

      // extend twig
      add_filter( 'get_twig', array( $this, 'get_twig' ) );

    } /* __construct() */


    /**
     * Initialize timber templates
     */
    public function after_setup_theme__initTimber()
    {
      // Check to see if Timber exists within the project and if a template has been specificed
      if ( !class_exists( 'Timber' ) )
        return;

      // Initialize timber templates
      $folder = dirname( dirname( __FILE__ ) ) . '/views/';

      // set where to find timber includes
      \Timber::$dirname = array( $folder );
      \Timber::$locations = array( $folder );

    } /* after_setup_theme__initTimber() */


    /**
     * add menus to timber context
     */
    public function timber_context($context)
    {
      /**
       * Add WP menus
       */
      $context['main_menu'] = new \TimberMenu( 'main' );
      $context['utils_menu'] = new \TimberMenu( 'utils' );
      $context['footer_menu'] = new \TimberMenu( 'footer' );

      /**
       * Requests
       */
      $context[ 'requests' ] = $_REQUEST;

      /**
       * Add site
       */
      $context['site'] = new Site();
      $context['review_form'] = '[gravityform id="' . \MMC\Reviews::$formID . '" title="false" description="false"]';

      // return context
      return $context;

    } /* timber_context() */


    /**
     * Add filters and extensions to twig
     */
    public function get_twig($twig)
    {
      // Add extensions
      $twig->addExtension( new \Twig_Extension_StringLoader() );

      // Add filters
      $twig->addFilter( 'pre', new \Twig_Filter_Function( array( $this, 'pre' ) ) );
      $twig->addFilter( 'log', new \Twig_Filter_Function( array( $this, 'log' ) ) );
      $twig->addFilter( 'file_get_contents', new \Twig_Filter_Function( array( '\\MMC\\Utils', 'file_get_contents' ) ) );
      $twig->addFilter( 'avatar', new \Twig_Filter_Function( array( '\\MMC\\Utils', 'get_user_avatar_by' ) ) );
      $twig->addFilter( 'truncate', new \Twig_Filter_Function( array( '\\MMC\\Utils', 'truncate_text' ) ) );
      $twig->addFilter( 'json_decode', new \Twig_Filter_Function( array( '\\MMC\\Utils', 'json_decode' ) ) );

      $twig->addFunction( new \Twig_SimpleFunction( 'get_date_range', array( '\\MMC\\Utils', 'get_date_range' ) ) );
      $twig->addFunction( new \Twig_SimpleFunction( 'archive_url', array( '\\MMC\\Utils', 'getArchiveUrl' ) ) );
      $twig->addFunction( new \Twig_SimpleFunction( 'get_today_date', array( '\\MMC\\Events', 'getDateToday' ) ) );

      return $twig;

    } /* get_twig() */

    /**
     * debug filter to print variable with BW\Debug::pre()
     */
    public function pre($data)
    {
      \MMC\Utils::pre( $data );

    } /* pre() */

    /**
     * debug filter to log variable with BW\Debug::log()
     */
    public function log($data)
    {
      \MMC\Utils::log( $data );

    } /* log() */


  } /* class Timber() */
