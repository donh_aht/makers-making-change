<?php
/**
 * Show the feeds that a form has
 *
 * @since 2.4.0
 *
 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
 */
class GFPGFU_Show_Feed_On_Form_List {


	private $_calling_get_addons = false;

	/**
	 * @since 2.4.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 */
	public function run () {

		add_action( 'gform_form_list_column_title', array( $this, 'gform_form_list_column_title' ) );

		add_filter( 'gfp_gfutility_get_addons_addon_data', array( $this, 'gfp_gfutility_get_addons_addon_data' ), 10, 2 );

		add_filter( 'gfp_gfutility_enabled_addons_for_form', array( $this, 'gfp_gfutility_enabled_addons_for_form' ), 10, 2 );

		add_filter( 'gfp_gfutility_get_addon_icon', array( $this, 'gfp_gfutility_get_addon_icon' ), 10, 2 );

		add_action( 'admin_head', array( $this, 'admin_head' ) );
	}

	public function admin_head(){

		if ( 'form_list' == GFForms::get_page() ){

			echo "<br /><style>span.gfp_gfutility_form_list_feed_addon {
    border: 2px solid #dddddd;
    border-radius: 4px;
    padding: 3px;
    display:inline-block;
}span.gfp_gfutility_form_list_feed_addon:hover {
    background: #eeeeee;
}span.gfp_gfutility_feed_addon_active{font-weight:bold;}span.gfp_gfutility_feed_addon_inactive{color:rgba(0, 115, 170, 0.41);}</style>";

		}

	}

	/**
	 * Output title and feeds for a form
	 *
	 * @see GF_Form_List_Table::column_title()
	 *
	 * @since 2.4.0
	 *
	 * @author Naomi C. Bush for gravity+ <support@gravityplus.pro>
	 *
	 * @param $form Form object
	 */
	public function gform_form_list_column_title( $form ) {

		echo '<strong><a href="?page=gf_edit_forms&id='. absint( $form->id ) .'">' . esc_html( $form->title ) . '</a></strong>';

		$this->_calling_get_addons = true;

		$addons = GFPGFU_Helper::get_addons();

		$this->_calling_get_addons = false;

		$enabled_addons = array();

		foreach( $addons as $slug => $addon_data ) {

			if( array_key_exists( $slug, $enabled_addons ) ) {

				continue;

			}


			$addon_feeds = GFAPI::get_feeds( null, $form->id, $slug );

			if ( is_array( $addon_feeds ) ) {

				$enabled_addons[ $slug ] = array( 'icon' => $this->get_addon_icon( $slug, $addon_data, true ),
				                                  'title' => $addon_data['title'] );
			}
			else {

				$addon_feeds = GFAPI::get_feeds( null, $form->id, $slug, false );

				if( is_array( $addon_feeds ) ) {

					$enabled_addons[ $slug ] = array( 'icon' => $this->get_addon_icon( $slug, $addon_data, false ),
					                                  'title' => $addon_data['title'] );

				}

			}


			unset( $addon_feeds );

		}

		$enabled_addons = apply_filters('gfp_gfutility_enabled_addons_for_form', $enabled_addons, $form );

		if ( ! empty( $enabled_addons ) ) {

			echo "<br /><div class='gfp_gfutility_form_list_addons_with_feeds'>";

			foreach ( $enabled_addons as $slug => $addon_info ) {

				echo "<span class='gfp_gfutility_form_list_feed_addon'><a title='{$addon_info['title']}' href='" . ( empty( $addon_info['link'] ) ? admin_url( "admin.php?page=gf_edit_forms&view=settings&subview={$slug}&id={$form->id}" ) : esc_attr( $addon_info['link'] ) ) . "'>{$addon_info['icon']}</a></span>&nbsp;";

			}

			echo "</div><br />";

		}

	}

	private function get_addon_icon( $slug, $addon_info, $active ) {

		$default_addon_icon = strtoupper($addon_info['title'][0] . $addon_info['title'][1]);

		$open_tag = "<span class='gfp_gfutility_feed_addon_" . ( $active ? 'active' : 'inactive' ) . "'>";

		$closing_tag = "</span>";

		if ( empty( $addon_info['icon'] ) ) {

			$icon = file_exists(GFP_GF_UTILITY_PATH . 'tools/show-feed-on-form-list/' . $slug . '.svg') ? '<img style="height:1em;" src="' . GFP_GF_UTILITY_URL . 'tools/show-feed-on-form-list/' . $slug . '.svg' . '">' : $default_addon_icon;

		}
		else {

			$icon = $addon_info['icon'];

		}

		$icon = apply_filters( 'gfp_gfutility_get_addon_icon', $icon, $slug );


		return "{$open_tag}{$icon}{$closing_tag}";
	}

	public function gfp_gfutility_get_addon_icon( $icon, $slug ) {

		switch( $slug ){

			case 'gravityflow':

				if ( function_exists('gravity_flow' ) ) {

					$icon = "<img style='height:1em;' src='" . gravity_flow()->get_base_url() . "/images/gravityflow-icon-blue.svg'>";

				}


				break;

			case 'gravityformspaypal':

				$icon = '<i class="fa fa-paypal"></i>';


				break;

			case 'gravityformsuserregistration':

				$icon = '<i class="fa fa-user"></i>';


				break;

			case 'gravityplus-third-party-post':

				$icon = '<i class="fa fa-share"></i>';


				break;
		}


		return $icon;
	}

	/**
	 * @param array $addon_data
	 * @param GFFeedAddOn $addon
	 *
	 * @return mixed
	 */
	public function gfp_gfutility_get_addons_addon_data( $addon_data, $addon ) {

		if ( $this->_calling_get_addons ) {

			$icon = $addon->form_settings_icon();

			if ( empty( $icon ) ) {

				$icon = $addon->plugin_settings_icon();

			}

			if ( ! empty( $icon ) ) {

				$addon_data = array_merge( $addon_data, array( 'icon'=> $icon ) );

			}

		}


		return $addon_data;
	}

	/**
	 * Add non-feed framework add-ons to the list
	 *
	 * @since 2.4.0
	 *
	 * @param array $enabled_addons
	 * @param object $form
	 *
	 * @return mixed
	 */
	public function gfp_gfutility_enabled_addons_for_form( $enabled_addons, $form ) {

		$enabled_addons = $this->add_gravityview( $enabled_addons, $form->id );

		$enabled_addons = $this->add_gfchart( $enabled_addons, $form->id );

		$enabled_addons = $this->add_gravitypdf( $enabled_addons, $form->id );


		return $enabled_addons;
	}

	/**
	 * Add GravityView
	 *
	 * @since 2.4.0
	 *
	 * @param $enabled_addons
	 * @param $form_id
	 *
	 * @return mixed
	 */
	private function add_gravityview( $enabled_addons, $form_id ) {

		if ( function_exists( 'gravityview_get_connected_views' ) ) {

			$connected_views = gravityview_get_connected_views( $form_id );

			if( ! empty( $connected_views ) ) {

				$can_edit_views = false;

				foreach ( (array) $connected_views as $view ) {

					if( GVCommon::has_cap( 'edit_gravityview', $view->ID ) ) {

						$can_edit_views = true;

						break;
					}

				}

				if ( $can_edit_views ) {

					$enabled_addons[ 'gravityview' ] = array(
						'title' => 'GravityView',
						'icon'  => "<span class='gfp_gfutility_feed_addon_active'><i class=\"fa fa-lg gv-icon-astronaut-head gv-icon\"></i></span>",
						'link'  => admin_url( "edit.php?post_type=gravityview&gravityview_form_id={$form_id}" )
					);

				}

			}

		}


		return $enabled_addons;
	}

	/**
	 * Add GFChart
	 *
	 * @since 2.4.0
	 *
	 * @param $enabled_addons
	 * @param $form_id
	 *
	 * @return mixed
	 */
	private function add_gfchart( $enabled_addons, $form_id ){

		if ( method_exists( 'GFChart_API', 'get_charts' ) ) {

			$charts = GFChart_API::get_charts( $form_id );

			if( ! empty( $charts ) ) {

				/*$can_edit_views = false;

				foreach ( (array) $connected_views as $view ) {

					if( GVCommon::has_cap( 'edit_gravityview', $view->ID ) ) {

						$can_edit_views = true;

						break;
					}

				}*/

				//if ( $can_edit_views ) {

					$enabled_addons[ 'gfchart' ] = array(
						'title' => 'GFChart',
						'icon'  => "<span class='gfp_gfutility_feed_addon_active'><img style='height:1em;' src='" . GFCHART_URL . "images/icon-optimized-2.svg'></span>",
						'link'  => admin_url( "edit.php?post_type=gfchart" )
					);

				//}

			}

		}


		return $enabled_addons;

	}

	/**
	 * Add GravityPDF
	 *
	 * @since 2.4.0
	 *
	 * @param $enabled_addons
	 * @param $form_id
	 *
	 * @return mixed
	 */
	private function add_gravitypdf( $enabled_addons, $form_id ) {

		global $gfpdf;

		if ( ! empty( $gfpdf->gform ) ) {

			if ( $gfpdf->gform->has_capability( 'gravityforms_edit_settings' ) ) {

				$form = $gfpdf->gform->get_form( $form_id );

				$pdfs = rgar( $form, 'gfpdf_form_settings' );

				if ( ! empty( $pdfs ) ){

					$active_pdf = false;

					foreach( $pdfs as $pdf ) {

						if( $active_pdf ) {

							break;

						}


						if ( ! empty( $pdf['active'] ) ) {

							$enabled_addons[ 'pdf' ] = array(
								'title' => 'Gravity PDF',
								'icon'  => "<span class='gfp_gfutility_feed_addon_active'><i class=\"fa fa-file\"></i></span>",
							);

							$active_pdf = true;

						}
						else {

							$enabled_addons[ 'pdf' ] = array(
								'title' => 'Gravity PDF',
								'icon'  => "<span class='gfp_gfutility_feed_addon_inactive'><i class=\"fa fa-file\"></i></span>",
							);

						}

						unset( $pdf );

					}

				}

				}

		}


		return $enabled_addons;
	}

}

$gfpgfu_show_feed = new GFPGFU_Show_Feed_On_Form_List();
$gfpgfu_show_feed->run();