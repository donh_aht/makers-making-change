<?php

namespace WDTGravityIntegration;

/**
 * @package Gravity Forms integration for wpDataTables
 * @version 1.0.1
 */
/*
Plugin Name: Gravity Forms integration for wpDataTables
Plugin URI: https://wpdatatables.com/documentation/addons/gravity-forms-integration/
Description: Tool that adds "Gravity Form" as a new table type and allows you to create wpDataTables from Gravity Forms entries data.
Version: 1.0.1
Author: TMS-Plugins
Author URI: http://tms-plugins.com
Text Domain: wpdatatables
Domain Path: /languages
*/

use DateTime;

// Full path to the WDT GF root directory
define('WDT_GF_ROOT_PATH', plugin_dir_path(__FILE__));
// URL of WDT GF integration plugin
define('WDT_GF_ROOT_URL', plugin_dir_url(__FILE__));
// Required wpDataTables version
define('WDT_GF_VERSION_TO_CHECK', '2.0.2');

// Init Gravity Forms integration for wpDataTables add-on
add_action('plugins_loaded', array('WDTGravityIntegration\Plugin', 'init'), 10);

// Enqueue Gravity files
add_action('wdt_enqueue_on_edit_page', array('WDTGravityIntegration\Plugin', 'wdtGravityEnqueue'));

// Add "Gravity Form" in "Input data source type" dropdown on "Data Source" tab
add_action('wdt_add_table_type_option', array('WDTGravityIntegration\Plugin', 'addTableTypeOption'));

// Add Gravity Form HTML elements on "Data Source" tab on table configuration page
add_action('wdt_add_data_source_elements', array('WDTGravityIntegration\Plugin', 'addGravityOnDataSourceTab'));

// Add "Gravity Form" tab on table configuration page
add_action('wdt_add_table_configuration_tab', array('WDTGravityIntegration\Plugin', 'addGravityTab'));

// Add tablpanel for "Gravity Form" tab on table configuration page
add_action('wdt_add_table_configuration_tabpanel', array('WDTGravityIntegration\Plugin', 'addGravityTabPanel'));

// Save table configuration
add_action('wp_ajax_wdt_gravity_save_table_config', array('WDTGravityIntegration\Plugin', 'saveTableConfig'));

// Get form fields AJAX action
add_action('wp_ajax_wdt_gf_get_form_fields', array('WDTGravityIntegration\Plugin', 'getGravityFormFields'));

// Extend the wpDataTables supported data sources
add_action('wpdatatables_generate_gravity', array('WDTGravityIntegration\Plugin', 'gravityBasedConstruct'), 10, 3);

// Extend table config before saving table to DB
add_filter('wpdatatables_filter_insert_table_array', array(
    'WDTGravityIntegration\Plugin',
    'extendTableConfig'
), 10, 1);

/**
 * Class Plugin
 * Main entry point of the wpDataTables Gravity Forms integration
 * @package WDTGravityIntegration
 */
class Plugin {

    public static $initialized = false;

    /**
     * Instantiates the class
     * @return bool
     */
    public static function init() {
        // Check if wpDataTables is installed
        if (!defined('WDT_ROOT_PATH') || !defined('GF_MIN_WP_VERSION')) {
            add_action('admin_notices', array('WDTGravityIntegration\Plugin', 'wdtNotInstalled'));
            return false;
        }

        // Check if wpDataTables required version is installed
        if (version_compare(WDT_CURRENT_VERSION, WDT_GF_VERSION_TO_CHECK) < 0) {
            // Show message if required wpDataTables version is not installed
            add_action('admin_notices', array('WDTGravityIntegration\Plugin', 'wdtRequiredVersionMissing'));
            return false;
        }

        \WPDataTable::$allowedTableTypes[] = 'gravity';
        return self::$initialized = true;
    }

    /**
     * Show message if wpDataTables is not installed
     */
    public static function wdtNotInstalled() {
        $message = __('Gravity Forms integration for wpDataTables is an add-on - please install and activate wpDataTables and Gravity Forms to be able to use it!', 'wpdatatables');
        echo "<div class=\"error\"><p>{$message}</p></div>";
    }

    /**
     * Show message if required wpDataTables version is not installed
     */
    public static function wdtRequiredVersionMissing() {
        $message = __('Gravity Forms integration for wpDataTables add-on requires wpDataTables version ' . WDT_GF_VERSION_TO_CHECK . '. Please update wpDataTables plugin to be able to use it!', 'wpdatatables');
        echo "<div class=\"error\"><p>{$message}</p></div>";
    }

    /**
     * Enqueue all necessary styles and scripts
     */
    public static function wdtGravityEnqueue() {
        // Gravity Forms integration CSS
        wp_enqueue_style('wdt-gf-wizard', WDT_GF_ROOT_URL . 'assets/css/table_creation_wizard.css');
        // Gravity Forms integration JS
        wp_enqueue_script('wdt-gf-table-config', WDT_GF_ROOT_URL . 'assets/js/gf_table_config_object.js', array(), false, true);
        wp_enqueue_script('wdt-gf-wizard', WDT_GF_ROOT_URL . 'assets/js/table_creation_wizard.js', array(), false, true);

        \WDTTools::exportJSVar('wdtGfSettings', \WDTTools::getDateTimeSettings());
        \WDTTools::exportJSVar('wdtGfTranslationStrings', \WDTTools::getTranslationStrings());
    }

    /**
     * Method that adds "Gravity From" option in "Input data source type" dropdown
     */
    public static function addTableTypeOption() {
        echo '<option value="gravity">Gravity Form</option>';
    }

    /**
     * Adds Gravity Form HTML elements on table configuration page
     */
    public static function addGravityOnDataSourceTab() {
        ob_start();
        include WDT_GF_ROOT_PATH . 'templates/data_source_block.inc.php';
        $gravityDataSource = apply_filters('wdt_gravity_data_source_block', ob_get_contents());
        ob_end_clean();

        echo $gravityDataSource;
    }

    /**
     * Add "Gravity Form" tab on table configuration page
     */
    public static function addGravityTab() {
        ob_start();
        include WDT_GF_ROOT_PATH . 'templates/tab.inc.php';
        $gravityTab = apply_filters('wdt_gravity_tab', ob_get_contents());
        ob_end_clean();

        echo $gravityTab;
    }

    /**
     * Add tablpanel for "Gravity Form" tab on table configuration page
     */
    public static function addGravityTabPanel() {
        ob_start();
        include WDT_GF_ROOT_PATH . 'templates/tabpanel.inc.php';
        $gravityTabpanel = apply_filters('wdt_gravity_tabpanel', ob_get_contents());
        ob_end_clean();

        echo $gravityTabpanel;
    }

    /**
     * Helper method to get form names and IDs
     */
    public static function getGFNamesIds() {
        global $wpdb;

        $table = \GFFormsModel::get_form_table_name();
        $sql = $wpdb->prepare(
            "SELECT id, title
                                 FROM $table
                                 WHERE is_active = %d
                                 AND is_trash = %d",
            true,
            false
        );

        return $wpdb->get_results($sql);

    }

    /**
     * Helper method to get form data for AJAX
     */
    public static function getGravityFormFields() {
        $nonce = sanitize_text_field($_POST['nonce']);
        if (wp_verify_nonce($nonce, 'wdtEditNonce')) {
            $formId = (int)$_POST['formId'];
            if ($formId) {
                $form = \GFAPI::get_form($formId);
                // Fetch fields as potential columns
                $formFields = array();
                foreach ($form['fields'] as $field) {
                    if (!in_array($field['type'], array('page', 'section', 'html', 'captcha', 'password'))) {
                        $formFields[] = array(
                            'id' => $field['id'],
                            'label' => $field['label'],
                            'type' => $field['type']
                        );
                    }
                }
                echo json_encode($formFields);
            } else {
                echo json_encode(array('error' => 'Form data could not be read!'));
            }
        } else {
            echo json_encode(array('error' => 'Nonce could not be verified!'));
        }
        exit();
    }

    /**
     * Validate and save Gravity based wpDataTable config to DB
     */
    public static function saveTableConfig() {
        // Sanitize NONCE
        $nonce = sanitize_text_field($_POST['nonce']);

        $gravityData = json_decode(
            stripslashes_deep($_POST['gravity'])
        );

        // Sanitize Gravity Config
        $gravityData = self::sanitizeGravityConfig($gravityData);

        if (wp_verify_nonce($nonce, 'wdtEditNonce')) {
            if ($gravityData->formId) {
                // Create a table object
                $table = json_decode(stripslashes_deep($_POST['table']));
                $table->content = json_encode(
                    array(
                        'formId' => $gravityData->formId,
                        'fieldIds' => $gravityData->fields
                    )
                );

                \WDTConfigController::saveTableConfig($table);
            } else {
                echo json_encode(array('error' => 'Form data could not be read!'));
            }
        } else {
            echo json_encode(array('error' => 'Nonce could not be verified!'));
        }
        exit();
    }

    /**
     * Helper method for sanitizing the user input in the gravity config
     *
     * @param $gravityData
     *
     * @return mixed
     */
    public static function sanitizeGravityConfig($gravityData) {
        foreach ($gravityData->fields as &$field) {
            if (!in_array($field, ['date_created', 'id', 'created_by', 'ip'])) {
                $field = (int)$field;
            } else {
                $field = sanitize_text_field($field);
            }
        }
        $gravityData->formId = (int)$gravityData->formId;
        $gravityData->showDeletedRecords = (int)$gravityData->showDeletedRecords;

        return $gravityData;
    }

    /**
     * Method that pass $array and $params to wpDataTable arrayBasedConstruct Method
     * that will fill wpDataTable object
     *
     * @param $wpDataTable - WPDataTable object
     * @param $content - stdClass with with form ID and form fields IDs
     * @param $params - parameters that are prepared in WPDataTable fillFromData method
     */
    public static function gravityBasedConstruct($wpDataTable, $content, $params) {
        $content = json_decode($content);
        /** @var \WPDataTable $wpDataTable */
        if ($wpDataTable->getWpId()) {
            $table = \WDTConfigController::loadTableFromDB($wpDataTable->getWpId());
            $gravityData = json_decode($table->advanced_settings)->gravity;
        } else {
            $gravityData = null;
        }

        if (empty($params['columnTitles'])) {
            $params['columnTitles'] = self::getColumnHeaders($content->formId, $content->fieldIds);
        }

        $wpDataTable->arrayBasedConstruct(self::generateFormArray($content->formId, $content->fieldIds, $gravityData), $params);
    }

    /**
     * Generate array for table
     *
     * @param $formId
     * @param $fieldsIds
     * @param $gravityData
     *
     * @return array
     */
    public static function generateFormArray($formId, $fieldsIds, $gravityData) {
        $fieldsData = array();
        $tableArray = array();

        $searchCriteria = self::prepareSearchCriteria($gravityData);

        $entries = \GFAPI::get_entries($formId, $searchCriteria, null, array('offset' => 0, 'page_size' => 10000));
        $form = \GFAPI::get_form($formId);

        // Get selected fields data with the child field (multi-fields
        // like checkboxes, products, etc.
        foreach ($form['fields'] as $formField) {
            if (in_array((int)$formField->id, $fieldsIds)) {
                $fieldsData[$formField->id]['type'] = $formField['type'];

                $existingOriginalHeaders = array_column($fieldsData, 'label');
                $fieldsData[$formField->id]['label'] = \WDTTools::generateMySQLColumnName($formField['label'], $existingOriginalHeaders);

                if (is_array($formField->inputs)) {
                    $selectedMultiField = array();
                    foreach ($formField->inputs as $formInput) {
                        $selectedMultiField[] = $formInput['id'];
                    }
                    $fieldsData[$formField->id]['fieldIds'] = $selectedMultiField;
                } else {
                    $fieldsData[$formField->id]['fieldIds'] = $formField->id;
                }
            }
        }

        foreach (array_intersect($fieldsIds, ['date_created', 'id', 'created_by', 'ip']) as $commonField) {
            $fieldsData[$commonField] = array(
                'type' => $commonField,
                'label' => str_replace('_', '', $commonField),
                'fieldIds' => $commonField,
            );
        }

        foreach ($entries as $entry) {
            $tableArrayEntry = array();

            foreach ($fieldsData as $fieldData) {
                $tableArrayEntry[$fieldData['label']] = self::prepareFieldsData($entry, $fieldData);
            }
            $tableArray[] = $tableArrayEntry;
        }

        return $tableArray;
    }

    /**
     * Method that return array that will passed in Gravity Form API get_entries method
     * to filter the entries
     *
     * @param $gravityData
     *
     * @return array
     */
    public static function prepareSearchCriteria($gravityData) {
        $searchCriteria = array();

        if (null === $gravityData || $gravityData->showDeletedRecords === 0) {
            $searchCriteria['status'] = 'active';
        }

        if ($gravityData !== null && $gravityData->dateFilterLogic) {

            if ($gravityData->dateFilterLogic === 'range') {

                $dateFormat = get_option('wdtDateFormat');
                $timeFormat = get_option('wdtTimeFormat');

                if ($gravityData->dateFilterFrom) {
                    $searchCriteria['start_date'] = DateTime::createFromFormat(
                        $dateFormat . ' ' . $timeFormat,
                        $gravityData->dateFilterFrom
                    )->format('Y-m-d H:i:s');
                }

                if ($gravityData->dateFilterTo) {
                    $searchCriteria['end_date'] = DateTime::createFromFormat(
                        $dateFormat . ' ' . $timeFormat,
                        $gravityData->dateFilterTo
                    )->format('Y-m-d H:i:s');
                }

            } else {
                $searchCriteria['start_date'] = date('Y-m-d', strtotime("-{$gravityData->dateFilterTimeUnits} {$gravityData->dateFilterTimePeriod}"));
                $searchCriteria['end_date'] = date('Y-m-d', time());
            }
        }

        return $searchCriteria;
    }

    /**
     * Helper method that formats the entry how it should be displayed in wpDataTable
     * based on the field type
     * @param $entry
     * @param $fieldData
     * @return mixed|string
     */
    public static function prepareFieldsData($entry, $fieldData) {
        switch ($fieldData['type']) {
            case 'textarea':
                $fieldData = nl2br($entry[$fieldData['fieldIds']]);

                return $fieldData;
                break;
            case 'multiselect':
                $fieldData = str_replace(',', ', ', str_replace(array('[', ']', '"'), '', $entry[$fieldData['fieldIds']]));

                return $fieldData;
                break;
            case 'checkbox':
                foreach ($fieldData['fieldIds'] as $fieldId) {
                    $childEntry[] = $entry[$fieldId];
                }

                return implode(', ', array_filter($childEntry));
                break;
            case 'name':
                foreach ($fieldData['fieldIds'] as $fieldId) {
                    $childEntry[] = $entry[$fieldId];
                }

                return implode(' ', array_filter($childEntry));
                break;
            case 'time':
                $fieldData = $entry[(int)$fieldData['fieldIds'][0]];

                return $fieldData;
                break;
            case 'date':
                if (is_array($fieldData['fieldIds'])) {
                    $fieldData = $entry[(int)$fieldData['fieldIds'][0]];
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            case 'address':
                foreach ($fieldData['fieldIds'] as $fieldId) {
                    $childEntry[] = $entry[$fieldId];
                }

                return implode('<br>', array_filter($childEntry));
                break;
            case 'email':
                if (is_array($fieldData['fieldIds'])) {
                    $fieldData = $entry[$fieldData['fieldIds'][0]];
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            case 'fileupload':
                $fileURLOutput = '';
                if (!empty($entry[$fieldData['fieldIds']])) {
                    if (json_decode($entry[$fieldData['fieldIds']])) {
                        $fieldData = json_decode($entry[$fieldData['fieldIds']]);

                        foreach ($fieldData as $fileURL) {
                            $fileURLOutput .= '<a href=' . $fileURL . '>' . basename($fileURL) . '</a></br>';
                        }
                    } else {
                        $fileURLOutput .= '<a href=' . $entry[$fieldData['fieldIds']] . '>' . basename($entry[$fieldData['fieldIds']]) . '</a></br>';
                    }

                    return $fileURLOutput;
                }
                break;
            case 'list':
                if (!empty($entry[$fieldData['fieldIds']])) {
                    $listArray = unserialize($entry[$fieldData['fieldIds']]);
                    if (is_array($listArray[0])) {
                        $keys = array_keys($listArray[0]);
                        $listOutput = self::listTemplate($listArray, $keys);
                    } else {
                        $listOutput = implode('<br>', array_filter($listArray));
                    }

                    return $listOutput;
                }
                break;
            case 'post_tags':
                if (is_array($fieldData['fieldIds'])) {
                    $childEntry = array();
                    foreach ($fieldData['fieldIds'] as $fieldId) {
                        $childEntry[] = $entry[$fieldId];
                    }

                    $fieldData = implode(', ', array_filter($childEntry));
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            case 'post_category':
                $allPostCategories = array();
                $fieldData = $entry[$fieldData['fieldIds']];
                $fieldData = explode(',', $fieldData);
                foreach ($fieldData as $postCategory) {
                    $postCategory = substr($postCategory, 0, strpos($postCategory, ':'));
                    $allPostCategories[] = $postCategory;
                }
                $allPostCategories = implode(', ', $allPostCategories);

                return $allPostCategories;
                break;
            case 'post_image':
                $fieldData = $entry[$fieldData['fieldIds']];
                $fieldData = substr($fieldData, 0, strpos($fieldData, '|:|'));
                $fieldData = '<img src=' . $fieldData . '>';

                return $fieldData;
                break;
            case 'post_custom_field':
                if (is_array($fieldData['fieldIds'])) {
                    $childEntry = array();
                    foreach ($fieldData['fieldIds'] as $fieldId) {
                        $childEntry[] = $entry[$fieldId];
                    }

                    $fieldData = implode(', ', array_filter($childEntry));
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            case 'created_by':
                $fieldData = get_user_by('id', $entry[$fieldData['fieldIds']])->user_login;

                return $fieldData;
                break;
            case 'product':
                if (is_array($fieldData['fieldIds'])) {
                    $childEntry = array();
                    foreach ($fieldData['fieldIds'] as $fieldId) {
                        $childEntry[] = $entry[$fieldId];
                    }
                    $fieldData = implode(' - ', array_filter($childEntry));
                } elseif (strpos($entry[$fieldData['fieldIds']], '|')) {
                    $fieldData = str_replace('|', ' - ', $entry[$fieldData['fieldIds']]);
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            case 'option':
                if (is_array($fieldData['fieldIds'])) {
                    $childEntry = array();
                    foreach ($fieldData['fieldIds'] as $fieldId) {
                        $childEntry[] = str_replace('|', ' - ', $entry[$fieldId]);
                    }
                    $fieldData = implode('<br>', array_filter($childEntry));
                } elseif (strpos($entry[$fieldData['fieldIds']], '|')) {
                    $fieldData = str_replace('|', ' - ', $entry[$fieldData['fieldIds']]);
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            case 'shipping':
                if (strpos($entry[$fieldData['fieldIds']], '|')) {
                    $fieldData = str_replace('|', ' - ', $entry[$fieldData['fieldIds']]);
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            case 'number':
            case 'total':
                $numberFormat = get_option('wdtNumberFormat') ? get_option('wdtNumberFormat') : 1;

                if ($numberFormat == 1) {
                    $fieldData = str_replace('.', ',', $entry[$fieldData['fieldIds']]);
                } else {
                    $fieldData = $entry[$fieldData['fieldIds']];
                }

                return $fieldData;
                break;
            // for field types: text, html, date, select, radio, phone, website, post_title, post_content, post_excerpt, quantity
            default:
                $fieldData = $entry[$fieldData['fieldIds']];

                return $fieldData;
                break;
        }

    }

    /**
     * Include template for list field table
     * @param $listArray
     * @param $keys
     * @return mixed
     */
    public static function listTemplate($listArray, $keys) {
        ob_start();
        include WDT_GF_ROOT_PATH . 'templates/list_field.inc.php';
        $gravityListField = apply_filters('wdt_gravity_list_field', ob_get_contents());
        ob_end_clean();

        return $gravityListField;
    }

    /**
     * Function that extend table config before saving table to the database
     *
     * @param $tableConfig - array that contains table configuration
     *
     * @return mixed
     */
    public static function extendTableConfig($tableConfig) {

        if ($tableConfig['table_type'] !== 'gravity') {
            return $tableConfig;
        }

        $gravityData = json_decode(
            stripslashes_deep($_POST['gravity'])
        );

        // Sanitize Gravity Config
        $gravityData = self::sanitizeGravityConfig($gravityData);

        $advancedSettings = json_decode($tableConfig['advanced_settings']);
        $advancedSettings->gravity = array(
            'dateFilterLogic' => $gravityData->dateFilterLogic,
            'dateFilterFrom' => $gravityData->dateFilterFrom,
            'dateFilterTo' => $gravityData->dateFilterTo,
            'dateFilterTimeUnits' => $gravityData->dateFilterTimeUnits,
            'dateFilterTimePeriod' => $gravityData->dateFilterTimePeriod,
            'showDeletedRecords' => $gravityData->showDeletedRecords
        );

        $tableConfig['advanced_settings'] = json_encode($advancedSettings);

        return $tableConfig;
    }

    /**
     * Get field display headers
     * @param $formId - Gravity Form ID
     * @param $fieldsIds - IDs of the fields to fetch labels
     * @return array of columns headers (field labels)
     */
    public static function getColumnHeaders($formId, $fieldsIds) {
        $form = \GFAPI::get_form($formId);
        $columnHeaders = array();

        foreach ($form['fields'] as $formField) {
            if (in_array((int)$formField->id, $fieldsIds)) {

                $originalHeader = \WDTTools::generateMySQLColumnName($formField['label'], $columnHeaders);

                if (empty($columnHeaders[$originalHeader])) {
                    $columnHeaders[$originalHeader] = $formField->label;
                }
            }
        }

        foreach (array_intersect($fieldsIds, ['date_created', 'id', 'created_by', 'ip']) as $commonField) {
            if ($commonField === 'date_created') {
                $columnHeaders['datecreated'] = 'Entry Date';
            } else if ($commonField === 'id') {
                $columnHeaders['id'] = 'Entry ID';
            } else if ($commonField === 'created_by') {
                $columnHeaders['createdby'] = 'User';
            } else {
                $columnHeaders['ip'] = 'User IP';
            }
        }

        return $columnHeaders;
    }

}
