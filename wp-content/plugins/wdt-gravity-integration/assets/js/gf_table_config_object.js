/**
 * wpDataTable Gravity Form config object
 *
 * Contains all the settings for the gravity form based wpDataTable.
 * setter methods adjust the binded jQuery elements
 *
 * @author Milos Timotic
 * @since 14.07.2017
 */
var wpdatatable_gf_config = {
    dateFilterLogic: null,
    dateFilterFrom: null,
    dateFilterTo: null,
    dateFilterTimeUnits: null,
    dateFilterTimePeriod: null,
    fields: null,
    formId: null,
    showDeletedRecords: 0,
    setDateFilterLogic: function (logic) {
        wpdatatable_gf_config.dateFilterLogic = logic;
        jQuery('#wdt-gf-date-filter-logic').selectpicker('val', logic);
    },
    setDateFilterFrom: function (dateFrom) {
        wpdatatable_gf_config.dateFilterFrom = dateFrom;
        jQuery('#wdt-gf-date-filter-from').val(dateFrom);
    },
    setDateFilterTo: function (dateTo) {
        wpdatatable_gf_config.dateFilterTo = dateTo;
        jQuery('#wdt-gf-date-filter-to').val(dateTo);
    },
    setDateFilterTimeUnits: function (timeUnits) {
        wpdatatable_gf_config.dateFilterTimeUnits = timeUnits;
        jQuery('#wdt-gf-date-filter-time-units').val(timeUnits);
    },
    setDateFilterTimePeriod: function (timePeriod) {
        wpdatatable_gf_config.dateFilterTimePeriod = timePeriod;
        jQuery('#wdt-gf-date-filter-time-period').selectpicker('val', timePeriod);
    },
    setFields: function (fields) {
        wpdatatable_gf_config.fields = fields;
        jQuery('#wdt-gravity-form-column-picker').selectpicker('val', fields);
    },
    setFormId: function (formId) {
        wpdatatable_gf_config.formId = formId;
        jQuery('#wdt-gravity-form-picker').selectpicker('val', formId);
    },
    setShowDeletedRecords: function (showDeletedRecords) {
        wpdatatable_gf_config.showDeletedRecords = showDeletedRecords;
        jQuery('#wdt-gf-toggle-deleted-records').prop('checked', showDeletedRecords);
    },
    getGFConfig: function () {
        return {
            dateFilterLogic: wpdatatable_gf_config.dateFilterLogic,
            dateFilterFrom: wpdatatable_gf_config.dateFilterFrom,
            dateFilterTo: wpdatatable_gf_config.dateFilterTo,
            dateFilterTimeUnits: wpdatatable_gf_config.dateFilterTimeUnits,
            dateFilterTimePeriod: wpdatatable_gf_config.dateFilterTimePeriod,
            fields: wpdatatable_gf_config.fields,
            formId: wpdatatable_gf_config.formId,
            showDeletedRecords: wpdatatable_gf_config.showDeletedRecords
        };
    }
};