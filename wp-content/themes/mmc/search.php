<?php

  /**
   * Search results page
   */

  $context['title'] = 'Search results for '. get_search_query();
  $context['posts'] = Timber::get_posts(false, 'MMC\\Post');
  $context['pagination'] = Timber::get_pagination();

  \MMC\Template::render( array( 'search.twig', 'archive.twig', 'index.twig' ), $context );
