<?php

  /**
   * The template for displaying all pages.
   */

  $post = \Timber::get_post('MMC\\Post');
  $context['post'] = $post;

  if ( post_password_required( $post->ID ) )
  {
    \MMC\Template::render( 'single-password.twig', $context );
  }
  else
  {
    $views = array( 'pages/' . $post->post_name . '.twig', 'index.twig' );

    if ( is_front_page() )
    {
      $images = get_field( 'images', $post->ID );
      shuffle( $images );
      $context[ 'home_images' ] = $images;
      $context[ 'home_images_mobile' ] = array_slice( $images, 0, 3 );

      array_unshift( $views, 'front-page.twig' );
    }

    $feature = get_field( 'post_feature_post', 'options' );

    global $wp_query;
    $args = $wp_query->query;
    $args[ 'post__not_in' ] = array( $feature );

    query_posts( $args );

    $pagination = \Timber::get_pagination();

    $context[ 'posts' ] = \Timber::get_posts( false, 'MMC\\Post' );
    $context[ 'total_page' ] = $pagination[ 'total' ];
    $context[ 'current_page' ] = $pagination[ 'current' ];
    $context[ 'pagination' ] = $pagination;

    \MMC\Template::render( $views, $context );
  }
