<?php

  /**
   * The template for displaying 404 pages (Not Found)
   */


  $context = Timber::get_context();
  $views = array( '404.twig' );
  $post = Timber::get_post( 'MMC\\Post' );

  $context[ 'post' ] = $post;

  $content = get_field( 'notfound_error_message', 'option' );

  if ( empty( $content ) )
    $content = 'Sorry, the page you are looking for does not exist.';

  $imageID = get_field( 'notfound_error_image', 'option' );

  $context[ 'content' ] = $content;

  \MMC\Template::render( $views, $context );
