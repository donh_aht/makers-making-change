<?php

  /**
   * The template for displaying all pages.
   */

  $post = Timber::get_post('MMC\\Post');
  $context['post'] = $post;

  if ( post_password_required( $post->ID ) )
  {
    \MMC\Template::render( 'single-password.twig', $context );
  }
  else
  {
    $views = array('pages/' . $post->post_name . '.twig', 'page.twig');
    if ( is_front_page() )
    {
      $images = get_field( 'images', $post->ID );
      shuffle( $images );
      $context[ 'home_images' ] = $images;
      $context[ 'home_images_mobile' ] = array_slice( $images, 0, 3 );

      array_unshift( $views, 'front-page.twig' );
    }

    \MMC\Template::render( $views, $context );
  }
