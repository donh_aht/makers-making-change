<?php
/**
 * Search & Filter Pro 
 *
 * 843 Results Template
 *
 */

if ( $query->have_posts() )
{
	?>

	<div class="results-container">
	<?php
	while ($query->have_posts())
	{
		$query->the_post();

		$post_image_id = get_post_thumbnail_id( get_the_ID() );
		$post_image = wp_get_attachment_image_src($post_image_id, "medium");

		$post_categories = get_the_category();
		$category_links = "";
		foreach ($post_categories as $category){
			$category_links.= "<a href='?_sft_category=".$category->slug."'>".$category->name."</a>, ";
		}

		?>
		<div class="wrap-results">
			
			<a href="<?php the_permalink(); ?>"><img src="<?php echo $post_image[0] ?>"></a>
			<div class="resources-top">
				<a href="<?php the_permalink(); ?>"><h3 class="result-title"><?php the_title(); ?> </h3></a>
				<div class="result-meta"><?php the_date(); ?> | Categories: <?php echo rtrim($category_links, ', '); ?></div>
				<div class="result-content"><?php 
						$my_content = apply_filters( 'the_content', get_the_content() );
						$my_content = wp_strip_all_tags($my_content);
						echo wp_trim_words( $my_content, 25, $moreLink);
					?>	
				</div>
				<div class="read-more">
					<a href="<?php the_permalink(); ?>">Read More</a>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	</div>
	<div class="pagination">
		<div class="pagination-item">
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>

	</div>
	<?php
}
else
{
	echo "No Results Found";
}
?>