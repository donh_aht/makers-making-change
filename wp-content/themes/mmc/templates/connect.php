<?php

  /**
   * Template Name: Connect
   */

  global $paged, $wp_query, $wpdb;

  $context[ 'post' ] = \Timber::get_post('MMC\\Post');

  if ( !empty($_REQUEST['north']) && !empty($_REQUEST['south']) && !empty($_REQUEST['east']) && !empty($_REQUEST['west']) )
  {
    // set pagination options
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $ppp = intval(get_option('posts_per_page'));

    $result = \MMC\LocationSearch::searchIndex(array_merge($_REQUEST, array(
      'posts_per_page' => $ppp,
      'page' => $paged,
    )));

    if ( $result['posts_found'] == 0 ) {
      $context[ 'posts_found' ] = 'none';
    } else {
      $context[ 'posts_found' ] = $result['posts_found'];
    }

    $context['posts'] = $result['posts'];

    $connectStats = \MMC\LocationSearch::getConnectStats( $result['stats'] );
    $context['stats'] = $connectStats['label'];

    if ( $result['posts_found'] == 0 ) {
      $pageTitle = __( 'No results found' );
    } else {
      $pageTitle = $connectStats['page_title'];
    }

    $context['page_title'] = $pageTitle;

    // build custom query for Timber Pagination object
    $customQuery = (object) array(
      'query_vars' => array(
        'posts_per_page' => $ppp,
        'paged' => $paged,
      ),
      'found_posts' => $result['posts_found'],
    );

    // compute custom pagination
    $pagination = new \Timber\Pagination(array(
      'mid_size' => 1,
    ), $customQuery);
    $context['pagination'] = get_object_vars($pagination);

  } else
  {

    // if there is no search parameters, prompt the users to search for a postal code or city
    $context[ 'posts_found' ] = 'initial';

  }

  \MMC\Template::render( array( 'templates/connect.twig' ), $context );
