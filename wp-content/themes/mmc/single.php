<?php

  /**
   * The Template for displaying all single posts
   */

  $post = Timber::get_post('MMC\\Post');
  $context['post'] = $post;
  $context['comment_form'] = TimberHelper::get_comment_form();

  $user = get_userdata( $post->post_author );
  $context[ 'user' ] = $user;
  $connectPage = get_field( 'connect_page', 'options' );

  $context['categories'] = get_the_category($post->ID);
  $context['author'] = get_the_author_meta( 'display_name' , $post->post_author );
  $context['tags'] = get_the_tags($post->ID);

  $context[ 'connect' ] = array(
    'url' => $connectPage,
    'title' => __( 'Find a Volunteer Maker' ),
    // 'attr' => 'data-connect_user="' . $post->post_author . '" data-type="connect"',
  );

  if ( post_password_required( $post->ID ) )
  {
    \MMC\Template::render( 'single-password.twig', $context );
  }
  else
  {
    \MMC\Template::render( array( 'singles/' . $post->ID . '.twig', 'singles/' . $post->post_type . '.twig', 'single.twig'), $context );
  }
