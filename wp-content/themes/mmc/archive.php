<?php

  /**
   * The template for displaying Archive pages.
   *
   * Used to display archive-type pages if nothing more specific matches a query.
   * For example, puts together date-based pages if no date.php file exists.
   */

  $views = array('archive.twig', 'index.twig');
  $context = Timber::get_context();

  if ( is_day() )
  {
    $context[ 'title' ] = 'Archive: '.get_the_date( 'D M Y' );
  }
  elseif ( is_month() )
  {
    $context[ 'title' ] = 'Archive: '.get_the_date( 'M Y' );
  }
  elseif ( is_year() )
  {
    $context[ 'title' ] = 'Archive: '.get_the_date( 'Y' );
  }
  elseif ( is_tag() )
  {
    $context[ 'title' ] = single_tag_title('', false);
  }
  elseif ( is_category() )
  {
    $context[ 'title' ] = single_cat_title('', false);
    array_unshift( $views, 'category.twig');
    array_unshift( $views, 'categories/' . get_query_var('category_name') . '.twig' );
  }
  elseif ( is_tax() )
  {
    $term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
    $context[ 'title' ] = $term->name;
    array_unshift( $views, 'archives/' . get_query_var('taxonomy') . '.twig' );
  }
  elseif ( is_post_type_archive() )
  {
    $context[ 'title' ] = post_type_archive_title('', false);
    array_unshift( $views, 'archives/' . get_post_type() . '.twig' );
  }

  $postType = $post->post_type;

  if ( is_tax() || is_post_type_archive() ) {
    $objectQuery = $wp_query->query;
    $postType = $objectQuery['post_type'];
    array_unshift( $views, 'archives/' . $objectQuery['post_type'] . '.twig' );
  }

  switch ( $postType ) {
    case 'event':

      $results = \MMC\Events::customQuery();
      break;

    case 'project':

      $context[ 'title' ] = get_field( 'project_title', 'options' );
      $context[ 'subtitle' ] = get_field( 'project_subtitle', 'options' );

      $results = \MMC\Projects::customQuery();
      break;

    case 'success-story':

      $results = \MMC\SuccessStories::customQuery();
      break;

    default:
      # code...
      break;
  }

  if ( is_array( $results ) && isset( $results[ 'query_args' ] ) && !empty( $results[ 'query_args' ] ) ) {

    query_posts( $results[ 'query_args' ] );

    if ( !empty( $results[ 'page_title' ] ) )
    {
      if ( $wp_query->found_posts == 1 ) {
        $pageTitle = $wp_query->found_posts . ' ' . $results[ 'page_title' ][ 'singular' ];
      } else {
        $pageTitle = $wp_query->found_posts . ' ' . $results[ 'page_title' ][ 'plural' ];
      }

      $context[ 'page_title' ] = $pageTitle;
    }

  }

  $pagination = \Timber::get_pagination();
  $context[ 'post' ] = \Timber::get_post('MMC\\Post');

  if ( isset( $results[ 'archive' ] ) ) {
    $context[ 'posts' ] = $results[ 'archive' ];
  } else {
    $context[ 'posts' ] = \Timber::get_posts( false, 'MMC\\Post' );
  }

  $context[ 'total_page' ] = $pagination[ 'total' ];
  $context[ 'current_page' ] = $pagination[ 'current' ];
  $context[ 'pagination' ] = $pagination;

  $context[ 'query_var' ] = get_query_var( 's' );

  $context[ 'archive_url' ] = get_post_type_archive_link( $postType );

  $brandStatements = \MMC\Post::getBrandStatements();
  $testimonials = \MMC\Post::getTestimonials();
  $context[ 'make_build' ] = \MMC\Post::getCallToActions( 'make_this' );
  $context[ 'want_build' ] = \MMC\Post::getCallToActions( 'want' );

  if ( $post->post_type == 'event' )
  {

    $twitterCount = 4 * $pagination[ 'total' ];

    $context[ 'twitter_count' ] = $twitterCount;
    $context[ 'twitter_feed_opts' ] = array(
      'id' => "375",
      'count' => '"' . $twitterCount . '"',
    );

  }

  if ( !empty( $brandStatements ) )
  {
    if ( count( $brandStatements ) < $pagination[ 'total' ] )
    {
      for ( $i = 0; $i < $pagination[ 'total' ]; $i++ )
      {
        $brandStatements_new[] = $brandStatements[ $i % count( $brandStatements ) ];
      }
    } else
    {
      $brandStatements_new = $brandStatements;
    }

    $context[ 'brand_statements' ] = $brandStatements_new;
  }

  if ( !empty( $testimonials ) )
  {
    if ( count( $testimonials ) < $pagination[ 'total' ] )
    {
      for ( $i = 0; $i < $pagination[ 'total' ]; $i++ )
      {
        $testimonials_new[] = $testimonials[ $i % count( $testimonials ) ];
      }
    } else
    {
      $testimonials_new = $testimonials;
    }

    $context[ 'testimonials' ] = $testimonials_new;
  }

  \MMC\Template::render( $views, $context );
