<?php

namespace MMC;

class SubmitIdea
{
  /**
   * User data:
   * - First Name
   * - Last Name
   * - Nickname -> {First Name}
   * - Display Name -> {First Name} {Last Name}
   * - Roles
   *
   * User Meta:
   * - Location
   *
   * ACF:
   * - Skills (user_skills)
   */

  private $formID = 7;

  // fields ids
  private $titleFieldID = 3;
  private $messageFieldID = 2;
  private $emailFieldID = 5;
  private $nameFieldID = 4;
  private $geolocationFieldID = 6;

  private $fieldUserMetaMap = array();

  public $titleMetaKey = 'post_title';
  public $messageMetaKey = 'post_content';
  public $emailMetaKey = 'user_email';
  public $nameMetaKey = 'display_name';
  public $geolocationMetaKey = 'geolocation';

  public function __construct()
  {
    // field IDs <--> User meta map
    $this->fieldUserMetaMap[$this->titleFieldID] = $this->titleMetaKey;
    $this->fieldUserMetaMap[$this->messageFieldID] = $this->messageMetaKey;
    $this->fieldUserMetaMap[$this->emailFieldID] = $this->emailMetaKey;
    $this->fieldUserMetaMap[$this->nameFieldID] = $this->nameMetaKey;
    $this->fieldUserMetaMap[$this->geolocationFieldID] = $this->geolocationMetaKey;

    add_filter( 'gform_pre_render_' . $this->formID, array( '\\MMC\\GravityForms', 'gform_validation__redirectToLogin' ) );
    // add_filter( 'gform_validation_' . $this->formID, array( '\\MMC\\GravityForms', 'gform_validation__redirectToLogin' ) );

    add_filter( 'gform_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
    add_filter( 'gform_pre_validation_' . $this->formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
    add_filter( 'gform_admin_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );

    // Note: this will allow for the labels to be used during the submission process in case values are enabled
    add_filter( 'gform_pre_submission_filter_' . $this->formID, array( &$this, 'gform__populateFields' ), 10 );

    add_filter( 'gform_post_data_' . $this->formID, array( &$this, 'gform_post_data__createPostForIdea' ), 10, 3 );

    add_filter( 'publish_idea', array(&$this, 'wp_publish_idea_hook'), 99, 2 );

  } /* __construct() */

  // function gform_validation( $validation_result ) {
  //   $userID = get_current_user_id();
  //   if (!$userID) {
  //     wp_redirect(site_url('/login/'));
  //     exit();
  //   }

  //   return $validation_result;
  // }

  /**
   * populate the project title field
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function gform__populateFields( $form )
  {

    $userID = get_current_user_id();
    if (!$userID) {
      return;
    }

    $userObj = new \WP_User($userID);
    $userData = array( 'ID' => $userID );

    foreach ( $form['fields'] as &$field )
    {

      switch ( $field->id ) {
        case $this->titleFieldID:

          $args = array(
            'posts_per_page' => -1,
            'post_type' => 'project',
          );

          $posts = get_posts( $args );
          $choices = array();
          $field->choices = array();

          foreach ( $posts as $post )
          {
            $choices[] = array(
              'text' => $post->post_title,
              'value' => $post->ID,
            );
          }

          $field->choices = $choices;
          $field->placeholder = 'Project Title';

          break;

        case $this->emailFieldID:

          $field->defaultValue = $userObj->data->user_email;

          break;

        case $this->nameFieldID:

          $field->defaultValue = $userObj->data->display_name;

          break;

        case $this->geolocationFieldID:

          $field->defaultValue = json_encode( get_usermeta( $userID, 'geolocation' ) );

          break;

        default:
          # code...
          break;
      }
    }

    return $form;

  }/* gform_field__projectTitle() */


  /**
   * set the data for the idea
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $form
   * @return
   */

  public function gform_post_data__createPostForIdea( $postData, $form, $entry )
  {

    /**
     * $entry
     * 3 = name of idea
     * 2 = message
     */

    $postData[ 'post_type' ] = 'idea';
    $postData[ 'post_content' ] = $entry[ 2 ];

    return $postData;

  }/* gform_post_data__createPostForIdea() */

  public function wp_publish_idea_hook($postID, $postObj)
  {
    ForumIntegration::SubmitIdea($postId, $postObj);
  }

} /* class ClassName() */
