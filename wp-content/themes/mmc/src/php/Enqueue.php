<?php

  namespace MMC;

  class Enqueue
  {

    /**
     * Set up actions and filters
     */
    public function __construct()
    {
      add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

      add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_styles' ) );

      add_action( 'acf/input/admin_enqueue_scripts', array( &$this, 'admin_enqueue_acf_scripts' ) );

      add_action( 'admin_enqueue_scripts', array( &$this, 'admin_enqueue_scripts' ) );

      add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_styles' ) );

    }/* __construct() */


    /**
     * Enqueue Scripts
     */
    public function wp_enqueue_scripts()
    {

      $distUrl = get_template_directory_uri();

      wp_enqueue_script( 'wp-api' );
      wp_register_script( 'mmc-scripts', $distUrl . '/dist/scripts/public.js', array( 'jquery', 'wp-api' ), null, true );

      $resources = get_stylesheet_directory_uri() . '/dist/';

      if ( !file_exists( $resources ) ) {
        $resources = get_stylesheet_directory_uri() . '/src/';
      }

      wp_localize_script( 'mmc-scripts', 'mmc_global', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'resources' => $resources,
        'env' => Utils::getEnv(),
      ));

      wp_enqueue_script( 'mmc-scripts' );

      $template = get_page_template_slug( get_the_ID() );
      if ( strpos( $template, 'connect' ) > -1 ) {
        wp_enqueue_script( 'mmc-locations', home_url() . '/connect/?json=true', array( 'mmc-scripts' ), null, true );
      }

    }/* wp_enqueue_scripts() */



    /**
     * Enqueue Scripts
     */
    public function wp_enqueue_styles()
    {
      $distUrl = get_template_directory_uri();

      wp_enqueue_style( 'mmc-styles', $distUrl . '/dist/styles/public.css' );

    }/* wp_enqueue_styles() */

    public function admin_enqueue_acf_scripts()
    {
      $distUrl = get_template_directory_uri();

      wp_enqueue_script( 'mmc-admin-acf-scripts', $distUrl . '/dist/scripts/adminACF.js', array('jquery'), null, true );

    }/* admin_enqueue_acf_scripts */

    public function admin_enqueue_scripts()
    {
      $distUrl = get_template_directory_uri();

      wp_register_script( 'mmc-admin-scripts', $distUrl . '/dist/scripts/admin.js', array('jquery'), null, true );

      wp_localize_script( 'mmc-admin-scripts', 'mmc_global', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' )
      ));

      wp_enqueue_script( 'mmc-admin-scripts' );

    }/* admin_enqueue_scripts */

    public function admin_enqueue_styles()
    {
      $distUrl = get_template_directory_uri();

      wp_enqueue_style( 'mmc-admin-styles', $distUrl . '/dist/styles/admin.css' );

    }/* admin_enqueue_styles */

  }/* class Enqueue */


