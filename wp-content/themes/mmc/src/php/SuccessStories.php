<?php

namespace MMC;

class SuccessStories {


  /**
   * get custom query for the archive
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function customQuery()
  {

    global $wp_query;
    $args = $wp_query->query;

    $feature = get_field( 'success_story_feautre_post', 'options' );
    $args[ 'post__not_in' ] = array( $feature );

    $data = [
      'query_args' => $args,
    ];

    return $data;

  }


}
