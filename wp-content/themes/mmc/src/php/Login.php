<?php

  namespace MMC;

  class Login
  {
    const USER_DATA_COOKIE = 'mmc_user_data';

    private $profileFormIDs = array(
      5, // edit profile form
      8, // registration form
    );

    public function __construct()
    {
      add_action( 'wp_head', array( $this, 'wp_head__loginCheckScript' ) );
      add_action( 'set_logged_in_cookie', array( $this, 'set_logged_in_cookie__setUserDataCookie' ), 10, 5 );
      add_action( 'clear_auth_cookie', array( $this, 'clear_auth_cookie__clearUserDataCookie' ) );
      add_filter( 'body_class', array( $this, 'body_class__removeLoggedInClass' ), 10, 2 );

      // gravity forms integration - regenerate login cookie after profile form submission
      foreach ($this->profileFormIDs as $formID) {
        add_action( 'gform_after_submission_' . $formID, array( $this, 'gform_after_submission__resetUserDataCookie' ), 99 );
      }

      // add_shortcode( 'wordpress_social_login', array( &$this, 'wordpress_social_login_fix' ) );

    }

    public function wordpress_social_login_fix( $attributes, $content ) {
      ob_start();
      wsl_render_login_form();
      return ob_get_clean();
    }

    public function wp_head__loginCheckScript()
    {
      $loginCheckScript = file_get_contents(dirname(dirname(__DIR__)) . '/public/login-check.js');
      echo "\n<script>\n" . $loginCheckScript . "</script>\n";

    }/* wp_head__loginCheckScript() */

    public function set_logged_in_cookie__setUserDataCookie( $logged_in_cookie, $expire, $expiration, $user_id, $cookie_id )
    {
      $user = get_user_by('id', $user_id);
      $avatarUrl = Utils::get_user_avatar_by( $user_id );
      $roles = Roles::getUserMMCRoles($user_id);
      $profile_completed = get_user_meta($user_id, 'profile_completed', true);

      if (user_can( $user_id, 'manage_options' )) {
        array_push($roles, 'admin');
      }

      $userDataCookie = [
        $expire,
        $expiration,
        $cookie_id,
        $user->user_login,
        $user->display_name,
        $avatarUrl,
        join(',', $roles),
        $user->user_email,
        $user->ID,
        $profile_completed,
        $user->first_name,
        $user->last_name,
      ];

      $userDataCookie = join('|', $userDataCookie);
      $userDataCookie = rawurlencode($userDataCookie);

      // Front-end cookie is secure when the auth cookie is secure and the site's home URL is forced HTTPS.
      $secure = is_ssl() && 'https' === parse_url( get_option( 'home' ), PHP_URL_SCHEME );

      setcookie( self::USER_DATA_COOKIE, $userDataCookie, $expire, COOKIEPATH, COOKIE_DOMAIN, $secure, false );

    }/* set_logged_in_cookie__setUserDataCookie() */

    public function gform_after_submission__resetUserDataCookie()
    {
      $userId = get_current_user_id();
      if (!$userId) {
        return;
      }

      $loggedInCookie = $_COOKIE[self::USER_DATA_COOKIE];
      if (empty($loggedInCookie)) {
        return;
      }

      $loggedInCookieArray = explode('|', rawurldecode($loggedInCookie));
      $expire = intval($loggedInCookieArray[0]);
      $expiration = intval($loggedInCookieArray[1]);
      $cookie_id = $loggedInCookieArray[2];

      $this->set_logged_in_cookie__setUserDataCookie($loggedInCookie, $expire, $expiration, $userId, $cookie_id);

    }/* gform_after_submission__resetUserDataCookie */

    public function clear_auth_cookie__clearUserDataCookie()
    {
      setcookie( self::USER_DATA_COOKIE,   ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );

    }/* clear_auth_cookie__clearUserDataCookie() */

    function body_class__removeLoggedInClass( $wp_classes, $extra_classes )
    {
        // List of the only WP generated classes that are not allowed
        $blacklist = array( 'logged-in', 'admin-bar' );

        // Filter the body classes
        $wp_classes = array_diff( $wp_classes, $blacklist );

        // Add the extra classes back untouched
        return array_merge( $wp_classes, (array) $extra_classes );

    }/* body_class__removeLoggedInClass() */

  }/* class Security() */
