<?php

namespace MMC;

class ProjectPostsController extends \WP_REST_Posts_Controller
{

  /**
   * list the projects
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $data
   * @return
   */

  public function get_items( $data )
  {

    // get the parameters/arguments of the query
    $params = $data->get_params();

    // set the default arguments for this controller
    $defaultArgs = array(
      'posts_per_page' => -1,
    );

    $args = array_merge( $defaultArgs, $params );

    $query = new \WP_Query( $args );

    // get the data
    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $postID = $query->post->ID;
      $query->post->fields = get_fields( $postID );

      $thumbnail = get_post_thumbnail_id( $postID );
      $query->post->thumbnail_image = get_the_post_thumbnail_url( $postID, 'card' );
      $query->post->thumbnail_title = get_post( $thumbnail )->post_title;
      $query->post->call_to_action_label = __( 'Edit Project' );
      $query->post->call_to_action_url = home_url( '/edit-project' );

    endwhile; wp_reset_postdata(); endif;

    $response = $this->prepare_response_for_collection( $query->posts );

    // return the data
    return $response;

  }/* alphabeticalHeadings() */

} /* class Customizer() */
