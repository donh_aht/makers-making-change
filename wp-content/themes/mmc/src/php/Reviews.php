<?php

namespace MMC;

class Reviews {

  public static $formID = 16;

  // field ids
  private $titleFieldID = 1;
  private $messageFieldID = 2;
  private $emailFieldID = 3;
  private $nameFieldID = 4;
  private $geolocationFieldID = 5;
  private $cityFieldID = 6;
  private $provinceFieldID = 7;
  private $phoneFieldID = 8;
  private $projectIDFieldID = 16;
  private $projectSelectFieldID = 9;
  private $projectTitleFieldID = 10;

  private $sustainabilityFieldID = 11;
  private $easeUseFieldID = 12;
  private $reliabilityFieldID = 13;
  private $experienceFieldID = 14;
  private $additionalNotesFieldID = 15;

  private $likertScales = [];
  private $metaFields = [];

  private $reviewTitleMetaKey = 'review_title';
  private $additionalNotesMetaKey = 'additional_notes';

  public function __construct() {

    if (  Utils::getEnv() != 'prod' ) {
      static::$formID = 15;
    }

    $this->likertScales = [
      'sustainability' => $this->sustainabilityFieldID,
      'easeUse' => $this->easeUseFieldID,
      'reliability' => $this->reliabilityFieldID,
      'experience' => $this->experienceFieldID,
    ];

    $this->metaFields = [
      $this->titleFieldID => $this->reviewTitleMetaKey,
      $this->additionalNotesFieldID => $this->additionalNotesMetaKey,
    ];

    add_filter( 'gform_pre_render_' . static::$formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
    add_filter( 'gform_pre_validation_' . static::$formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
    add_filter( 'gform_admin_pre_render_' . static::$formID, array( &$this, 'gform__populateFields' ) );

    // Note: this will allow for the labels to be used during the submission process in case values are enabled
    add_filter( 'gform_pre_submission_filter_' . static::$formID, array( &$this, 'gform__populateFields' ), 10 );

    add_action( 'gform_after_submission_' . static::$formID, array( &$this, 'gform_after_submission__createProjectComment' ), 10, 2 );

  }

  /**
   * create a comment to be connected to the current project
   *
   * @author Ynah Pantig <me@ynahpantig.com>
   */
  public function gform_after_submission__createProjectComment( $entry, $form ) {

    // TODO
    // Add a new comment to the selected project once the user hits submit.
    // It shouldn't matter if this is a stand-alone form, or it is filled up through a project.
    // The post_id should be pulled in from the form field.

    $user = get_user_by( 'email', $entry[ $this->emailFieldID ] );
    $postID = $entry[ $this->projectIDFieldID ];

    $args = [
      'comment_post_ID' => $postID, // to which post the comment will show up
      'comment_author' => $entry[ $this->nameFieldID ], //fixed value - can be dynamic
      'comment_author_email' => $entry[ $this->emailFieldID ], //fixed value - can be dynamic
      'comment_content' => $entry[ $this->messageFieldID ], //fixed value - can be dynamic
      'comment_type' => '',
      'comment_parent' => 0, //0 if it's not a reply to another comment; if it's a reply, mention the parent comment ID here
      'user_id' => $user->ID, //passing current user ID or any predefined as per the demand
    ];

    $likertValues = [];
    $fields = $form[ 'fields' ];

    // go through the fields to get the selected choice
    // the values coming from the likert scale field
    // is dynamic and we have no way of telling which value
    // is connected to the selected option
    foreach ( $fields as $field ) {
      // go through our likert array
      foreach ( $this->likertScales as $key => $fieldID ) {
        if ( $field->id == $fieldID ) {
          $choices = $field[ 'choices' ];

          // go through the choices to determine which of the choices
          // is the value of the entry
          foreach ( $choices as $choice ) {
            if ( in_array( $entry[ $fieldID ], $choice ) ) {
              $likertValues[ $key ] = $choice[ 'score' ];
            }
          }
        }
      }
    }

    $commentID = wp_new_comment( $args );

    // need to pass the comment object in order to update
    // fields within the comments
    $comment = get_comment( $commentID );

    // update the rating field for the comment
    foreach ( $likertValues as $key => $value) {
      update_field( 'ratings_' . $key, $value, $comment );
    }

    // update meta fields with data if available
    foreach ( $this->metaFields as $fieldID => $meta ) {
      if ( !empty( $entry[ $fieldID ] ) ) {
        update_field( $meta, $entry[ $fieldID ], $comment );
      }
    }

  }

  public function gform__populateFields( $form ) {

    $userID = get_current_user_id();
    if (!$userID) {
      $form['requireLoginMessage'] = GravityForms::promptUserLogin();
      return $form;
    }

    $userObj = new \WP_User($userID);
    $userData = array( 'ID' => $userID );
    $geolocation = get_usermeta( $userID, 'geolocation' );
    $postID = get_the_ID();
    $postType = get_post_type( $postID );

    foreach ( $form['fields'] as &$field )
    {

      switch ( $field->id ) {
        case $this->titleFieldID:

          $field->placeholder = 'Review Title';

          break;

        case $this->emailFieldID:

          $field->defaultValue = $userObj->data->user_email;

          break;

        case $this->nameFieldID:

          $field->defaultValue = $userObj->data->display_name;

          break;

        case $this->geolocationFieldID:

          $field->defaultValue = json_encode( $geolocation );

          break;

        case $this->cityFieldID:

          foreach ( $geolocation[ 'address_components' ] as $item ) {
            if ( in_array( 'locality', $item[ 'types' ] ) ) {
              $field->defaultValue = $item[ 'long_name' ];
            }
          }

          break;

        case $this->provinceFieldID:

          foreach ( $geolocation[ 'address_components' ] as $item ) {
            if ( in_array( 'administrative_area_level_1', $item[ 'types' ] ) ) {
              $field->defaultValue = $item[ 'long_name' ];
            }
          }

          break;

        case $this->projectSelectFieldID:

          $args = array(
            'posts_per_page' => -1,
            'post_type' => 'project',
            'post_status' => 'publish',
          );

          $posts = get_posts( $args );
          $choices = array();
          $field->choices = array();

          foreach ( $posts as $item )
          {
            $choices[] = array(
              'text' => $item->post_title,
              'value' => $item->ID,
              'isSelected' => $postID == $item->ID ? true : false,
            );

          }

          $field->choices = $choices;
          $field->placeholder = 'Project Title';

          if ( $postType == 'project' ) {
            $field->defaultValue = get_the_title( $postID );
          }

          break;

        case $this->projectIDFieldID:

          if ( $postType == 'project' ) {
            $field->defaultValue = $postID;
          }

          break;

        case $this->projectTitleFieldID:

          if ( $postType == 'project' ) {
            $field->defaultValue = get_the_title( $postID );
          }
          break;

      }

    }

    return $form;
  }
}
