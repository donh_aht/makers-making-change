<?php

namespace MMC;

class Admin
{
  protected $customStatus = [
    'not_approved' => [
      'label' => 'Not Approved',
      'public' => false,
    ]
  ];

  public function __construct(  )
  {

    add_action( 'do_meta_boxes', array( &$this, 'do_meta_boxes__removeMetaBoxes' ) );
    add_action( 'admin_head', array( &$this, 'admin_head__removeEditor' ), 10 );

    add_filter( 'manage_project_posts_columns', array( &$this, 'manage_post_columns__project' ) );
    add_filter( 'manage_event_posts_columns', array( &$this, 'manage_post_columns__event' ) );
    add_filter( 'manage_request_posts_columns', array( &$this, 'manage_post_columns__requestIdea' ) );
    add_filter( 'manage_idea_posts_columns', array( &$this, 'manage_post_columns__requestIdea' ) );

    add_filter( 'manage_posts_custom_column', array( &$this, 'manage_posts_custom_column__customColumns' ), 10, 2 );

    add_filter( 'init', array( &$this, 'init__registerCustomStatus' ) );
    add_action( 'post_submitbox_misc_actions', array( &$this, 'post_submitbox_misc_actions__addCustomStatus' ) );
    add_action( 'admin_footer-edit.php', array( &$this, 'admin_footer__customStatus' ) );
    add_filter( 'display_post_states', array( &$this, 'display_post_states__displayPostStatus' ) );

    add_action( 'admin_menu', array( &$this, 'admin_menu__createCustomAdminPage' ) );

    add_action( 'init', array( &$this, 'init__limitBackendAccess' ) );

    // if ( is_admin() && current_user_can( 'manage_options' ) )
    // {
    //   // add RUN API button in the admin bar
    //   add_action( 'admin_bar_menu', array( &$this, 'admin_menu__runAPI' ), 50 );
    // }

  }/* __construct() */


  /**
   * limit the access to the backend for admin users
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function init__limitBackendAccess(  )
  {

    if ( is_admin() && !current_user_can( 'administrator' ) && !( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {

      if ( is_user_logged_in() ) {
        wp_redirect( home_url( '/profile/' ) );
      } else {
        wp_redirect( home_url( '/login/' ) );
      }

      exit;
    }

  }


  /**
   * rename the feature image meta box to "grid image"
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function do_meta_boxes__removeMetaBoxes(  )
  {

    $postType = get_post_type();

    $removeFromPostType = array( 'page', 'acf-field', 'acf-field-group' );

    if ( in_array( $postType, $removeFromPostType ) )
    {
      remove_meta_box( 'postimagediv', $postType, 'side' );
    }

  }/* do_meta_boxes__removeMetaBoxes() */


  /**
   * remove the editor for pages
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function admin_head__removeEditor(  )
  {

    $postType = get_post_type();

    $removeFromPostType = array( 'page' );

    if ( in_array( $postType, $removeFromPostType ) )
    {
      remove_post_type_support( $postType, 'editor' );
    }

  }/* admin_head__removeEditor() */


  /**
   * manage columns for the project post type
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function manage_post_columns__project(  )
  {

    $columns = array(
      'cb' => '<input type="checkbox" />',
      'title' => 'Title',
      'author' => 'Author',
      'taxonomy-skill' => 'Skills',
      'taxonomy-need' => 'Needs',
      'taxonomy-stage' => 'Stage',
      'date' => 'Date'
    );

    return $columns;

  }/* manage_post_columns__project() */

  /**
   * manage columns for the event post type
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function manage_post_columns__event(  )
  {

    $columns = array(
      'cb' => '<input type="checkbox" />',
      'title' => 'Title',
      'event_date_start' => 'Start Date',
      'event_date_end' => 'End Date',
      'location' => 'Location',
      'date' => 'Date'
    );

    return $columns;

  }/* manage_post_columns__event() */
  /**
   * manage columns for the request post type
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function manage_post_columns__requestIdea(  )
  {

    $columns = array(
      'cb' => '<input type="checkbox" />',
      'title' => 'Title',
      'status' => 'Status',
      'user' => 'User',
      'location' => 'Location',
      'date' => 'Date'
    );

    return $columns;

  }/* manage_post_columns__requestIdea() */

  /**
   * manage the data for the columns
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $columnName, $postID
   * @return
   */

  public function manage_posts_custom_column__customColumns( $columnName, $postID )
  {

    switch ( $columnName )
    {

      case 'stage':

        $stage = get_field( 'stage', $postID );

        if ( !empty( $stage ) )
        {
          $content = '<strong class="status status--' . strtolower( $stage ) . '">' . $stage . '</strong>';
        } else
        {
          $content = '&mdash;';
        }

        break;

      case 'event_date_range':

        $content = get_field( 'date_range', $postID );

        break;

      case 'event_date_end':

        $date = strtotime( get_field( 'date_end', $postID ) );
        if ( !empty( $date ) )
        {
          $content = date( 'F j, Y', $date );
        } else {
          $content = '&mdash;';
        }

        break;

      case 'event_date_start':

        $date = strtotime( get_field( 'date_start', $postID ) );
        if ( !empty( $date ) )
        {
          $content = date( 'F j, Y', $date );
        } else {
          $content = '&mdash;';
        }

        break;

      case 'status':

        $arr = get_field_object( 'status', $postID );
        $value = get_field( 'status', $postID );
        $content = $arr[ 'choices' ][ $value ];

        break;

      case 'project_need_amount':

        $content = get_field( 'project_need_amount', $postID );

        break;

      case 'user':

        $content = get_the_author();

        break;

      case 'location':

        $content = get_field( 'location', $postID );

        break;

      case 'project_links':

        $links = get_field( 'links', $postID );

        $content = '';
        if ( !empty( $links ) )
        {
          foreach ( $links as $key => $link ) {
            if ( empty( $link[ 'link' ][ 'url' ] )) {
              continue;
            }

            $content .= $link[ 'link' ][ 'url' ] . ', ';
          }
        }

        break;

      default:
        break;
    }

    if ( !empty( $content ) )
    {
      echo $content;
    } else
    {
      // echo '&mdash;';
    }

  }/* manage_posts_custom_column__customColumns() */



  /**
   * register custom status
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function post_submitbox_misc_actions__addCustomStatus(  )
  {

    $script = '<script>

    jQuery(document).ready(function($){
        $( "select#post_status" ).append( "';

        foreach ( $this->customStatus as $slug => $args )
        {
          $script .= '<option value=\"' . $slug . '\" ' . selected( $slug, $post->post_status ) . '>' . $args['label'] . '</option>';
        }

        $script .= '" );
    });

    </script>';

    echo $script;

  }


  /**
   * register custom post status
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function init__registerCustomStatus(  )
  {

    $defaultArgs = [
      'public'                    => false,
      'exclude_from_search'       => false,
      'show_in_admin_all_list'    => true,
      'show_in_admin_status_list' => true,
      'private'                   => true,
      'label_count'               => _n_noop( 'Unread <span class="count">(%s)</span>', 'Unread <span class="count">(%s)</span>' ),
    ];

    foreach ( $this->customStatus as $key => $args )
    {
      $args = array_merge( $args, $defaultArgs );
      register_post_status( $key, $args );
    }

  }


  /**
   * add the custom status to the dropdown in the quickedit
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function admin_footer__customStatus(  )
  {

    $script = '<script>

    jQuery(document).ready( function() {
      jQuery( "select[name=\"_status\"]" ).append( "';

      foreach ( $this->customStatus as $slug => $args )
      {
        $script .= '<option value=\"' . $slug . '\">' . $args['label'] . '</option>';
      }

      $script .= '" );
    });

    </script>';

    echo $script;

  }

  /**
   * display the status in the title
   *
   * @author Ynah Pantig
   * @param $statuses
   * @return
   */

  public function display_post_states__displayPostStatus( $statuses )
  {

    global $post;

    if ( array_key_exists( $post->post_status, $this->customStatus ) ) {
      return array( $this->customStatus[ $post->post_status ][ 'label' ] );
    }

    return $statuses; // returning the array with default statuses

  }


  /**
   * create a custom admin page for tracking
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function admin_menu__createCustomAdminPage(  )
  {

    add_menu_page( 'Tracking', 'Tracking', 'manage_options', 'mmc_tracking', '\\MMC\\Tracking::buildPageMarkup', 'dashicons-chart-bar' );

  }


  /**
   * add the Run API button in the admin menu
   * and make sure to run the API when triggered
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function admin_menu__runAPI()
  {
    global $wp_admin_bar;

    $args = array(
      'id'    => 'map_api',
      'title' => 'Run Map API',
      'href'  => get_admin_url() . '?map_api=run',
    );

    $wp_admin_bar->add_menu( $args );

  }/* admin_menu__runAPI() */

} /* class Admin() */
