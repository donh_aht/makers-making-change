<?php

namespace MMC;

class GravityForms
{

  public static $requestFormID = 1;

  public function __construct()
  {

    add_filter( 'gform_ajax_spinner_url', array( &$this, 'gform_ajax_spinner_url__changeSpinner' ) );

    $this->gform_load_8();
    $this->gform_load_11();

    add_filter( 'gform_field_content', array( &$this, 'gform_field_content__updateSelectMarkup' ), 10, 3 );

    /**
     * Force all GravityForms to display for all users,
     * even if the form requires login.
     *
     * Note that users still need to be logged in to be able to
     * submit the forms, but the form will display for everyone
     * so that it can be cached.
     */
    add_filter( 'gform_pre_render', array( &$this, 'gform_pre_render__forceDisplay' ) );
    add_filter( 'gform_submit_button', array( &$this, 'gform_submit_button__forceDisplay' ), 10, 2 );

    add_filter( 'gform_field_css_class', [ &$this, 'gform_field_css_class__updateFieldContainer' ], 10, 3 );

  } /* __construct() */

  public static function gform_validation__redirectToLogin( $validation_result ) {

    $userID = get_current_user_id();
    if (!$userID || !is_user_logged_in() ) {
      wp_redirect(site_url('/login/'));
      exit();
    }

    return $validation_result;
  }

  /**
   * add classes to the field container li markup
   *
   * @author Ynah Pantig
   * @param $classes, $field, $form
   * @return (string) $classes
   */

  public function gform_field_css_class__updateFieldContainer( $classes, $field, $form )
  {

      $type = $field->type;

      switch ( $type ) {
          case 'radio':
          case 'checkbox':
              $class = 'is-checkbox-radio';
              break;

          case 'select':
              $class = 'is-select';
              break;

          case 'multiselect':
              $class = 'is-multi-select';
              break;

          case 'textarea':
              $class = 'is-textarea';
              break;

          case 'date':
          case 'time':
          case 'name':
              $class = 'is-complex';
              break;

          case 'email':
              $class = 'is-email is-field';
              break;

          default:
              $class = 'is-field';
              break;
      }

      $classes .= ' ' . $class;

      return $classes;

  }


  /**
   * all functions related to Edit Profile (Form ID 8)
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function gform_load_8()
  {

    add_action( 'gform_after_submission_8', array( &$this, 'gform_after_submission__resizeImageOnSubmit' ), 10, 2 );
    add_action( 'gform_after_submission_8', array( &$this, 'gform_after_submission__updateData' ), 10, 2 );

  }/* gform_load_8() */

  /**
   * all functions related to Reset Password (Form ID 11)
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function gform_load_11()
  {
    add_filter( 'gform_pre_render_11', array( &$this, 'gform_pre_render_11__checkResetPasswordKey' ) );
    add_filter( 'gform_field_validation_11_2', array( &$this, 'gform_field_validation_11_2__checkEmailExists' ), 10, 4 );
    add_filter( 'gform_field_validation_11_9', array( &$this, 'gform_field_validation_11_9__checkPasswordResetKey' ), 10, 4 );
    add_filter( 'gform_replace_merge_tags', array( &$this, 'gform_replace_merge_tags__addRandomPasswordMergeTag' ), 10, 7 );
    add_action( 'gform_after_submission_11', array( &$this, 'gform_after_submission_11__setNewUserPassword' ), 10, 4 );

  }/* gform_load_11() */


  public function passwordResetExpiredRedirect() {
    wp_redirect(site_url('/reset-password/?expired=true'));
    exit();
  }

  public function gform_pre_render_11__checkResetPasswordKey($form)
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      return $form;
    }

    $action = $_GET['action'];

    if ($action == 'rp') {
      // reset password
      $login = $_GET['login'];
      $user = get_user_by( 'login', $login );

      if (!$user || is_wp_error($user)) {
        $this->passwordResetExpiredRedirect();
      }

      $key = $_GET['key'];

      if (!PasswordReset::isPasswordResetKeyValid($user->ID, $key)) {
        $this->passwordResetExpiredRedirect();
      }
    }

    return $form;
  }

  public function gform_replace_merge_tags__addRandomPasswordMergeTag($text, $form, $entry, $url_encode, $esc_html, $nl2br, $format)
  {
    if (isset( $form['id'] ) && intval($form['id']) != 11) {
      return $text;
    }

    $custom_merge_tag = '{reset_password_link}';

    if ( strpos( $text, $custom_merge_tag ) === false ) {
        return $text;
    }

    $generatedKey = rgar( $entry, 6 );

    $email = rgar( $entry, 2 );
    $user = get_user_by( 'email', $email );
    $login = $user->user_login;

    $reset_password_link = site_url('/reset-password/?action=rp&key=' . $generatedKey . '&login=' . $login);
    $reset_password_link = '<a href="' . $reset_password_link . '" target="_blank">' . $reset_password_link . '</a>';
    $text = str_replace( $custom_merge_tag, $reset_password_link, $text );


    return $text;

  }/* gform_replace_merge_tags__addRandomPasswordMergeTag() */

  public function gform_after_submission_11__setNewUserPassword($entry, $form)
  {
    $action = rgar( $entry, 10 );

    if ($action == 'rp') {
      // reset password
      $login = rgar( $entry, 11 );
      $user = get_user_by( 'login', $login );

      if (!$user || is_wp_error($user)) {
        return;
      }

      $key = rgar( $entry, 12 );

      if (!PasswordReset::isPasswordResetKeyValid($user->ID, $key)) {
        return;
      }

      $password = rgar( $entry, 9 );

      PasswordReset::completePasswordResetProcess($user->ID, $password);
    } else {
      // save generated key in user metadata
      $email = rgar( $entry, 2 );
      $user = get_user_by( 'email', $email );
      $generatedKey = rgar( $entry, 6 );

      PasswordReset::setPasswordResetKey($user->ID, $generatedKey);
    }

  }/* gform_after_submission_11__setNewUserPassword() */


  public function gform_field_validation_11_9__checkPasswordResetKey($result, $value, $form, $field)
  {
    if ( !$result['is_valid'] ) {
      return $result;
    }

    $action = rgar( $entry, 10 );

    if ($action == 'rp') {
      // reset password
      $login = rgar( $entry, 11 );
      $user = get_user_by( 'login', $login );

      if (!$user || is_wp_error($user)) {
        $this->passwordResetExpiredRedirect();
      }

      $key = rgar( $entry, 12 );

      if (!PasswordReset::isPasswordResetKeyValid($user->ID, $key)) {
        $this->passwordResetExpiredRedirect();
      }
    }

    return $result;

  }/* gform_field_validation_11_9__checkPasswordResetKey() */

  public function gform_field_validation_11_2__checkEmailExists($result, $value, $form, $field)
  {
    if ( !$result['is_valid'] ) {
      return $result;
    }

    if (!email_exists( $value )) {
      $result['is_valid'] = false;
      $result['message']  = empty( $field->errorMessage ) ? 'There is no user registered with that email address.' : $field->errorMessage;
    }

    return $result;

  }/* gform_field_validation_11_2__checkEmailExists() */


  /**
   * change the spinner
   */

  public function gform_ajax_spinner_url__changeSpinner( $spinner )
  {
    $spinner = get_template_directory_uri() . '/public/images/ajax-loader.svg';

    return $spinner;

  }/* gform_ajax_spinner_url__changeSpinner() */



  /**
   * set the post data for the request
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $form
   * @return
   */

  public function gform_post_data__populateProjectTitle( $postData, $form, $entry )
  {

    /**
     * $entry
     * 1 = project title dropdown
     * 2 = message
     */

    $postTitle = get_the_title( $entry[ 1 ] );
    $postData[ 'post_title' ] = __( 'Request for' ) . ' ' . $postTitle;
    $postData[ 'post_type' ] = 'request';
    $postData[ 'post_content' ] = $entry[ 2 ];

    return $postData;

  }/* gform_post_data__populateProjectTitle() */


  /**
   * populate the checkboxes, radio buttons, and select fields
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function gform__populateFields( $form )
  {

    $skillsList = Post::getTaxonomy( 'skill' );
    foreach ( $skillsList as $key => $skill ) { // for each column

      $skills[] = array(
        'text' => $skill->name,
        'value' => $skill->name,
      );

    }

    $industriesList = Post::getTaxonomy( 'industry' );
    $industries = array();
    foreach ( $industriesList as $key => $industry ) { // for each column

      $industries[] = array(
        'text' => $industry->name,
        'value' => $industry->name,
      );

    }

    $stageList = Post::getTaxonomy( 'stage' );
    $stage = array();
    foreach ( $stageList as $key => $item ) { // for each column

      $stage[] = array(
        'text' => $item->name,
        'value' => $item->name,
      );

    }

    foreach ( $form['fields'] as &$field )
    {

      if ( ($field->inputName == 'project_needs' || $field->inputName == 'project_needs_2') ) {

        $needsList = Post::getTaxonomy( 'need' );
        $choices = array();
        $inputs = array();

        $inputID = 1;
        foreach ( $needsList as $key => $need ) { // for each column

          //skipping index that are multiples of 10 (multiples of 10 create problems as the input IDs)
          if ( $inputID % 10 == 0 ) {
              $inputID++;
          }

          $choices[] = array(
            'text' => $need->name,
            'value' => $need->name,
          );

          $inputs[] = array(
            'label' => $need->name,
            'id' => "{$field->id}.{$inputID}",
          );

          $inputID++;
        }

        $field->choices = $choices;
        $field->inputs = $inputs;

      } else if ( $field->inputName == 'stage' && !empty( $status ) ) {

        $field->choices = $status;

      } else
      {
        continue;
      }

    }

    return $form;

  }/* gform__populateFields() */


  public function gform_pre_render__forceDisplay( $form )
  {
    if (rgar( $form, 'requireLogin' )) {
      $form['requireLoginBkp'] = true;
      $form['requireLogin'] = false;
    }

    return $form;

  }/* gform_pre_render__forceDisplay() */

  public function gform_submit_button__forceDisplay( $button_input, $form )
  {
    if (rgar( $form, 'requireLoginBkp' )) {
      $form_id = $form['id'];
      $button_input .= wp_nonce_field( 'gform_submit_' . $form_id, '_gform_submit_nonce_' . $form_id, true, false );
    }

    return $button_input;

  }/* gform_submit_button__forceDisplay() */

  /**
   * append the user email's to the notification
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $notification, $form, $entry
   * @return
   */

  public function gform_notification__addUserEmail( $notification, $form, $entry )
  {

    if ( empty( $_REQUEST[ 'user_id' ] ) || !isset( $_REQUEST[ 'user_id' ] ) )
    {
      return $notification;
    }

    $userID = $_REQUEST[ 'user_id' ];
    $user = get_user_by( 'id', $userID );
    $userEmail = $user->user_email;

    $notification['to'] = get_option( 'admin_email' ) . ',' . $userEmail;
    $notification['message'] .= "n -- " . get_bloginfo( 'name' );

    return $notification;

  }/* gform_notification__addUserEmail() */


  /**
   * resize image on submit
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function gform_after_submission__resizeImageOnSubmit( $entry, $form )
  {

    //replace 14 with field ID of upload field
    $url = $entry[ 25 ];
    $parsed_url = parse_url($url);
    $path = $_SERVER['DOCUMENT_ROOT'] . $parsed_url['path'];
    $image = wp_get_image_editor($path);

    if ( ! is_wp_error( $image ) )
    {
      //replace 640,480 with desired dimensions.  false indicates not to crop image.
      $result = $image->resize( 200, 200, true );
      $result = $image->save( $path );
    }

  }/* gform_after_submission__resizeImageOnSubmit() */


  /**
   * update user data on form submit
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $entry, $form
   * @return
   */

  public function gform_after_submission__updateData( $entry, $form )
  {
    // TODO: add code to save fields, just like registration form.

  }/* gform_after_submission__updateData() */



  public static function getTaxonomyFieldChoices($tax)
  {
    $terms = Post::getTaxonomy($tax);
    $choices = array();

    foreach ( $terms as $slug => $term ) {
      $choices[] = array(
        // 'text' => Utils::cleanString( $term->name, ' ' ),
        'text' => $term->name,
        'value' => $term->term_id,
      );
    }

    return $choices;
  }

  public static function getFieldInputs($fieldID, $choices)
  {
    $inputs = array();

    foreach ( $choices as $key => $choice ) {
      $inputs[] = array(
        'id' => $fieldID . '.' . ($key + 1),
        'label' => $choice['text'],
      );
    }

    return $inputs;
  }


  /**
   * change the markup for the select so we can
   * make it accessible
   *
   * @author Ynah Pantig
   * @param $field_content, $field, $value
   * @return
   */

  public function gform_field_content__updateSelectMarkup( $field_content, $field, $value )
  {

    if ( is_admin() ) {
      return $field_content;
    }

    if ( $field->type == 'select' ) {

      $fieldIDHash = $field->formId . '_' . $field->id;
      $inputID = 'input_' . $fieldIDHash;
      $fieldID = 'field_' . $fieldIDHash;
      $labelID = 'label_' . $fieldIDHash;
      $listID = 'list_' . $fieldIDHash;

      $newFieldContent = '<div class="dqpl-field-wrap">';
      $newFieldContent .= '<label class="gfield_label dqpl-label" for="' . $inputID . '" id="' . $labelID . '">' . $field->label;

      if ( $field->isRequired ) {
        $newFieldContent .= '<span class="gfield_required"> * <span class="sr-only"> Required</span></span>';
      }

      $newFieldContent .= '</label>';

      if ( $field->description != '' ) {
        $newFieldContent .= '<div id="' . $fieldID . '_dmessage" class="gfield_description">';
        $newFieldContent .= $field->description;
        $newFieldContent .= '</div>';
      }

      $newFieldContent .= '<div class="ginput_container dqpl-select js-select-wrapper">';

      $newFieldContent .= '<div class="dqpl-combobox" role="combobox" tabindex="0" aria-autocomplete="none" aria-owns="' . $listID . '" aria-expanded="false" aria-labelledby="' . $labelID . '" aria-required="true">';

        $newFieldContent .= '<select name="input_' . $field->id . '" id="' . $inputID . '" style="display: none;">';
        foreach ( $field->choices as $choice ) {
          $newFieldContent .= '<option value="' . sanitize_title( $choice['value'] ) . '">' . $choice['text'] . '</option>';
        }
        $newFieldContent .= '</select>';

        $newFieldContent .= '<div class="dqpl-pseudo-value js-gf-select-value">';

          if ( $value == '' || is_array( $value ) ) {
            $newFieldContent .= __( 'Select' ) . ' ' . $field->label;
          } else {
            $newFieldContent .= $value;
          }

        $newFieldContent .=  '</div>';
      $newFieldContent .= '</div>';

      $newFieldContent .= '<ul id="' . $listID . '" aria-labelledby="' . $labelID . '" class="dqpl-listbox" role="listbox">';

      foreach ( $field->choices as $choice ) {
        $newFieldContent .= '<li id="' . sanitize_title( $choice['value'] ) . '" class="dqpl-option" role="option">' . $choice['text'] . '</li>';
      }

      $newFieldContent .= '</ul>';
      $newFieldContent .= '</div>';
      $newFieldContent .= '</div>';

      $field_content = $newFieldContent;

    }

    return $field_content;

  }


  /**
   * validate the location field
   *
   * @author Ynah Pantig
   * @param $geolocationFieldID, $result
   * @return
   */

  public function validateLocationField( $geolocationField, $result )
  {
    if ( empty( $geolocationField ) ) {
      $result['is_valid'] = false;
      $result['message'] = __( 'Please use a postal code or zip code for your location and select from one of the items on the list.' );
    }

    return $result;

  }


  /**
   * prompt user to login
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public static function promptUserLogin()
  {

    $markup = '';
    $markup .= '<div class="container prompt-login">';
    $markup .= '<p>Please login to access this form.</p>';
    $markup .= '<a href="' . home_url( '/login/' ) . '" class="btn btn--small btn--solid">Login</a>';
    $markup .= '</div>';

    return $markup;

  }



} /* class ClassName() */
