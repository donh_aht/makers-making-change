<?php

// MMC PHP

namespace MMC\REST;

require_once '__constants.php';

class REST
{
    public function __construct()
    {
        new ForumIntegration\Sync\Idea_Controller();
        new ForumIntegration\Sync\Project_Controller();
        new ForumIntegration\Sync\User_Controller();
    }
}
