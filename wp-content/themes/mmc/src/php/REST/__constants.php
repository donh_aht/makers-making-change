<?php

/**
 * REST Constants.
 */
define('MMC_REST_VENDOR', 'mmc');
define('MMC_REST_VERSION', '1');
define('MMC_REST_NAMESPACE', MMC_REST_VENDOR.'/v'.MMC_REST_VERSION);
