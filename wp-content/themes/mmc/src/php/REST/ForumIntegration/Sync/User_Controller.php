<?php

// MMC

namespace MMC\REST\ForumIntegration\Sync;

use GFAPI;
use GFCommon;
use MMC\ForumIntegration;
use WP_Error;
use WP_REST_Controller;
use WP_REST_Response;
use WP_REST_Server;

class User_Controller extends WP_REST_Controller
{
    protected $controller_route = '/forumintegration/sync/users';
    private $_filterUserKeys = [];
    private $_unsafeUserKeys = [];

    public function __construct()
    {
        // $this->_filterUserKeys = [
        //     'ID' => true,
        //     'nickname' => true,
        //     'user_login' => true,
        //     'user_nicename' => true,
        //     'user_email' => true,
        //     'entry_id' => true,
        //     'discourse_sso_user_id' => true,
        //     'discourse_username' => true,
        // ];
        $this->_unsafeUserKeys = [
            'session_tokens' => true,
            'user_pass' => true,
        ];

        add_action('rest_api_init', [$this, 'register_routes']);
    }

    /**
     * Register the routes for the objects of the controller.
     */
    public function register_routes()
    {
        register_rest_route(
            MMC_REST_NAMESPACE,
            $this->controller_route,
            [
                [
                    'methods' => 'GET',
                    'callback' => [$this, 'get_items'],
                    'permission_callback' => [$this, 'get_items_permissions_check'],
                    'args' => [
                        'number' => [
                            'default' => 1,
                            'validate_callback' => function ($param, $request, $key) {
                                return is_numeric($param);
                            },
                        ],
                        'page' => [
                            'default' => 1,
                            'validate_callback' => function ($param, $request, $key) {
                                return is_numeric($param);
                            },
                        ],
                    ],
                ],
            ]
        );
        register_rest_route(
            MMC_REST_NAMESPACE,
            $this->controller_route.'/count',
            [
                [
                    'methods' => 'GET',
                    'callback' => [$this, 'get_items_count'],
                    'permission_callback' => [$this, 'get_items_permissions_check'],
                    'args' => [],
                ],
            ]
        );
        register_rest_route(
            MMC_REST_NAMESPACE,
            $this->controller_route.'/(?P<id>[\d]+)',
            [
                [
                    'methods' => WP_REST_Server::READABLE,
                    'callback' => [$this, 'get_item'],
                    'permission_callback' => [$this, 'get_item_permissions_check'],
                    'args' => [
                        'context' => [
                            'default' => 'view',
                        ],
                    ],
                ],
                [
                    'methods' => WP_REST_Server::CREATABLE,
                    'callback' => [$this, 'create_item'],
                    'permission_callback' => [$this, 'create_item_permissions_check'],
                    'args' => $this->get_endpoint_args_for_item_schema(true),
                ],
                [
                    'methods' => WP_REST_Server::EDITABLE,
                    'callback' => [$this, 'update_item'],
                    'permission_callback' => [$this, 'update_item_permissions_check'],
                    'args' => $this->get_endpoint_args_for_item_schema(false),
                ],
            ]
        );
        register_rest_route(
            MMC_REST_NAMESPACE,
            $this->controller_route.'/schema',
            [
                'methods' => WP_REST_Server::READABLE,
                'callback' => [$this, 'get_public_item_schema'],
            ]
        );
    }

    /**
     * Create one item from the collection.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return WP_Error|WP_REST_Request
     */
    public function create_item($request)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        // get parameters from request
        $params = $request->get_params();

        if (!isset($params['id']) || !intval($params['id'])) {
            return new WP_Error(
                'missing-id',
                __('Missing user ID'),
                ['status' => 500, 'in_array' => isset($params['id'])]
            );
        }

        $userID = intval($params['id']);

        $userData = get_userdata($userID);
        $userMeta = get_user_meta($userID);
        $userMetaInfo = ForumIntegration::fixArray((array) $userMeta);

        $registration_entry = \GFAPI::get_entry(intval($userMetaInfo['entry_id']));

        if (is_a($registration_entry, 'WP_Error')) {
            return new WP_Error(
                'missing-registration-id',
                __('Missing user registration ID'),
                ['status' => 500, 'userID' => $params['id']]
            );
        }

        $user_pass = bin2hex(random_bytes(12));

        $signupResult = ForumIntegration::SignUpRegistration($registration_entry, $userData->user_login, $user_pass, $userData);

        $response = ['result' => 'OK', 'signupResult' => $signupResult];

        if (isset($signupResult['result']) && 'OK' != $signupResult['result']) {
            $response = $signupResult;
        }

        return new WP_REST_Response($response, 200);
    }

    /**
     * Delete one item from the collection.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return WP_Error|WP_REST_Request
     */
    public function delete_item($request)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        $item = $this->prepare_item_for_database($request);

        if (\function_exists('slug_some_function_to_delete_item')) {
            $deleted = slug_some_function_to_delete_item($item);
            if ($deleted) {
                return new WP_REST_Response(true, 200);
            }
        }

        return new WP_Error('cant-delete', __('message', 'text-domain'), ['status' => 500]);
    }

    /**
     * Get a collection of items.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return WP_Error|WP_REST_Response
     */
    public function get_items($request)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        $numbered = intval($request['number']);
        $paged = intval($request['page']);

        $items = []; //do a query, call another class, etc

        $query = [
            'fields' => ['ID', 'user_email', 'user_login'],
            'meta_key' => 'discourse_username',
            'meta_compare' => 'NOT EXISTS',
            'orderby' => ['ID' => 'DESC'],
            'number' => $numbered,
            'paged' => $paged,
        ];

        $users = get_users($query);

        foreach ($users as $userEntry) {
            // $items[] = [intval($userEntry->ID), $userEntry->user_email];
            $items[] = $userEntry;
        }

        $data = [];
        foreach ($items as $item) {
            $itemdata = $this->prepare_item_for_response($item, $request);
            $data[] = $this->prepare_response_for_collection($itemdata);
        }

        return new WP_REST_Response($data, 200);
    }

    /**
     * Get item count.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return WP_Error|WP_REST_Response
     */
    public function get_items_count($request)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        $items = []; //do a query, call another class, etc

        $query = [
            'fields' => ['ID'],
            'meta_key' => 'discourse_username',
            'meta_compare' => 'NOT EXISTS',
            'orderby' => ['ID' => 'DESC'],
            'number' => -1,
        ];

        $users = get_users($query);

        $items['count'] = count($users);

        $data = [];
        foreach ($items as $itemKey => $item) {
            $itemdata = $this->prepare_item_for_response($item, $request);
            $data[$itemKey] = $this->prepare_response_for_collection($itemdata);
        }

        $data = ForumIntegration::fixArray($data);

        return new WP_REST_Response($data, 200);
    }

    /**
     * Get one item from the collection.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return WP_Error|WP_REST_Response
     */
    public function get_item($request)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        // get parameters from request
        $params = $request->get_params();

        if (!isset($params['id']) || !intval($params['id'])) {
            return new WP_Error('missing-id', __('Missing user ID'), ['status' => 500, 'in_array' => isset($params['id'])]);
        }

        $userID = intval($params['id']);

        $userData = (array) get_userdata($userID);

        $userMetaData = (array) get_user_meta($userID);

        $dataSet = (array) $userData['data'];
        unset($userData['data']);

        $combinedData = array_merge($dataSet, $userData, $userMetaData);

        $combinedData = ForumIntegration::fixArray($combinedData);

        $item = $combinedData;
        if (count($this->_filterUserKeys)) {
            $item = array_intersect_key($combinedData, $this->_filterUserKeys);
        }
        if (count($this->_unsafeUserKeys)) {
            $item = array_diff_key($item, $this->_unsafeUserKeys);
        }

        // Prep data for response
        $data = $this->prepare_item_for_response($item, $request);

        //return a response or error based on some conditional
        if (1 === 1) {
            return new WP_REST_Response($data->data, 200);
        }

        return new WP_Error('code', __('message', 'text-domain'));
    }

    /**
     * Update one item from the collection.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return WP_Error|WP_REST_Request
     */
    public function update_item($request)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        // get parameters from request
        $params = $request->get_params();

        if (!isset($params['id']) || !intval($params['id'])) {
            return new WP_Error('missing-id', __('Missing user ID'), ['status' => 500, 'in_array' => isset($params['id'])]);
        }

        $userID = intval($params['id']);

        $userData = get_userdata($userID);

        $editProfileResult = ForumIntegration::EditProfile($userData);

        $userMetaUpdates = $params['user_meta'];

        $updateResult = [];

        foreach ($userMetaUpdates as $userMetaKey => $userMetaValue) {
            $updateResult[$userMetaKey] = update_user_meta($userID, $userMetaKey, $userMetaValue);
        }

        return new WP_REST_Response(['result' => 'OK', 'EditProfile' => $editProfileResult, 'user_meta' => $updateResult], 200);
    }

    /**
     * Check if a given request has access to get items.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return bool|WP_Error
     */
    public function get_items_permissions_check($request)
    {
        return $this->_check_permissions($request);
    }

    /**
     * Check if a given request has access to get a specific item.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return bool|WP_Error
     */
    public function get_item_permissions_check($request)
    {
        return $this->_check_permissions($request);
    }

    /**
     * Check if a given request has access to create items.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return bool|WP_Error
     */
    public function create_item_permissions_check($request)
    {
        return $this->_check_permissions($request);
    }

    /**
     * Check if a given request has access to update a specific item.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return bool|WP_Error
     */
    public function update_item_permissions_check($request)
    {
        return $this->_check_permissions($request);
    }

    /**
     * Check if a given request has access to delete a specific item.
     *
     * @param WP_REST_Request $request full data about the request
     *
     * @return bool|WP_Error
     */
    public function delete_item_permissions_check($request)
    {
        return $this->_check_permissions($request);
    }

    /**
     * Prepare the item for the REST response.
     *
     * @param mixed           $item    wordPress representation of the item
     * @param WP_REST_Request $request request object
     *
     * @return mixed
     */
    public function prepare_item_for_response($item, $request)
    {
        return new WP_REST_Response($item);
    }

    /**
     * Get the query params for collections.
     *
     * @return array
     */
    public function get_collection_params()
    {
        return [
            'page' => [
                'description' => 'Current page of the collection.',
                'type' => 'integer',
                'default' => 1,
                'sanitize_callback' => 'absint',
            ],
            'per_page' => [
                'description' => 'Maximum number of items to be returned in result set.',
                'type' => 'integer',
                'default' => 10,
                'sanitize_callback' => 'absint',
            ],
            'search' => [
                'description' => 'Limit results to those matching a string.',
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
            ],
        ];
    }

    /**
     * Undocumented function.
     *
     * @param [type] $request
     */
    protected function _check_permissions($request)
    {
        return current_user_can('integrator');
    }

    /**
     * Prepare the item for create or update operation.
     *
     * @param WP_REST_Request $request Request object
     *
     * @return object|WP_Error $prepared_item
     */
    protected function prepare_item_for_database($request)
    {
        return [];
    }

    private static function dump($data)
    {
        header('Content-Type: text/plain');
        die(json_encode($data, JSON_PRETTY_PRINT));
    }

    private static function getFormIDByName($form_title = '')
    {
        return intval(\RGFormsModel::get_form_id($form_title));
    }
}
