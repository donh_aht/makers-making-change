<?php

  namespace MMC;

  class Menu
  {

    /**
     * Set up actions and filters
     */
    public function __construct()
    {

      add_action( 'init', array( &$this, 'init__registerMenuLocations' ) );

    }/* __construct() */



    /**
     * register menu locations
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    public function init__registerMenuLocations(  )
    {

      register_nav_menus(
          array(
            'main' => __( 'Main Menu' ),
            'utils' => __( 'Utils' ),
            'footer' => __( 'Footer' ),
            'nav_left' => __( 'Menu left page' ),
          )
        );

    }/* init__registerMenuLocations() */


  }/* class Menu */


