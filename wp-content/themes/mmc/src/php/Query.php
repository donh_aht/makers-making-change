<?php

  namespace MMC;

  class Query
  {

    public function __construct(  )
    {

      add_action( 'pre_get_posts', array( &$this, 'pre_get_posts' ), 1 );

      add_action( 'found_posts', array( &$this, 'found_posts' ), 10, 2 );

      add_filter( 'rest_prepare_comment', array( &$this, 'rest_prepare_comment' ), 10, 3 );

    }/* __construct() */

    /**
     * events pre get posts
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $query
     * @return
     */

    public function pre_get_posts( $query )
    {

      if ( is_admin() ) {
        return;
      }

      if ( !is_post_type_archive() ) {
        return $query;
      }

      $paged = $query->query_vars[ 'paged' ];
      $postType = $query->get( 'post_type' );
      $postsPerPage = static::getPostsPerPage( $postType );

      if ( $postType != 'event' && $postType != 'success-story' ) {
        $query->set( 'posts_per_page', $postsPerPage );
      }

      switch ( $postType ) {
        case 'project':

          $feature = get_field( 'project_feature_post', 'options' );
          $query->set( 'post__not_in', array( $feature ) );
          $query->set( 'orderby', array( 'menu_order' => 'ASC', 'date' => 'DESC' ) );

          break;

        default:
          break;
      }

      return $query;

    }/* pre_get_posts() */


    /**
     * get the offset based on the post type
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $postType
     * @return
     */

    static public function getPostsPerPage( $postType )
    {

      switch ( $postType ) {
        case 'event':

          $postsPerPage = 12;
          break;

        case 'project':

          $postsPerPage = 11;
          break;

        default:

          $postsPerPage = 11;
          break;
      }

      return $postsPerPage;

    }/* getPostsPerPage() */

    /**
     * get the offset based on the post type
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $postType
     * @return
     */

    public function getQueryOffset( $postType, $paged )
    {

      switch ( $postType ) {
        case 'event':

          break;

        case 'project':

          // if ( $paged > 1 )
          // {
          //   $offset = 8 * ( ( $paged + 1 ) - 2 );
          // }

          break;

        default:
          # code...
          break;
      }

      if ( isset( $offset ) )
      {
        return $offset;
      }

    }/* getQueryOffset() */


    /**
     * found posts
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    public function found_posts( $found_posts, $query )
    {

      // if ( is_admin() || !$query->is_main_query() )
      // {
      //   return $found_posts;
      // }

      // $postType = $query->get( 'post_type' );

      // //Ensure we're modifying the right query object
      // if ( $postType == 'project' )
      // {
      //   $paged = $query->query_vars[ 'paged' ];

      //   // Define our offset again...
      //   $offset = $this->getQueryOffset( $paged, $postType );

      //   // check the minimum that we need to show
      //   if ( $found_posts > 8 )
      //   {
      //     // Reduce WordPress's found_posts count by the offset
      //     $found_posts = $found_posts + $offset;
      //   }

      // }

      return $found_posts;

    }/* found_posts() */


    /**
     * update the return response for the comments
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $response, $comment, $request
     * @return
     */

    public function rest_prepare_comment( $response, $comment, $request )
    {

      $data = $response->data;

      $data[ 'fields' ] = get_fields( $comment );
      $data[ 'post_type' ] = get_post_type( $data[ 'post' ] );

      $response->data = $data;

      return $response;

    }/* rest_prepare_comment() */


  }/* class Query */


