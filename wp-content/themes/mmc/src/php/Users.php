<?php

namespace MMC;

/**
 * Timber extensions
 */
class Users
{

  public $profileCompletedMetaKey = 'profile_completed';

  public function __construct()
  {

    // Block access from wp-login
    add_action( 'wp_logout', array( &$this, 'wp_logout__redirectAfterLogout' ) );

    add_action( 'wp_login', array( $this, 'wp_login__updateLastLoggedInDate' ), 10, 2 );
    add_action( 'wp_login', array( $this, 'wp_login__redirectToPost' ), 10, 2 );

    add_filter( 'wp_login', array( &$this, 'wp_login__redirectOnLogin' ), 10, 3 );

  }

  /**
   * redirect users as they login
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function wp_login__redirectOnLogin( $userLogin, $user )
  {

    $profile_completed = get_user_meta($user->ID, $this->profileCompletedMetaKey, true);

    if ($profile_completed) {
      $redirectTo = site_url('/profile/');
    } else {
      // $redirectTo = site_url('/sign-up/');
    }

    wp_redirect( $redirectTo );
    exit();

  }


  public function wp_logout__redirectAfterLogout()
  {
    wp_redirect(home_url('/login/'));
    exit();

  } /* wp_logout__redirectAfterLogout() */


  /**
   * get the latest usres
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  static public function latestMembers(  )
  {

    $args = array(
      'number' => 2,
      'orderby' => 'registered',
      'role' => 'maker',
    );

    $usersQuery = new \WP_User_Query( $args );
    $users = $usersQuery->get_results();

    foreach ( $users as $key => $user )
    {
      $userFieldID = 'user_' . $user->ID;

      $item = array();
      $item[ 'thumbnail' ] = Utils::get_user_avatar_by( $user->ID );
      $item[ 'post_title' ] = $user->display_name;
      $item[ 'ID' ] = $user->ID;
      $item[ 'user_email' ] = $user->user_email;
      $item[ 'post_type' ] = 'user';
      $item[ 'roles' ] = implode( ', ', Roles::getUserMMCRoles( $user->ID ) );

      $data[] = $item;
    }

    return $data;

  }/* latestMembers() */


  /**
   * get user connections
   *
   * @author Ynah Pantig
   * @package Users.php
   * @since 1.0
   * @param $userID
   * @return $data
   */

  static public function getUserConnections( $userID )
  {

    $userConnections = get_field( 'connection_users', 'user_' . $userID );

    if ( is_string( $userConnections ) ) {
      $userConnections = explode( ',', $userConnections );
    }

    if ( empty( $userConnections ) ) {
      die();
    }

    $connections = array();

    foreach ( $userConnections as $user )
    {
      $info = array();

      $userSkills = get_field( 'user_skills', 'user_' . $user['ID'] );
      $skills = array();

      foreach ( $userSkills as $id ) {
        $skill = get_term_by( 'id', $id, 'skill' );
        $skills[] = $skill->name;
      }

      if ( !empty( $skills ) ) {
        $skills = implode( ', ', $skills );
      }

      $info[ 'name' ] = $user['display_name'];
      $info[ 'fields' ] = array(
        'skills' => $skills,
        'thumbnail' => Utils::get_user_avatar_by( $user['ID'] ),
      );

      $connections[] = $info;
    }

    return $connections;

  }/* getUserConnections() */


  /**
   * get user reviews
   *
   * @author Ynah Pantig
   * @package Users.php
   * @since 1.0
   * @param $userID
   * @return $data
   */

  static public function getUserReviews( $userID )
  {

    $args = array(
      'author__in' => $userID,
      'post_type' => 'project',
      'status' => 'approve',
    );

    $comments = get_comments( $args );

    foreach ( $comments as &$comment )
    {
      $comment->fields = get_fields( 'comment_' . $comment->comment_ID );
    }

    return $comments;

  }/* getUserReviews() */


  /**
   * get user projects
   *
   * @author Ynah Pantig
   * @package Users.php
   * @since 1.0
   * @param $userID
   * @return $data
   */

  static public function getUserProjects( $userID )
  {

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'project',
      'author' => $userID,
      'post_status' => array( 'pending', 'publish' ),
    );

    $query = new \WP_Query( $args );

    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $post = $query->post;
      $postID = $query->post->ID;
      $query->post->fields = get_fields( $postID );

      $thumbnail = get_post_thumbnail_id( $postID );
      $query->post->thumbnail_image = get_the_post_thumbnail_url( $postID, 'card' );
      $query->post->thumbnail_title = get_post( $thumbnail )->post_title;
      $query->post->call_to_action_label = __( 'Edit Project' );
      $query->post->call_to_action_url = home_url( '/edit-project' );

      if ( $post->post_status == 'publish' )
      {
        $query->post->project_status = __( 'Approved' );
      } else {
        $query->post->project_status = __( 'Pending' );
      }

      // $stage = get_the_terms( $postID, 'stage' );
      // $query->post->term_status = $stage[0];

    endwhile; wp_reset_postdata(); endif;

    return $query->posts;

  }/* getUserProjects() */


  /**
   * get user requests
   *
   * @author Ynah Pantig
   * @package Users.php
   * @since 1.0
   * @param $userID
   * @return $data
   */

  static public function getUserRequests( $userID )
  {

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'request',
      'author' => $userID,
      'meta_query' => array(
        array(
          'key' => 'status',
          'value' => 'none',
        ),
      ),
    );

    $query = new \WP_Query( $args );

    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $postID = $query->post->ID;
      $query->post->fields = get_fields( $postID );
      $query->post->project_permalink = get_permalink( get_post_meta( $postID, 'project_id', true ) );

    endwhile; wp_reset_postdata(); endif;

    return $query->posts;

  }/* getUserRequests() */


  /**
   *
   * set the last login data for the user
   *
   * @author Ynah Pantig
   * @param $user_login, $user
   * @return
   */

  public function wp_login__updateLastLoggedInDate( $user_login, $user )
  {

    update_field( 'last_login_date', date('Ymd'), 'user_' . $user->ID );

  }


  /**
   * if there's a post ID in the URL, redirect the user to that post
   *
   * @author Ynah Pantig
   * @param $user_login, $user
   * @return
   */

  public function wp_login__redirectToPost( $user_login, $user )
  {

    if ( isset( $_REQUEST[ 'post_id' ] ) && !empty( $_REQUEST[ 'post_id' ] ) ) {
      $link = get_permalink( $_REQUEST[ 'post_id' ] );

      if ( isset( $_REQUEST[ 'comments' ] ) && !empty( $_REQUEST[ 'comments' ] ) ) {
        $link = '?comments=true';
      }

      wp_redirect( $link );
      exit();
    }

  }


} /* class Users() */
