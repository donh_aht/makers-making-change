<?php

namespace MMC;

/**
 * Timber extensions
 */
class Project
{

  public function __construct()
  {



  }


  /**
   * get the skills from the project post type and list
   *
   * @author Ynah Pantig
   * @package Project.php
   * @since 1.0
   * @param
   * @return $data
   */

  static public function getSkills(  )
  {

    $data = array();
    $args = array(
      'hide_empty' => false,
      'taxonomy' => 'skill',
    );

    $terms = new \WP_Term_Query( $args );

    foreach ( $terms->get_terms() as $term )
    {
      $data[ $term->slug ] = $term->name;
    }

    return $data;

  }/* getSkills() */


} /* class Project() */
