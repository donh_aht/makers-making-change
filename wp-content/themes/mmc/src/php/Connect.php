<?php

namespace MMC;

class Connect
{
  private static $tableName = 'mmc_connect';
  private static $tableCheck;

  private static $latLonTableName = 'mmc_lat_lon';
  private static $latLonTableCheck;

  public function __construct(  )
  {

    add_filter( 'template_include', array( &$this, 'template_include__useConnectTemplate' ) );

  }/* __construct() */

  /**
   * check if the projects table exists
   *
   * @author Ynah Pantig
   * @package Projects.php
   * @since 1.0
   * @param
   * @return $data
   */

  static private function checkProjectsTable()
  {

    global $wpdb;

    if ( !static::$tableCheck )
    {
      static::$tableCheck = true;

      $query = 'SHOW TABLES LIKE "' . static::$tableName . '"';
      $result = $wpdb->query( $query );

      // if the table exists, then don't do anything and return
      if ( $result > 0 )
      {
        return;
      }

      // if it doesn't, make sure to creat the table
      static::createConnectTable();
    }

  }/* checkProjectsTable() */


  /**
   * check if the latlon table exists
   *
   * @author Ynah Pantig
   * @package Projects.php
   * @since 1.0
   * @param
   * @return $data
   */

  static private function checkLatLonTable()
  {

    global $wpdb;

    if ( !static::$latLonTableCheck )
    {
      static::$latLonTableCheck = true;

      $query = 'SHOW TABLES LIKE "' . static::$latLonTableName . '"';
      $result = $wpdb->query( $query );

      // if the table exists, then don't do anything and return
      if ( $result > 0 )
      {
        return;
      }

      // if it doesn't, make sure to creat the table
      static::createLatLonTable();
    }

  }/* checkLatLonTable() */


  /**
   * create the projects table if it does not exist
   *
   * @author Ynah Pantig
   * @package Projects.php
   * @since 1.0
   * @param
   * @return $data
   */

  static private function createConnectTable()
  {

    global $wpdb;
    $table_name = static::$tableName;

    $sql = "CREATE TABLE " . static::$tableName . " (
      id INT(11) NOT NULL AUTO_INCREMENT,
      post_id INT(11) DEFAULT NULL NULL,
      postal_code VARCHAR(255) DEFAULT '' NULL,
      post_type VARCHAR(255) DEFAULT '' NULL,
      city VARCHAR(255) DEFAULT '' NULL,
      lat VARCHAR(255) DEFAULT '' NULL,
      lon VARCHAR(255) DEFAULT '' NULL,
      UNIQUE KEY id (id)
    ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

  }

  /**
   * create the projects table if it does not exist
   *
   * @author Ynah Pantig
   * @package Projects.php
   * @since 1.0
   * @param
   * @return $data
   */

  static private function createLatLonTable()
  {

    global $wpdb;

    $sql = "CREATE TABLE " . static::$latLonTableName . " (
      id INT(11) NOT NULL AUTO_INCREMENT,
      city VARCHAR(255) DEFAULT '' NULL,
      postal_code VARCHAR(255) DEFAULT '' NULL,
      lat VARCHAR(255) DEFAULT '' NULL,
      lon VARCHAR(255) DEFAULT '' NULL,
      UNIQUE KEY id (id)
    ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

  }


  /**
   * use the connect template even when there's a parameter
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function template_include__useConnectTemplate( $template )
  {

    if ( isset( $_REQUEST[ 'location' ] ) && !empty( $_REQUEST[ 'location' ] ) ) {
      $templateUrl = get_stylesheet_directory_uri() . '/templates/connect.php';
      $template = Utils::getAbspathUrl( $templateUrl );
    }

    return $template;


  }/* template_include__useConnectTemplate() */

  /**
   * get lat lon and add to the array
   *
   * @author Ynah Pantig
   * @package Projects.php
   * @since 1.0
   * @param $decoded
   * @return $data
   */

  static public function getLatLon( $decoded, $vars )
  {

    extract( $vars );
    $latlon = Utils::googleGeolocate( $city, $postal_code );

    if ( !empty( $latlon ) )
    {
      $decoded[ 'lat' ] = $latlon[ 'lat' ];
      $decoded[ 'lon' ] = $latlon[ 'lon' ];
    }

    return $decoded;

  }/* getLatLon() */


  /**
   * get the radius to search within
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $lat, $lon, $limit = 50, $distance = 50, $unit = 'km'
   * @return
   */

  static public function getRadiusValues( $lat, $lon, $distance = 50, $unit = 'km' )
  {

    // radius of earth; @note: the earth is not perfectly spherical, but this is considered the 'mean radius'
    if ( $unit == 'km' )
    {
      $radius = 6371.009;
    }
    else if ( $unit == 'mi' )
    {
      $radius = 3958.761;
    }

    // latitude boundaries
    $maxLat = ( float ) $lat + rad2deg( $distance / $radius );
    $minLat = ( float ) $lat - rad2deg( $distance / $radius );

    // longitude boundaries (longitude gets smaller when latitude increases)
    $maxLng = ( ( float ) $lon + rad2deg( $distance / $radius) );// /  cos( deg2rad( ( float ) $lat ) );
    $minLng = ( ( float ) $lon - rad2deg( $distance / $radius) );// /  cos( deg2rad( ( float ) $lat ) );

    $max_min_values = array(
      'max_latitude' => $maxLat,
      'min_latitude' => $minLat,
      'max_longitude' => $maxLng,
      'min_longitude' => $minLng
    );

    return $max_min_values;


  }/* getRadiusValues() */


  /**
   * get the connect page query
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function getConnectQuery( $postsPerPage = '10', $request = array() )
  {

    if ( empty( $request ) )
    {
      $request = $_REQUEST;
    }

    global $paged, $wp_query;

    if ( !isset( $paged ) || !$paged )
    {
      $paged = 1;
    }

    // if searching within a city, make the search radius bigger
    $boundaryCalc = 0.13;

    // otherwise, if searching within a postal code,
    // make the search radius slightly smaller
    if ( !empty( $request[ 'type' ] ) )
    {
      if ( strpos( $request[ 'type' ], 'postal_' ) )
      {
        $boundaryCalc = 0.1;
      }

      if ( strpos( $request[ 'type' ], 'ministrative_area_level_1' ) )
      {
        $boundaryCalc = 0.3;
      }
    }

    $lat = (float) $request[ 'lat' ];
    $lon = (float) $request[ 'lon' ];

    $context[ 'lat' ] = $lat;
    $context[ 'lon' ] = $lon;

    $boundaryMin = array(
      'lat' => $lat - $boundaryCalc,
      'lon' => $lon + $boundaryCalc,
    );

    $boundaryMax = array(
      'lat' => $lat + $boundaryCalc,
      'lon' => $lon - $boundaryCalc,
    );

    $metaQuery = array();
    $filter = array( 'event', 'request' );

    $metaQuery =  array(
      'relation' => 'AND',
      array(
        'key' => 'status',
        'value' => 'none',
      ),
      array(
        'key' => 'location',
        'compare' => 'EXISTS',
      ),
      array(
        'relation' => 'AND',
        array(
          'key' => 'latitude',
          'value' => $boundaryMin[ 'lat' ],
          'compare' => '>=',
        ),
        array(
          'key' => 'latitude',
          'value' => $boundaryMax[ 'lat' ],
          'compare' => '<=',
        ),
      ),
      array(
        'relation' => 'AND',
        array(
          'key' => 'longitude',
          'value' => $boundaryMin[ 'lon' ],
          'compare' => '>=',
        ),
        array(
          'key' => 'longitude',
          'value' => $boundaryMax[ 'lon' ],
          'compare' => '<=',
        ),
      ),
    );

    if ( !empty( $request[ 'filter' ] ) )
    {
      $filter = $request[ 'filter' ];
    }

    $args = array(
      'posts_per_page' => (int) $postsPerPage,
      'post_type' => $filter,
      'orderby' => array( 'date' => 'ASC' ),
      'paged' => $paged,
      'post_status' => array( 'publish', 'future' ),
      'meta_query' => $metaQuery,
      'orderby' => array(
        'meta_value_num' => 'ASC',
        'date' => 'DESC'
      ),
    );

    return $args;

  }


} /* class Connect() */
