<?php

namespace MMC;

class Projects {

  public function __construct() {

    add_action( 'transition_post_status', array( &$this, 'transition_post_status__onProjectPublish' ), 10, 3 );

  }


  /**
   * send the user an email once their project has been approved
   *
   * @author Ynah Pantig
   * @param $newStatus, $oldStatus, $post
   * @return
   */

  public function transition_post_status__onProjectPublish( $newStatus, $oldStatus, $post )
  {

    if ( $post->post_type != 'project' ) {
      return;
    }

    if ( $oldStatus != 'publish' && $newStatus == 'publish' )
    {
      $author = $post->post_author;
      $authorEmail = get_the_author_meta( 'email', $author );
      $authorDisplayName = get_the_author_meta( 'display_name', $author );

      $message = get_field( 'project_published_message', 'options' );
      $subject = get_field( 'project_published_subject', 'options' );

      if ( strpos( $subject, '$project' ) > -1 ) {
        $subject = str_replace( '$project', $post->post_title, $subject );
      }

      if ( strpos( $message, '$project' ) > -1 ) {
        $message = str_replace( '$project', $post->post_title, $message );
      }

      if ( strpos( $message, '$user' ) > -1 ) {
        $message = str_replace( '$user', $authorDisplayName, $message );
      }

      if ( !empty( $message ) && !empty( $subject ) ) {
        wp_mail( $authorEmail, $subject, $message );
      }

    }

  }


  /**
   * get custom query for the archive
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function customQuery()
  {

    global $wp_query;
    $args = $wp_query->query;
    $pageTitle = [];

    $args[ 'post_status' ] = 'publish';

    if ( isset( $_REQUEST[ 'project_search' ] ) ) {
      $args[ 's' ] = $_REQUEST[ 'project_search' ];
    }

    // if ( isset( $_REQUEST[ 'stage' ] ) && !empty( $_REQUEST[ 'stage' ] ) ) {

    //   $stage = $_REQUEST[ 'stage' ];

    //   if ( $stage == 'recent' )
    //   {
    //     $args[ 'orderby' ] = array( 'date' => 'ASC' );

    //     $pageTitle = [
    //       'singular' => __( 'Recent Project Found' ),
    //       'plural' => __( 'Recent Projects Found' ),
    //     ];

    //   } else
    //   {
    //     $args[ 'orderby' ] = array( 'meta_value_num' => 'ASC', 'date' => 'ASC' );
    //     $args[ 'tax_query' ] = array(
    //       'relation' => 'AND',
    //       array(
    //         'taxonomy' => 'stage',
    //         'field' => 'slug',
    //         'terms' => $stage,
    //       ),
    //     );

    //     $stageTerms = get_term_by( 'slug', $stage, 'stage' );
    //     $pageTitle .= ' ' . $stageTerms->name . ' ';

    //     $pageTitle = [
    //       'singular' => $stageTerms->name . ' ' . __( 'project found' ),
    //       'plural' => $stageTerms->name . ' ' . __( 'projects found' ),
    //     ];
    //   }

    //   unset( $args[ 'stage' ] );
    // }

    // if ( isset( $_REQUEST[ 'needs' ] ) && !empty( $_REQUEST[ 'needs' ] ) )
    // {
    //   $needTerms = get_term_by( 'slug', $filter, 'need' );
    //   $requestNeeds = $_REQUEST[ 'needs' ];

    //   $needsArgs = [
    //     'relation' => 'OR'
    //   ];

    //   foreach ( $requestNeeds as $need ) {
    //     $needsArgs[] = [
    //       'taxonomy' => 'need',
    //       'field' => 'slug',
    //       'terms' => $need,
    //     ];
    //   }

    //   $args[ 'tax_query' ][] = $needsArgs;
    // }

    $query = new \WP_Query( $args );

    if ( ( isset( $args[ 's' ] ) && $args[ 's' ] != '' ) && function_exists( 'relevanssi_do_query' ) ) {
      relevanssi_do_query( $query );
    }

    $posts = $query->posts;

    $data = [
      'page_title' => $pageTitle,
      'query_args' => $args,
      'archive' => $posts,
    ];

    return $data;

  }


}
