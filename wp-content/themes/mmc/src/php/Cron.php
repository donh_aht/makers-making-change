<?php

namespace MMC;

class Cron
{
  private $schedules = array(
    '10min' => array(
      'interval' => 10*60,
      'display' => 'Once every 10 minutes',
    ),
  );

  public function __construct() {
    add_filter( 'cron_schedules', array( &$this, 'cron_schedules' ) );
  }

  function cron_schedules($schedules){
    foreach ($this->schedules as $key => $value) {
      if(isset($schedules[$key])){
        continue;
      }

      $schedules[$key] = $value;
    }

    return $schedules;
  }
}
