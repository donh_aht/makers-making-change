<?php

namespace MMC;

class UserImport
{

  public static $inactiveMeta = 'inactive_since_import';
  private $pageSlug = 'mmc_import_users_settings';

  public function __construct()
  {
    // add ACF options pages
    add_action( 'init', array( $this, 'init__addOptionsPages' ) );
    add_action( 'acf/load_field/name=associated_role', array( $this, 'acf_load_field__populateRoleChoices' ) );

    add_action( 'wp_login', array( $this, 'wp_login__activateUserAfterLogin' ), 10, 2 );

    add_action( 'wp_ajax_import_account', array( $this, 'ajax__importAccount' ) );
    add_action( 'wp_ajax_nopriv_import_account', array( $this, 'ajax__importAccount' ) );
  }

  public function init__addOptionsPages() {
    if ( function_exists( 'acf_add_options_sub_page' ) )
    {
      acf_add_options_sub_page(array(
        'title'      => __( 'Import Users', 'mmc' ),
        'parent_slug'=> 'users.php',
        'capability' => 'manage_options',
        'menu_slug'  => $this->pageSlug,
      ));
    }
  }

  function acf_load_field__populateRoleChoices($field)
  {
    $field['choices'] = array();

    foreach (Roles::getMMCRoles() as $key => $role) {
      $field['choices'][$key] = $role['name'];
    }

    return $field;
  }

  public function wp_login__activateUserAfterLogin($user_login, $user) {
    delete_user_meta($user->ID, static::$inactiveMeta);
  }

  /**
   * Import user account
   *
   * @author Alessandro Biavati
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__importAccount()
  {
    if (!is_admin()) {
      wp_send_json_error('Unauthorized.', 401);
      die();
    }

    $account = json_decode(stripslashes(urldecode($_REQUEST['data'])), true);

    // example $account data:
    // {
    //   "campaign_name": "Google LipSync",
    //   "member_type": "Lead",
    //   "member_status": "1-Targetted",
    //   "lead_contact_id": "00Q1600000wk4lR",
    //   "member_first_associated_date": "15/01/2018",
    //   "member_status_update_date": "08/03/2018",
    //   "member_first_responded_date": "",
    //   "responded": "0",
    //   "first_name": "Barb",
    //   "last_name": "Schober",
    //   "company": "Schober Household",
    //   "title": "",
    //   "phone": "",
    //   "mobile": "",
    //   "email": "bschober@telus.net",
    //   "street": "7969 Wiltshire Blvd",
    //   "state_province": "BC",
    //   "zip_postal_code": "V4C 4B5",
    //   "country": "Canada",
    //   "email_opt_out": "0",
    //   "row": 2,
    //   "roles": ["pwd"],
    //   "geolocation": {
    //     ...
    //     "geometry": {
    //       ...
    //       "location": {
    //         "lat": 49.147768,
    //         "lng": -122.925245
    //       }
    //     }
    //   }
    // }

    if (!$account['email']) {
      wp_send_json_error('Invalid email.', 400);
      die();
    }

    $successResponse = array('message' => 'Account updated');

    // create account if it doesn't exist yet
    $userID = email_exists($account['email']);
    if ($userID) {
      $overrideExisting = $account['override_existing'];
      if (empty($overrideExisting) || $overrideExisting == 'no') {
        wp_send_json_error('Account already exists, skipping.', 409);
        die();
      }

      $inactive = get_user_meta($userID, static::$inactiveMeta, true);
      if (!$inactive) {
        wp_send_json_error('Account is already active, skipping.', 409);
        die();
      }
    } else {
      $emailParts = explode('@', $account['email']);
      $username = $emailParts[0];
      $username = Utils::generate_unique_username($username);
      $password = wp_generate_password(12, false);

      // Create user silently! without sending an email.
      $userID = wp_create_user( $username, $password, $account['email'] );

      if (!$userID) {
        wp_send_json_error('Error creating account.', 500);
        die();
      }

      $successResponse['message'] = 'Account created';
      $successResponse['created'] = true;

      // set as inactive until user logs-in and completes profile
      update_user_meta($userID, static::$inactiveMeta, true);
    }

    $userObj = new \WP_User($userID);
    $userObj->set_role('subscriber');

    // initialize user data object
    $userData = array( 'ID' => $userID );

    foreach ($account as $key => $value) {
      if (empty($value)) {
        continue;
      }

      switch ($key) {
        case 'geolocation':
          Location::updateUserGeolocationData($userID, $value);
          break;

        case 'roles':
          foreach ($value as $role) {
            $userObj->add_role($role);
          }
          break;

        case 'member_first_associated_date':
          $dateobj = \DateTime::createFromFormat('d/m/Y', $value);
          if ($dateobj) {
            $user_registered = $dateobj->format('Y-m-d 00:00:00');
            $userData['user_registered'] = $user_registered;
          }
          break;

        case 'first_name':
          update_user_meta($userID, $key, $value);
          $userData['nickname'] = $value;
          $display_name = trim($account['first_name'] . ' ' . $account['last_name']);
          $userData['display_name'] = $display_name;
          break;

        case 'last_name':
          update_user_meta($userID, $key, $value);
          break;

        default:
          break;
      }
    }

    $updatedUserId = wp_update_user($userData);

    if (is_wp_error($updatedUserId)) {
      wp_send_json_error('Error updating account.', 400);
      die();
    }

    $successResponse['id'] = $userID;
    $successResponse['url'] = admin_url('/user-edit.php?user_id=' . $userID);
    wp_send_json(array('success' => true, 'data' => $successResponse));
    die();

  }/* ajax__importAccount() */



} /* class UserImport() */
