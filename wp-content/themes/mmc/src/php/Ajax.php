<?php

namespace MMC;

/**
 * Timber extensions
 */
class Ajax
{

  public function __construct()
  {

    add_action( 'wp_ajax_get_user_data', array( $this, 'ajax__getUserData' ) );
    add_action( 'wp_ajax_nopriv_get_user_data', array( $this, 'ajax__getUserData' ) );

    add_action( 'wp_ajax_change_request_status', array( $this, 'ajax__changeRequestStatus' ) );
    add_action( 'wp_ajax_nopriv_change_request_status', array( $this, 'ajax__changeRequestStatus' ) );

    add_action( 'wp_ajax_remove_download_item', array( $this, 'ajax__removeProjectListItem' ) );
    add_action( 'wp_ajax_nopriv_remove_download_item', array( $this, 'ajax__removeProjectListItem' ) );

    add_action( 'wp_ajax_remove_image_item', array( $this, 'ajax__removeProjectListItem' ) );
    add_action( 'wp_ajax_nopriv_remove_image_item', array( $this, 'ajax__removeProjectListItem' ) );

    add_action( 'wp_ajax_check_user_connections', array( $this, 'ajax__checkUserConnections' ) );
    add_action( 'wp_ajax_nopriv_check_user_connections', array( $this, 'ajax__checkUserConnections' ) );

    add_action( 'wp_ajax_set_maker_email', array( $this, 'ajax__connectForm_setMakerEmail' ) );
    add_action( 'wp_ajax_nopriv_set_maker_email', array( $this, 'ajax__connectForm_setMakerEmail' ) );

    add_action( 'wp_ajax_get_user_posts', array( $this, 'ajax__getUserPosts' ) );
    add_action( 'wp_ajax_nopriv_get_user_posts', array( $this, 'ajax__getUserPosts' ) );

    add_action( 'wp_ajax_deactivate_user_profile', array( $this, 'ajax__deactivateUserProfile' ) );
    add_action( 'wp_ajax_nopriv_deactivate_user_profile', array( $this, 'ajax__deactivateUserProfile' ) );

    add_action( 'wp_ajax_edit_comment', array( $this, 'ajax__editComment' ) );
    add_action( 'wp_ajax_nopriv_edit_comment', array( $this, 'ajax__editComment' ) );

    add_action( 'wp_ajax_delete_comment', array( $this, 'ajax__deleteComment' ) );
    add_action( 'wp_ajax_nopriv_delete_comment', array( $this, 'ajax__deleteComment' ) );

    add_action( 'wp_ajax_fill_maker_data', array( $this, 'ajax__fillConnectFormData' ) );
    add_action( 'wp_ajax_nopriv_fill_maker_data', array( $this, 'ajax__fillConnectFormData' ) );
  }


  /**
   * get user data to use for the profile page
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__getUserData()
  {
    $userID = get_current_user_id();

    if ( !$userID ) {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $user = get_user_by('id', $userID);
    $userFieldID = 'user_' . $user->ID;

    $facebook = get_field( 'user_facebook', $userFieldID );
    $instagram = get_field( 'user_instagram', $userFieldID );
    $twitter = get_field( 'user_twitter', $userFieldID );
    $youtube = get_field( 'user_youtube', $userFieldID );
    $thingiverse = get_field( 'user_thingiverse', $userFieldID );
    $github = get_field( 'user_github', $userFieldID );

    $skillsList = get_field( 'user_skills', $userFieldID );
    $location = get_field( 'geolocation_formatted_address', $userFieldID );
    $connections = get_field( 'connection_users', $userFieldID );

    $skills = array();

    if ( !empty( $skillsList ) ) {
      $termArgs = [
        'taxonomy' => 'skill',
        'include' => $skillsList,
        'hide_empty' => false,
      ];

      $skills = new \WP_Term_Query( $termArgs );
      $skills = $skills->terms;
    }

    $logout_url = html_entity_decode(wp_logout_url());

    $data = array(
      'userID' => $userFieldID,
      'connections' => $connections,
      'location' => $location,
      'skills' => $skills,
      'email' => $user->user_email,
      'social' => array(
        'facebook' => $facebook,
        'twitter' => $twitter,
        'instagram' => $instagram,
        'youtube' => $youtube,
        'thingiverse' => $thingiverse,
        'github' => $github,
      ),
      'logout_url' => urlencode($logout_url),
    );

    echo json_encode( $data );
    die();

  }/* ajax__getUserData() */


  /**
   * get the user's reviews
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__getUserReviews(  )
  {

    $userID = get_current_user_id();

    if (!$userID)
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $args = array(
      'author__in' => $userID,
      'post_type' => 'project',
    );

    $comments = get_comments( $args );

    foreach ( $comments as &$comment )
    {
      $comment->fields = get_fields( 'comment_' . $comment->comment_ID );
    }

    echo json_encode( $comments );
    die();

  }/* ajax__getUserReviews() */


  /**
   * check user connections
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__checkUserConnections()
  {

    $userID = get_current_user_id();

    if (!$userID)
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $connections = get_field( 'connection_users', 'user_' . $userID );

    $data = array();

    if ( empty( $connections ) )
    {
      echo json_encode($data);
      die();
    }

    foreach ( $connections as $connection )
    {
      $data[] = $connection['ID'];
    }

    echo json_encode( $data );
    die();

  }/* ajax__checkUserConnections() */


  /**
   * get user requests
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__getUserPosts()
  {
    $userID = get_current_user_id();

    if (!$userID)
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $postType = $_REQUEST[ 'post_type' ];

    switch ( $postType ) {
      case 'connections':
        $data = Users::getUserConnections( $userID );
        break;

      case 'reviews':
        $data = Users::getUserReviews( $userID );
        break;

      case 'projects':
        $data = Users::getUserProjects( $userID );
        break;

      case 'requests':
        $data = Users::getUserRequests( $userID );
        break;

      default:
        # code...
        break;
    }

    echo json_encode( $data );
    die();

  }/* ajax__getUserPosts() */


  /**
   * remove a download item based on the post ID and the ID of the item
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__removeProjectListItem()
  {
    $postID = (int) $_REQUEST[ 'post_id' ];

    if ( !current_user_can('edit_post', $postID) )
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $itemID = $_REQUEST[ 'item_id' ];
    $field = $_REQUEST[ 'field' ];
    $namespace = $_REQUEST[ 'namespace' ];

    $list = get_field( $field, $postID );
    $initCount = count( $list );

    if ( $namespace == 'download' )
    {
      $list = $list[ 'downloadables' ];
    }

    foreach ( $list as $key => $item )
    {
      // check if it's a file field by checking the namespace
      if ( $namespace == 'download' )
      {
        $item = $item[ 'file' ];
      }

      // remove the item from the list
      if ( $item[ 'ID' ] == $itemID )
      {
        unset( $list[ $key ] );
      }
    }

    // only update the field if the number of items
    // in the list has been changed
    if ( count( $list ) < $initCount )
    {
      $new = $list;

      // the downloadable fields is namespaced, so we need to make sure
      // that the data is going to the correct field
      if ( $namespace == 'download' )
      {
        $new = array();
        $new[ 'downloadables' ] = $list;
      }

      update_field( $field, $new, $postID );
    }

    die();
  }/* ajax__removeProjectListItem() */



  /**
   * change the status of the request / idea post
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__changeRequestStatus()
  {
    $postID = (int) $_REQUEST[ 'post_id' ];

    if ( !current_user_can('edit_post', $postID) )
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $value = $_REQUEST[ 'value' ];

    $oldValue = get_field( 'status', $postID );

    if ( $value != $oldValue )
    {
      update_field( 'status', $value, $postID );
    }

    update_post_meta( $postID, $value . '_date', date( 'Y-m-d' ) );

    die();

  }/* ajax__changeRequestStatus() */


  /**
   * set the user's email in the connect form
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__connectForm_setMakerEmail(  )
  {

    $userID = get_current_user_id();

    if ( !$userID )
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $user = get_user_by( 'id', $userID );
    $userEmail = $user->user_email;

    echo array(
      'email' => $userEmail,
      'name' => $user->display_name,
    );

    die();

  }/* ajax__connectForm_setMakerEmail() */


  /**
   * deactivate the user's profile
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function ajax__deactivateUserProfile(  )
  {

    $userID = get_current_user_id();

    if ( !$userID )
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $user = get_user_by( 'id', $userID );

    $userRoles = Roles::getUserMMCRoles( $userID );
    foreach ( $userRoles as $role ) {
      $user->remove_role( $role );
    }

    update_field( 'user_roles', '', 'user_' . $userID );
    update_user_meta( $userID, 'deactivate', true );
    update_user_meta( $userID, 'profile_completed', '' );

    wp_logout();

    die();

  }/* ajax__deactivateUserProfile() */


  /**
   * delete the comment
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function ajax__deleteComment(  )
  {
    $userID = get_current_user_id();

    if (!$userID){
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $commentID = $_REQUEST[ 'comment_id' ];
    $comment = get_comment($commentID);

    if (!$comment || !isset($comment->user_id) || $comment->user_id != $userID) {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    wp_delete_comment( $commentID );

    die();

  }


  /**
   * setup connect form data
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function ajax__fillConnectFormData(  )
  {

    $userID = get_current_user_id();

    if ( !$userID )
    {
      wp_send_json_error('unauthorized', 401);
      die();
    }

    $makerID = (int) $_REQUEST[ 'maker_id' ];
    $requestID = (int) $_REQUEST[ 'request_id' ];

    $maker = get_user_by( 'id', $makerID );
    $user = get_user_by( 'id', $userID );

    $request = '';
    if ( !empty( $requestID ) ) {
      $post = get_post( $requestID );

      if ( !empty( $post->post_content ) ) {
        $request = $post->post_content . '<br />&mdash; ' . $maker->display_name;
      }
    }

    $makerFieldID = 'user_' . $makerID;
    $makerProvince = get_field( 'geolocation_state', $makerFieldID );
    $makerCity = get_field( 'geolocation_city', $makerFieldID );
    $makerCountry = get_field( 'geolocation_country', $makerFieldID );

    $userFieldID = 'user_' . $userID;
    $userProvince = get_field( 'geolocation_state', $userFieldID );
    $userCity = get_field( 'geolocation_city', $userFieldID );
    $userCountry = get_field( 'geolocation_country', $userFieldID );

    $data = array(
      'request' => $request,
      'maker' => array(
        'email' => $maker->user_email,
        'name' => $maker->display_name,
        'location' => $makerCity . ', ' . $makerProvince . ', ' . $makerCountry,
      ),
      'user' => array(
        'email' => $user->user_email,
        'name' => $user->display_name,
        'location' => $makerCity . ', ' . $makerProvince . ', ' . $makerCountry,
      ),
    );

    echo json_encode( $data );

    die();

  }

} /* class Project() */
