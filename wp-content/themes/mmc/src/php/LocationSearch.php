<?php

namespace MMC;

class LocationSearch
{
  private static $indexTableName = 'mmc_location_search_index';
  private static $indexTableVersion = '1.0';
  private static $indexTableVersionOption = 'mmc_location_search_index_version';

  private static $cronEventName = 'mmc_update_location_search_index';
  private static $cronEventRecurrence = '10min';

  public function __construct() {
    add_filter( 'after_switch_theme', array( &$this, 'after_switch_theme' ) );
    add_filter( 'after_setup_theme', array( &$this, 'after_setup_theme' ) );
    add_action( self::$cronEventName, array( &$this, 'updateIndex' ) );

    if ( isset( $_REQUEST[ 'json' ] ) && !empty( $_REQUEST[ 'json' ] ) && $_REQUEST[ 'json' ] == true ) {
      add_action( 'init', array( &$this, 'init__buildMapSearchResults' ) );
    }

    if ( isset( $_REQUEST[ 'map_api' ] ) && $_REQUEST[ 'map_api' ] == 'run' ) {
      add_action( 'init', array( &$this, 'updateIndex' ) );
    }
  }

  public function after_switch_theme() {
    self::installIndexTable();
    self::installIndexCronJob();
  }

  public function after_setup_theme() {
    // only update if administrator is in admin area
    if (!is_admin() || !current_user_can('administrator')) {
      return;
    }

    self::installIndexTable();
    self::installIndexCronJob();
  }

  public static function getIndexTableName() {
    global $wpdb;
    return $wpdb->prefix . self::$indexTableName;
  }

  public static function installIndexTable() {
    global $wpdb;

    $installedVersion = get_option( self::$indexTableVersionOption );
    if ( $installedVersion != self::$indexTableVersion ) {
      $tableName = self::getIndexTableName();
      $charsetCollate = $wpdb->get_charset_collate();

      $sql = "CREATE TABLE $tableName (
        id INT(11) NOT NULL,
        type VARCHAR(20) NOT NULL,
        lat DECIMAL(10, 8) NOT NULL,
        lng DECIMAL(11, 8) NOT NULL,
        date datetime NOT NULL,
        outdated INT(4) NOT NULL,
        PRIMARY KEY  (id,type)
      ) $charsetCollate;";

      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
      $result = dbDelta( $sql );
    }

    update_option( self::$indexTableVersionOption, self::$indexTableVersion );
  }

  public static function installIndexCronJob() {
    if (!wp_next_scheduled(self::$cronEventName)) {
      wp_schedule_event(time(), self::$cronEventRecurrence, self::$cronEventName);
    }
  }

  public static function setAllIndexItemsOutdated() {
    global $wpdb;

    $tableName = self::getIndexTableName();
    $sql = "UPDATE {$tableName} set outdated = 1 where 1 = 1";
    $wpdb->query($sql);
  }

  public static function removeOutdated() {
    global $wpdb;

    $tableName = self::getIndexTableName();
    $sql = "DELETE FROM {$tableName} WHERE outdated = 1";
    $wpdb->query($sql);
  }

  public function updateIndex() {
    self::setAllIndexItemsOutdated();

    $items = self::getIndexItems();
    foreach ($items as $item) {
      self::updateIndexItem($item['id'], $item['type'], $item['lat'], $item['lng'], $item['date']);
    }

    self::removeOutdated();
  }

  public static function updateIndexItem($id, $type, $lat, $lng, $date) {
    global $wpdb;

    if (empty($id) || empty($type) || empty($lat) || empty($lng) || empty($date)) {
      return;
    }

    $tableName = self::getIndexTableName();

    $sqlRaw = "
      INSERT INTO {$tableName} (id, type, lat, lng, date, outdated)
      VALUES (%d, %s, %s, %s, %s, 0)
      ON DUPLICATE KEY UPDATE
      lat = %s, lng = %s, date = %s, outdated = 0
    ";

    $sql = $wpdb->prepare($sqlRaw, $id, $type, $lat, $lng, $date, $lat, $lng, $date);

    $wpdb->query($sql);
  }

  public static function getIndexItems() {
    return array_merge(
      self::getMakersIndexItems(),
      self::getRequestsIndexItems(),
      self::getEventsIndexItems()
      // self::getTestIndexItems() // this is test data
    );
  }

  public static function getMakersIndexItems() {
    $items = array();

    // get users with Maker role
    $args = array(
      'role' => 'maker',
      'fields' => array('ID', 'user_registered'),

      'meta_query' => array(
        // exclude inactive
        array(
          'key' => UserImport::$inactiveMeta,
          'compare' => 'NOT EXISTS',
        ),

        // exclude users with no geolocation data
        array(
          'key' => 'geolocation_lat',
          'compare' => 'EXISTS',
        ),
        array(
          'key' => 'geolocation_lng',
          'compare' => 'EXISTS',
        ),
      ),
    );

    $query = new \WP_User_Query($args);
    $users = $query->get_results();

    foreach ($users as $user) {
      $lat = get_user_meta($user->ID, 'geolocation_lat', true);
      $lng = get_user_meta($user->ID, 'geolocation_lng', true);

      // exclude posts by users with no geolocation data
      if (empty($lat) || empty($lng)) {
        continue;
      }

      $items[] = array(
        'id' => $user->ID,
        'type' => 'maker',
        'lat' => $lat,
        'lng' => $lng,
        'date' => $user->user_registered,
      );
    }

    return $items;
  }

  public static function getRequestsIndexItems() {
    $items = array();

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'request',
      'meta_query' => array(
        array(
          'key' => 'status',
          'value' => 'none',
        )
      ),
      'fields' => array(
        'ID',
        'post_date',
        'post_author',
        'status'
      ),
    );

    $query = new \WP_Query($args);
    $posts = $query->posts;

    foreach ($posts as $post) {
      // $lat = get_field('latitude', 'user_' . $post->post_author);
      // $lng = get_field('longitude', 'user_' . $post->post_author);
      $lat = get_user_meta( $post->post_author, 'geolocation_lat', true );
      $lng = get_user_meta( $post->post_author, 'geolocation_lng', true );

      // exclude posts by users with no geolocation data
      if (empty($lat) || empty($lng)) {
        continue;
      }

      $items[] = array(
        'id' => $post->ID,
        'type' => 'request',
        'lat' => $lat,
        'lng' => $lng,
        'date' => $post->post_date,
      );
    }

    return $items;
  }

  public static function getEventsIndexItems() {
    $items = array();

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'event',
      'fields' => array('ID'),

      'meta_query' => array(
        // exclude events with no date data
        array(
          'key' => 'date_start',
          'compare' => 'EXISTS',
        ),

        // exclude events with no geolocation data
        array(
          'key' => 'geolocation',
          'compare' => 'EXISTS',
        ),
      ),
    );

    $query = new \WP_Query($args);
    $posts = $query->posts;

    foreach ($posts as $post) {
      $geolocation = get_field('geolocation', $post->ID);
      $date = get_field('date_start', $post->ID);

      if (empty($geolocation) || empty($date)) {
        continue;
      }

      $items[] = array(
        'id' => $post->ID,
        'type' => 'event',
        'lat' => $geolocation['lat'],
        'lng' => $geolocation['lng'],
        'date' => $date,
      );
    }

    return $items;
  }

  public static function getTestIndexItems() {
    $items = array();
    $testData = array_map('str_getcsv', file(__DIR__ . '/locationSearchTestData.csv'));

    foreach ($testData as $index => $item) {
      $items[] = array(
        'id' => $index + 1,
        'type' => 'test',
        'lat' => $item[0],
        'lng' => $item[1],
        'date' => date('Y-m-d H:i:s'),
      );
    }

    return $items;
  }

  public static function searchIndex($params) {
    global $wpdb;

    /**
     * Validate mandatory params
     */
    if (
      empty($params['south']) ||
      empty($params['north']) ||
      empty($params['west']) ||
      empty($params['east'])
    ) {
      return array();
    }

    // raw query
    $tableName = self::getIndexTableName();
    $sqlRaw = "
      SELECT id, type, lat, lng
      FROM $tableName
      WHERE 1 = 1
      AND lat >= %s AND lat <= %s AND lng >= %s AND lng <= %s
      ORDER BY date DESC
    ";

    $sql = $wpdb->prepare($sqlRaw, array(
      $params['south'],
      $params['north'],
      $params['west'],
      $params['east'],
    ));

    $results = $wpdb->get_results($sql);

    // initialize response
    $response = array( 'stats' => array(), 'posts' => array() );

    /**
     * Calculate stats and filter items
     */
    foreach ($results as $index => $result) {
      // initialize total count for type
      if (empty($response['stats'][$result->type])) {
        $response['stats'][$result->type] = 0;
      }

      // increase total count for type
      $response['stats'][$result->type] += 1;

      // check if we need to get post data
      if (!empty($params['type']) && $params['type'] != $result->type) {
        continue;
      }

      $response['posts'][] = (array) $result;
    }

    // set posts found before pagination
    $response['posts_found'] = count($response['posts']);

    /**
     * Pagination
     */
    if (!empty($params['page']) && !empty($params['posts_per_page']) && $params['posts_per_page'] != -1) {
      $firstPage = 1;
      $page = empty($params['page']) || $params['page'] < 1 ? $firstPage : $params['page'];
      $postsPerPage = $params['posts_per_page'];
      $offset = $postsPerPage * ($page - 1);
      $response['posts'] = array_slice($response['posts'], $offset, $postsPerPage);
    }

    return $response;
  }
  /**
   * get the stats from the returned data
   *
   * @author Ynah Pantig
   * @param $query
   * @return $data
   */

  static public function getConnectStats( $stats )
  {

    $statLabels = array(
      'event' => array(
        'singular' => __( 'Event' ),
        'plural' => __( 'Events' ),
      ),
      'maker' => array(
        'singular' => __( 'Maker' ),
        'plural' => __( 'Makers' ),
      ),
      'request' => array(
        'singular' => __( 'Request' ),
        'plural' => __( 'Requests' ),
      ),
    );

    if ( isset( $_REQUEST[ 'filter' ] ) && !empty( $_REQUEST[ 'filter' ] ) )
    {
      $filter = $_REQUEST[ 'filter' ];
    }

    $labelArr = array();

    $location = $_REQUEST[ 'location' ];
    $testStats = 0;
    foreach ( $stats as $key => $stat ) {

      if ( isset( $filter ) && !empty( $filter ) && $filter != $key ) {
        continue;
      }

      if ( $key == 'test' ) {
        $testStats = $stat;
        continue;
      }

      $statLabel = $statLabels[ $key ];

      if ( $key == 'maker' ) {
        $stat += $testStats;
      }

      if ( $stat == 1 )
      {
        $label = $statLabel[ 'singular' ];
      } else {
        $label = $statLabel[ 'plural' ];
      }

      $labelArr[] = $stat . ' ' . $label;
    }

    $stats = implode( ', ', $labelArr )  . ' ' . __( 'found' );

    $data = array(
      'label' => $stats,
      'page_title' => $stats . ' ' . __( 'in' ) . ' ' . $location,
    );

    return $data;

  }


  /**
   * build the data for the map
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function init__buildMapSearchResults()
  {

    $items = self::getIndexItems();

    foreach ( $items as &$item ) {
      $data = Post::getConnectData( $item );
      $item[ 'data' ] = $data;
    }

    // load this via Enqueue
    header( 'Content-Type: text/javascript' );
    echo 'loadSearchItems(' . json_encode( $items ) . ');';
    die();

  }


} /* class LocationSearch() */
