<?php

namespace MMC;

class PasswordReset
{
  public static $passwordResetMetaKey = 'reset_password_key';
  public static $passwordResetTimeMetaKey = 'reset_password_key_time';
  public static $passwordResetKeyLifetime = 7200;

  private function __construct(){}

  public static function setPasswordResetKey($userID, $key)
  {
    update_user_meta( $userID, self::$passwordResetMetaKey, $key );
    update_user_meta( $userID, self::$passwordResetTimeMetaKey, time() );
  }

  public static function isPasswordResetKeyValid($userID, $key)
  {
    $savedKey = get_user_meta($userID, self::$passwordResetMetaKey, true);
    $savedKeyTime = get_user_meta($userID, self::$passwordResetTimeMetaKey, true);

    if (empty($savedKeyTime) || empty($savedKey) || empty($key) || $savedKey != $key) {
      return false;
    }

    $currentTime = time();
    $keyExpired = $currentTime - $savedKeyTime > self::$passwordResetKeyLifetime;

    if ($keyExpired) {
      return false;
    }

    return true;
  }

  public static function completePasswordResetProcess($userID, $password)
  {
    wp_update_user( array (
        'ID' => $userID,
        'user_pass' => $password
      )
    );

    delete_user_meta( $userID, self::$passwordResetMetaKey);
    delete_user_meta( $userID, self::$passwordResetTimeMetaKey);
  }

} /* class PasswordReset */
