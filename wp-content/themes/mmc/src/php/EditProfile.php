<?php

namespace MMC;

class EditProfile
{
  /**
   * User data:
   * - First Name
   * - Last Name
   * - Nickname -> {First Name}
   * - Display Name -> {First Name} {Last Name}
   * - Roles
   *
   * User Meta:
   * - Location
   *
   * ACF:
   * - Skills (user_skills)
   */

  private $formID = 8;

  // fields ids
  private $firstNameFieldID = 29;
  private $lastNameFieldID = 30;
  private $geolocationFieldID = 31;
  private $locationFieldID = 5;
  private $skillsFieldID = 34;
  private $facebookFieldID = 9;
  private $twitterFieldID = 10;
  private $instagramFieldID = 33;
  private $youtubeFieldID = 12;
  private $thingiverseFieldID = 27;
  private $githubFieldID = 28;
  private $profileImageFieldID = 25;

  private $fieldUserMetaMap = array();
  private $fieldTaxonomyMap = array();

  public $skillsMetaKey = 'user_skills';
  public $firstNameMetaKey = 'first_name';
  public $lastNameMetaKey = 'last_name';
  public $displayNameMetaKey = 'display_name';
  public $nicknameNameMetaKey = 'nickname';
  public $facebookMetaKey = 'user_facebook';
  public $twitterMetaKey = 'user_twitter';
  public $instagramMetaKey = 'user_instagram';
  public $youtubeMetaKey = 'user_youtube';
  public $thingiverseMetaKey = 'user_thingiverse';
  public $githubMetaKey = 'user_github';
  public $profileImageMetaKey = 'user_profile_image';
  public $profileCompletedMetaKey = 'profile_completed';

  public function __construct()
  {
    // field IDs <--> User meta map
    $this->fieldUserMetaMap[$this->skillsFieldID] = $this->skillsMetaKey;
    $this->fieldUserMetaMap[$this->firstNameFieldID] = $this->firstNameMetaKey;
    $this->fieldUserMetaMap[$this->lastNameFieldID] = $this->lastNameMetaKey;
    $this->fieldUserMetaMap[$this->facebookFieldID] = $this->facebookMetaKey;
    $this->fieldUserMetaMap[$this->twitterFieldID] = $this->twitterMetaKey;
    $this->fieldUserMetaMap[$this->instagramFieldID] = $this->instagramMetaKey;
    $this->fieldUserMetaMap[$this->youtubeFieldID] = $this->youtubeMetaKey;
    $this->fieldUserMetaMap[$this->thingiverseFieldID] = $this->thingiverseMetaKey;
    $this->fieldUserMetaMap[$this->githubFieldID] = $this->githubMetaKey;
    $this->fieldUserMetaMap[$this->githubFieldID] = $this->githubMetaKey;
    $this->fieldUserMetaMap[$this->profileImageFieldID] = $this->profileImageMetaKey;

    // field IDs <--> taxonomy
    $this->fieldTaxonomyMap[$this->skillsFieldID] = 'skill';

    add_action( 'gform_validation_' . $this->formID, array( &$this, 'gform_validation' ) );
    add_action( 'gform_after_submission_' . $this->formID, array( &$this, 'gform_after_submission__saveUserData' ), 10, 2 );
    add_filter( 'gform_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_pre_validation_' . $this->formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_admin_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_pre_submission_filter_' . $this->formID, array( &$this, 'gform__populateFields' ), 10 );

    add_filter( 'gform_confirmation_' . $this->formID, array( &$this, 'gform_confirmation__redirectToProfile' ), 10, 4 );

    add_filter( 'gform_field_validation_' . $this->formID . '_' . $this->locationFieldID, array( &$this, 'gform_field_validation__validateLocationField' ), 10, 4 );

  } /* __construct() */

  function gform_validation( $validation_result ) {
    $userID = get_current_user_id();
    if (!$userID) {
      wp_redirect(site_url('/login/'));
      exit();
    }

    // $profile_completed = get_user_meta($userID, $this->profileCompletedMetaKey, true);
    // if (!$profile_completed) {
    //   wp_redirect(site_url('/register/'));
    //   exit();
    // }

    return $validation_result;
  }

  public function gform_after_submission__saveUserData( $entry, $form )
  {
    $userID = get_current_user_id();
    if (!$userID) {
      return;
    }

    $userObj = new \WP_User($userID);
    $userData = array( 'ID' => $userID );

    foreach ( $form['fields'] as $field ) {
      $value = $field->get_value_export($entry);

      switch ($field->id) {

        case $this->firstNameFieldID:
        case $this->lastNameFieldID:
          $userData[$this->fieldUserMetaMap[$field->id]] = $value;
          break;

        case $this->facebookFieldID:
        case $this->twitterFieldID:
        case $this->instagramFieldID:
        case $this->youtubeFieldID:
        case $this->thingiverseFieldID:
        case $this->githubFieldID:
          if ( empty( $value ) ) {
            $value = '';
          }

          update_field( $this->fieldUserMetaMap[$field->id], $value, 'user_' . $userID );
          break;

        case $this->skillsFieldID:
          $valueArr = explode(', ', $value);
          update_field($this->fieldUserMetaMap[$field->id], $valueArr, 'user_' . $userID);
          break;

        case $this->profileImageFieldID:

          if ( !empty( $value ) ) {
            update_field($this->fieldUserMetaMap[$field->id], $value, 'user_' . $userID);
          }

          break;

        case $this->geolocationFieldID:

          if (empty($value)) {
            continue;
          }

          Location::updateUserGeolocationData($userID, json_decode($value, true));
          break;

        default:
          # code...
          break;
      }
    }

    // Update names
    if (!empty($userData[$this->firstNameMetaKey])) {
      $userData[$this->displayNameMetaKey] = $userData[$this->firstNameMetaKey];
      $userData[$this->nicknameMetaKey] = $userData[$this->firstNameMetaKey];

      if (!empty($userData[$this->lastNameMetaKey])) {
        $userData[$this->displayNameMetaKey] .= ' ' . $userData[$this->lastNameMetaKey];
      }
    }

    wp_update_user($userData);

    // set profile completed flag
    update_user_meta($userID, $this->profileCompletedMetaKey, true);

    $current_user = wp_get_current_user();

    // Forum Integration
    ForumIntegration::EditProfile($current_user, $entry);

  }/* gform_after_submission__saveUserData() */


  public function gform__populateFields($form)
  {
    foreach ( $form['fields'] as &$field )
    {
      switch ($field->id) {
        case $this->geolocationFieldID;

          $userID = get_current_user_id();

          if (!$userID) {
            continue;
          }

          $geolocation = get_usermeta( $userID, 'geolocation' );
          $field->defaultValue = json_encode( $geolocation );

          break;

        case $this->skillsFieldID:

          $inputType = $field->get_input_type();

          $choices = GravityForms::getTaxonomyFieldChoices($this->fieldTaxonomyMap[$field->id]);
          $values = GravityForms::getFieldInputs($field->id, $choices);

          switch ( $inputType ) {
            case 'list':

              // add the choices to the key created by the plugin
              $field->itsg_list_field_drop_down_options = $choices;
              $field->itsg_list_field_drop_down = true;
              $field->itsg_list_field_drop_down_enhanced = false;
              $field->list_choice_drop_down_enhanced_other = false;

              break;

            case 'checkbox':

              $field->choices = $choices;
              $field->inputs = $values;

              break;

            default:
              # code...
              break;
          }

          break;

        default:
          # code...
          break;
      }
    }

    return $form;

  }/* gform__populateFields() */


  /**
   * change the confirmation message to add the user's name
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $confirmation, $form, $entry, $ajax
   * @return
   */

  public function gform_confirmation__redirectToProfile( $confirmation, $form, $entry, $ajax )
  {

    $confirmation = array( 'redirect' => home_url() . '/profile?success=true' );

    return $confirmation;

  }/* gform_confirmation__dynamicallyChangeConfirmation() */


  /**
   * validate the location field if they selected from a list
   * or just typed in a city
   *
   * @author Ynah Pantig
   * @param $result, $value, $form, $field
   * @return
   */

  public function gform_field_validation__validateLocationField( $result, $value, $form, $field )
  {

    if ( $value != '' && $result['is_valid'] ) {
      $geolocation = rgpost( 'input_' . $this->geolocationFieldID );
      $result = GravityForms::validateLocationField( $geolocation, $result );
    }

    return $result;

  }

} /* class ClassName() */
