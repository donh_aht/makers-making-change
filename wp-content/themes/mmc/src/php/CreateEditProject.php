<?php

namespace MMC;

class CreateEditProject {

  private $formID = 2;

  // fields ids
  private $titleFieldID = 1;
  private $featureImageFieldID = 2;
  private $imageGalleryFieldID = 5;
  private $descriptionFieldID = 4;
  private $stageFieldID = 6;
  private $skillsFieldID = 7;
  private $needsFieldID = 19;
  private $downloadsFieldID = 10;
  private $linksFieldID = 9;
  private $creativeCommonsFieldID = 14;
  private $editProjectFieldID = 20;
  private $projectUrlFieldID = 18;
  private $projectIDFieldID = 21;
  private $timeCompleteFieldID = 22;
  private $costBuildFieldID = 23;

  private $fieldProjectMetaMap = array();
  private $fieldTaxonomyMap = array();
  private $fieldUserMetaMap = array();

  public $titleMetaKey = 'post_title';
  public $messageMetaKey = 'post_content';
  public $emailMetaKey = 'user_email';
  public $nameMetaKey = 'display_name';
  public $geolocationMetaKey = 'geolocation';
  public $costBuildMetaKey = 'cost_to_build';
  public $timeCompleteMetaKey = 'time_to_complete';
  public $creativeCommonsMetaKey = 'creative_commons_license';

  public function __construct()
  {

    $this->fieldTaxonomyMap[$this->skillsFieldID] = 'skill';
    $this->fieldTaxonomyMap[$this->needsFieldID] = 'need';
    $this->fieldTaxonomyMap[$this->stageFieldID] = 'stage';

    $this->fieldProjectMetaMap = [
      $this->costBuildFieldID => $this->costBuildMetaKey,
      $this->timeCompleteFieldID => $this->timeCompleteMetaKey,
      $this->creativeCommonsFieldID => $this->creativeCommonsMetaKey,
    ];

    add_action( 'gform_validation_' . $this->formID, array( '\\MMC\\GravityForms', 'gform_validation__redirectToLogin' ) );

    // check if there is a post_id parameter. if there is, we are editing
    if ( ( isset( $_REQUEST[ 'edit' ] ) && $_REQUEST[ 'edit' ] == 'true' ) && isset( $_REQUEST[ 'post_id' ] ) && !empty( $_REQUEST[ 'post_id' ] ) )
    {

      // pre-populate the fields with the data from the post
      add_filter( 'gform_pre_render_' . $this->formID, array( &$this, 'gform__prePopulateFields' ) );
      add_filter( 'gform_field_value_project_links', array( &$this, 'gform__prePopulateProjectLinkField' ) );

    } else {

      // on submit, create the post for the project
      add_filter( 'gform_post_data_' . $this->formID, array( &$this, 'gform_post_data__createPostForProject' ), 10, 3 );

    }

    add_filter( 'gform_confirmation_' . $this->formID, array( &$this, 'gform_confirmation__redirectToProject' ), 10, 4 );
    add_action( 'gform_after_submission_' . $this->formID, array( &$this, 'gform_after_submission__createUpdatePostForProject' ), 10, 2 );

    /**
     * Popuplate the creative commons select field
     */
    add_filter( 'gform_pre_render_' . $this->formID, array( &$this, 'gform__populateCreativeCommons' ) );

    // Note: when changing drop down values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
    add_filter( 'gform_pre_validation_' . $this->formID, array( &$this, 'gform__populateCreativeCommons' ) );

    // Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
    add_filter( 'gform_admin_pre_render_' . $this->formID, array( &$this, 'gform__populateCreativeCommons' ) );

    // Note: this will allow for the labels to be used during the submission process in case values are enabled
    add_filter( 'gform_pre_submission_filter_' . $this->formID, array( &$this, 'gform__populateCreativeCommons' ), 10 );


    add_filter('publish_project', array(&$this, 'wp_project_publish_hook'), 10, 2);
  }/* __construct() */


  /**
   * redirect the form to the project page after creation or editing
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $confirmation, $form, $entry, $ajax
   * @return
   */

  public function gform_confirmation__redirectToProject( $confirmation, $form, $entry, $ajax )
  {

    if ( isset( $_REQUEST[ 'edit' ] ) && !empty( $_REQUEST[ 'edit' ] ) && $_REQUEST[ 'edit' ] == true )
    {
      $confirmation = array( 'redirect' => rgar( $entry, 18 ) );
    } else
    {
      $confirmation = __( 'Thank you for submitting a project. This project will be reviewed by the MMC team and you will be notified once it has been reviewed.' );
    }

    return $confirmation;

  }/* gform_confirmation__redirectToProject() */

  /**
   * create a post for the project and set the data
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $form
   * @return
   */

  public function gform_post_data__createPostForProject( $postData, $form, $entry )
  {

    /**
     * $entry
     * 1 = project title
     * 2 = feeature image
     * 5 = image gallery
     * 4 = project description
     * 6 = stage
     * 7 = capabilities/skills
     * 19 = needs
     * 10 = downloadables
     * 9 = project links
     * 13 = string(0) ""
     * 14 = creative commons license
     */

    $postData[ 'post_type' ] = 'project';
    $postData[ 'post_content' ] = rgar( $entry, '4' );
    $postData[ 'post_status' ] = 'pending';

    return $postData;

  }/* gform_post_data__createPostForProject() */


  /**
   * update data after submitting the form
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $entry, $form
   * @return
   */

  public function gform_after_submission__createUpdatePostForProject( $entry, $form )
  {

    /**
     * $entry
     * 1 = project title / post title
     * 2 = feeature image
     * 5 = image gallery
     * 15 = image gallery html list
     * 4 = project description / post content
     * 6 = stage
     * 7 = capabilities/skills
     * 19 = needs
     * 10 = downloadables
     * 9 = project links
     * 14 = creative commons license
     */

    if ( !empty( $_REQUEST[ 'post_id' ] ) && !empty( $_REQUEST[ 'edit' ] ) )
    {
      $postID = $_REQUEST[ 'post_id' ];
    } else {
      $postID = rgar( $entry, 'post_id' );
    }

    if ( isset( $_REQUEST[ 'edit' ] ) && $_REQUEST[ 'edit' ] == true )
    {
      $newPost = array(
        'ID'           => $postID,
        'post_title'   => rgar( $entry, 1 ),
        'post_content' => rgar( $entry, 4 ),
      );

      // Update the post into the database
      wp_update_post( $newPost );

      $thumbImage = rgar( $entry, '2' );

      if ( !empty( $thumbImage ) )
      {
        if ( strpos( $thumbImage, '|' ) > 0 )
        {
          $thumbImage = explode( '|', $thumbImage );
          $thumbImage = $thumbImage[0];
          $thumbID = Utils::getAttachmentIDFromUrl( $thumbImage );
        }

        if ( !empty( $thumbID ) )
        {
          set_post_thumbnail( $postID, $thumbID );
        }
      }
    }

    // update custom fields
    foreach ( $this->fieldProjectMetaMap as $fieldID => $field ) {
      update_field( $field, rgar( $entry, $fieldID ), $postID );
    }

    $needField = \GFFormsModel::get_field( $form, 19 );
    $needValue = is_object( $needField ) ? $needField->get_value_export( $entry ) : '';

    if ( !empty( $needValue ) )
    {
      $needArr = explode( ',', $needValue );
      foreach ( $needArr as $need )
      {
        $need = get_term_by( 'name', $need, 'need' );
        $needs[] = $need->term_id;
      }

      wp_set_post_terms( $postID, $needs, 'need' );
    }

    if ( !empty( rgar( $entry, '6' ) ) )
    {
      $stage = get_term_by( 'name', rgar( $entry, '6' ), 'stage' );
      wp_set_post_terms( $postID, $stage->term_id, 'stage' );
    }

    // check the image gallery
    if ( !empty( rgar( $entry, '5' ) ) )
    {
      $imagesRaw = json_decode( rgar( $entry, '5' ) );
      $images = array();

      $default = get_field( 'images', $postID );

      if ( !empty( $default ) )
      {
        // get the images that are in the database
        foreach ( $default as $image )
        {
          $images[] = $image[ 'ID' ];
        }
      }

      if ( !empty( $imagesRaw ) )
      {
        foreach ( $imagesRaw as $image ) {
          $images[] = Utils::getAttachmentIDFromUrl( $image, $postID );
        }
      }

      update_field( 'images', $images, $postID );
    }

    // check if the skills are empty
    if ( !empty( rgar( $entry, '7' ) ) )
    {
      $skills = unserialize( rgar( $entry, '7' ) );
      wp_set_post_terms( $postID, $skills, 'skill' );
    }

    // check if the downloadables are empty
    if ( !empty( rgar( $entry, '10' ) ) )
    {

      $downloadablesRaw = json_decode( rgar( $entry, '10' ) );
      $items = array();
      $rows = array();

      $default = get_field( 'project_downloadables', $postID );

      if ( !empty( $default ) )
      {
        foreach ( $default as $item )
        {
          if ( empty( $item[ 'file' ][ 'ID' ] ) )
          {
            continue;
          }

          $rows[] = $item[ 'file' ][ 'ID' ];
        }
      }

      foreach ( $downloadablesRaw as $key => $link )
      {

        $itemID = Utils::getAttachmentIDFromUrl( $link, $postID );
        if ( in_array( $itemID, $rows ) )
        {
          continue;
        }

        $item[ 'file' ] = $itemID;
        $rows[] = $item;
      }

      $items[ 'downloadables' ] = $rows;

      update_field( 'project_downloadables', $items, $postID );

    }

    // check if the links are empty
    if ( strlen( rgar( $entry, '9' ) ) > 2 )
    {

      $linksRaw = unserialize( rgar( $entry, '9' ) );
      $items = array();
      $rows = array();

      foreach ( $linksRaw as $key => $link )
      {

        $item[ 'link' ] = array(
          'url' => $link[ 'Link' ],
          'target' => '_blank',
          'title' => $link[ 'Title' ],
        );

        $rows[] = $item;

      }

      $items[ 'links' ] = $rows;

      update_field( 'project_links', $items, $postID );
    }

  }/* gform_after_submission__createUpdatePostForProject() */

  /**
   * popuplate the creative commons select field
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $form
   * @return
   */

  public function gform__populateCreativeCommons( $form )
  {

    // get the data from the JS file
    $optionsRaw = wp_remote_get( 'http://api.creativecommons.org/rest/dev/simple/chooser.js' );

    if ( empty( $optionsRaw[ 'body' ] ) )
    {
      return;
    }

    $optionsRaw = $optionsRaw[ 'body' ];
    $options = array();

    $optionsRaw = str_replace( '(\'', '', $optionsRaw );
    $optionsRaw = str_replace( '\');', '', $optionsRaw );
    $optionsRaw = explode( 'document.write', $optionsRaw );

    foreach ( $optionsRaw as $option ) {

      if ( empty( $option ) )
      {
        continue;
      }

      $option = str_replace( '<option value="', '', $option );
      $option = str_replace( '</option>', '', $option );

      $optionExpl = explode( '">', $option );

      $options[] = array(
        'text' => $optionExpl[1],
        'value' =>  $optionExpl[1],
      );
    }

    // set the field
    foreach ( $form['fields'] as &$field )
    {
      if ( $field->inputName != 'creative_commons' )
      {
        continue;
      }

      $field->choices = $options;
      $field->placeholder = 'Select Creative Commons License';
    }

    return $form;

  }/* gform__populateCreativeCommons() */

  /**
   * pre populate the fields based on the user data
   * in the backend
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $form
   * @return
   */

  public function gform__prePopulateFields( $form )
  {

    $postID = $_REQUEST[ 'post_id' ];
    $post = get_post( $postID );
    $postFields = $postID ? get_fields( $postID ) : array();

    foreach ( $form[ 'fields' ] as &$field )
    {

      if ( !empty( $postFields[ 'images' ] ) )
      {
        if ( strpos( $field->cssClass, 'hp-image-gallery-list' ) > 0 )
        {
          $field->content = '<script>var imageGallery = ' . json_encode( $postFields[ 'images' ] ) . '</script>';
        }

        if ( strpos( $field->cssClass, 'hp-image-gallery' ) > 0 && strpos( $field->cssClass, 'gallery-list' ) <= 0 )
        {
          $field->defaultValue = json_encode( $postFields[ 'images' ] );
        }
      }

      if ( !empty( $postFields[ 'project_downloadables' ][ 'downloadables' ] ) )
      {
        if ( strpos( $field->cssClass, 'hp-downloadables-list' ) > 0 )
        {
          $field->content = '<script>var downloadableList = ' . json_encode( $postFields[ 'project_downloadables' ][ 'downloadables' ] ) . '</script>';
        }
      }

      if ( strpos( $field->cssClass, 's-feature-image' ) > 0 )
      {
        $field->content = get_the_post_thumbnail_url( $postID );
      }

      switch ( $field->type ) {

        case 'post_content':
          if ( !empty( $post->post_content ) )
          {
            $field->defaultValue = $post->post_content;
          }

          break;

        default:

          break;
      }

      switch ( $field->id ) {
        case $this->titleFieldID:
          $field->defaultValue = $post->post_title;
          break;

        case $this->stageFieldID:

          $terms = get_the_terms( $postID, 'stage' );

          if ( !empty( $terms ) )
          {
            $stage = array();
            foreach ( $terms as $status ) {
              $stage[] = $status->name;
            }

            $field->defaultValue = $stage[0];
          }

          break;

        case $this->projectIDFieldID:
          $field->defaultValue = $postID;
          break;

        case $this->editProjectFieldID:
          if ( ( isset( $_REQUEST[ 'edit' ] ) && $_REQUEST[ 'edit' ] == 'true' ) && isset( $_REQUEST[ 'post_id' ] ) && !empty( $_REQUEST[ 'post_id' ] ) )
          {
            $field->defaultValue = 'true';
          }
          break;

        case $this->skillsFieldID:

          $terms = get_the_terms( $postID, 'skill' );

          if ( !empty( $terms ) )
          {
            $skills = array();
            foreach ( $terms as $skill ) {
              $skills[] = $skill->name;
            }

            $field->defaultValue = serialize( $skills );
          }

          break;

        case $this->needsFieldID:

          $terms = get_the_terms( $postID, 'need' );

          if ( !empty( $terms ) )
          {
            $needs = array();
            foreach ( $field->choices as &$choice )
            {
              $isChosen = array_filter( $terms, function( $term ) use ( $choice ) {
                if ( $choice[ 'value' ] == $term->name ) {
                  return $term->name;
                }
              });

              if ( !empty( $isChosen ) )
              {
                $choice[ 'isSelected' ] = true;
              }

              $needs = array_merge( $needs, $isChosen );
            }

            echo '<script>var projectNeeds = ' . json_encode( $needs ) . '</script>';

          }

          break;

        case $this->projectUrlFieldID:
          $field->defaultValue = get_permalink( $postID );
          break;

        default:
          if ( !empty( $postFields[ $this->fieldProjectMetaMap[ $field->id ] ] ) && isset( $this->fieldProjectMetaMap[ $field->id ] ) ) {
            $field->defaultValue = $postFields[ $this->fieldProjectMetaMap[ $field->id ] ];
          }

          break;
      }
    }


    return $form;

  }/* gform__prePopulateFields() */


  /**
   * pre-fill links field
   *
   * @author Ynah Pantig
   * @param $value
   * @return
   */

  public function gform__prePopulateProjectLinkField( $value )
  {
    $postID = $_REQUEST[ 'post_id' ];
    $post = get_post( $postID );
    $postFields = $postID ? get_fields( $postID ) : array();
    $value = [];

    if ( !empty( $postFields[ 'project_links' ][ 'links' ] ) )
    {
      foreach ( $postFields[ 'project_links' ][ 'links' ] as $link )
      {
        if ( $link[ 'link' ] == '' ) {
          continue;
        }

        $item = [
          'Title' => $link[ 'link' ][ 'title' ],
          'Link' => $link[ 'link' ][ 'url' ],
        ];

        $value[] = $item;
      }

      return $value;
    }

  }

  public function wp_project_publish_hook($postID, $postObj)
  {
    ForumIntegration::CreateEditProject($postId, $postObj);
  }
}
