<?php

namespace MMC;

class ProfilePostsController extends \WP_REST_Posts_Controller
{

  /**
   * list the projects
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $data
   * @return
   */

  public function get_items( $data )
  {

    // get the parameters/arguments of the query
    $params = $data->get_params();

    // set the default arguments for this controller
    $defaultArgs = array(
      'posts_per_page' => -1,
    );

    $args = array_merge( $defaultArgs, $params );

    $query = new \WP_Query( $args );

    // get the data
    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $postID = $query->post->ID;
      $query->post->fields = get_fields( $postID );

    endwhile; wp_reset_postdata(); endif;

    $response = $this->prepare_response_for_collection( $query->posts );

    // return the data
    return $response;

  }/* alphabeticalHeadings() */

} /* class Customizer() */
