<?php

namespace MMC;

use GFCommon;

/**
 * HookHub interface class.
 */
class HookHub
{
    private static $_headers = [
        'Content-Type: application/json; charset=utf-8',
        'X-API-Key: '.HOOKHUB_KEY,
    ];

    private static $_unsafeFields = [
        'password', 'user_password',
    ];

    public static function dump($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data, JSON_PRETTY_PRINT));
    }

    public static function request($request_data)
    {
        $hookhub_handler = curl_init(HOOKHUB_URI);

        GFCommon::log_debug(sprintf('%s[%d]: headers: %s', __METHOD__, __LINE__, json_encode(self::$_headers)));
        GFCommon::log_debug(sprintf('%s[%d]: payload: %s', __METHOD__, __LINE__, self::safeJSON($request_data)));

        curl_setopt($hookhub_handler, CURLOPT_POST, 1);
        curl_setopt($hookhub_handler, CURLOPT_HTTPHEADER, self::$_headers);
        curl_setopt($hookhub_handler, CURLOPT_POSTFIELDS, self::safeJSON($request_data, true));
        curl_setopt($hookhub_handler, CURLOPT_RETURNTRANSFER, 1);

        $request_result = curl_exec($hookhub_handler);

        $result_info = curl_getinfo($hookhub_handler);

        curl_close($hookhub_handler);

        GFCommon::log_debug(sprintf('%s[%d]: request result info: %s', __METHOD__, __LINE__, json_encode($result_info)));
        GFCommon::log_debug(sprintf('%s[%d]: request result: %s', __METHOD__, __LINE__, $request_result));

        return ['request' => $request_data, 'info' => $result_info, 'result' => $request_result];
    }

    public static function safeJSON($data, $isPayload = false)
    {
        $data = is_string($data) ? json_decode($data, true) : $data;

        if (!$isPayload) {
            foreach(self::$_unsafeFields as $unsafeKey) {
                unset($data[$unsafeKey]);
            }
        }

        if (is_array($data)) {
            foreach ($data as $idx => $entry) {
                if (is_string($entry)) {
                    $unserialized = @unserialize($entry);
                    if (false != $unserialized && $unserialized != $entry) {
                        GFCommon::log_debug(sprintf('%s[%d]: %s -> %s <-> %s', __METHOD__, __LINE__, $idx, $entry, json_encode($unserialized)));
                        $data[$idx] = $unserialized;
                    }
                }
            }
        }

        return json_encode($data);
    }
}
