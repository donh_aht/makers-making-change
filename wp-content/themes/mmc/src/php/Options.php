<?php

  namespace MMC;

  /**
   * Timber setup
   */
  class Options
  {
    /**
    * register hooks
    */
    function __construct()
    {

      // Add global data to timber context
      add_action( 'timber_context', array( &$this, 'timber_context__addSiteOptions' ) );

    } /* __construct() */

    /**
     * add menus to timber context
     */

    public function timber_context__addSiteOptions( $context )
    {

      $options = array();

      $context[ 'options' ] = $options;

      // return context
      return $context;

    } /* timber_context() */

  } /* class Timber() */
