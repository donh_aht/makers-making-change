<?php

namespace MMC;

class Registration
{
  /**
   * User data:
   * - First Name
   * - Last Name
   * - Nickname -> {First Name}
   * - Display Name -> {First Name} {Last Name}
   * - Roles
   *
   * User Meta:
   * - Location
   *
   * ACF:
   * - Skills (user_skills)
   * - Interests (user_interests)
   * - Industry (user_industry)
   * - School (user_school_name)
   */

  private $formID = 5;

  // fields ids
  private $firstNameFieldID = 8;
  private $lastNameFieldID = 9;
  private $geolocationFieldID = 38;
  private $rolesFieldID = 4;
  private $interestsFieldID = 3;
  private $industryFieldID = 35;
  private $skillsFieldID = 11;
  private $schoolFieldID = 37;
  private $locationFieldID = 10;

  private $fieldUserMetaMap = array();
  private $fieldTaxonomyMap = array();

  public $interestsMetaKey = 'user_interests';
  public $industryMetaKey = 'user_industry';
  public $skillsMetaKey = 'user_skills';
  public $schoolMetaKey = 'user_school';
  public $firstNameMetaKey = 'first_name';
  public $lastNameMetaKey = 'last_name';
  public $displayNameMetaKey = 'display_name';
  public $nicknameMetaKey = 'nickname';
  public $profileCompletedMetaKey = 'profile_completed';

  public function __construct()
  {
    // field IDs <--> User meta map
    $this->fieldUserMetaMap[$this->interestsFieldID] = $this->interestsMetaKey;
    $this->fieldUserMetaMap[$this->industryFieldID] = $this->industryMetaKey;
    $this->fieldUserMetaMap[$this->skillsFieldID] = $this->skillsMetaKey;
    $this->fieldUserMetaMap[$this->schoolFieldID] = $this->schoolMetaKey;
    $this->fieldUserMetaMap[$this->firstNameFieldID] = $this->firstNameMetaKey;
    $this->fieldUserMetaMap[$this->lastNameFieldID] = $this->lastNameMetaKey;

    // field IDs <--> taxonomy
    $this->fieldTaxonomyMap[$this->interestsFieldID] = 'interest';
    $this->fieldTaxonomyMap[$this->industryFieldID] = 'industry';
    $this->fieldTaxonomyMap[$this->skillsFieldID] = 'skill';

    add_action( 'gform_validation_' . $this->formID, array( &$this, 'gform_validation' ) );
    add_action( 'gform_after_submission_' . $this->formID, array( &$this, 'gform_after_submission__saveUserData' ), 10, 2 );
    add_filter( 'gform_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_pre_validation_' . $this->formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_admin_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_pre_submission_filter_' . $this->formID, array( &$this, 'gform__populateFields' ), 10 );

    add_filter( 'gform_field_validation_' . $this->formID . '_' . $this->locationFieldID, array( &$this, 'gform_field_validation__validateLocationField' ), 10, 4 );

  } /* __construct() */

  function gform_validation( $validation_result ) {
    $userID = get_current_user_id();
    if (!$userID) {
      wp_redirect(site_url('/login/'));
      exit();
    }

    $profile_completed = get_user_meta($userID, $this->profileCompletedMetaKey, true);
    if ($profile_completed) {
      wp_redirect(site_url('/profile/'));
      exit();
    }

    return $validation_result;
  }

  public function gform_after_submission__saveUserData( $entry, $form )
  {
    $userID = get_current_user_id();
    if (!$userID) {
      return;
    }

    $userObj = new \WP_User($userID);
    $userData = array( 'ID' => $userID );

    foreach ( $form['fields'] as $field ) {
      $value = $field->get_value_export($entry);
      if (empty($value)) {
        continue;
      }

      switch ($field->id) {
        case $this->firstNameFieldID:
        case $this->lastNameFieldID:
          $userData[$this->fieldUserMetaMap[$field->id]] = $value;
          break;

        case $this->rolesFieldID:
          $valueArr = explode(', ', $value);
          foreach ($field->choices as $choice) {
            if (in_array($choice['value'], $valueArr)) {
              $userObj->add_role($choice['value']);
            } else {
              $userObj->remove_role($choice['value']);
            }
          }
          break;

        case $this->interestsFieldID:
        case $this->industryFieldID:
        case $this->skillsFieldID:
          $valueArr = explode(', ', $value);
          update_field($this->fieldUserMetaMap[$field->id], $valueArr, 'user_' . $userID);
          break;

        case $this->schoolFieldID:
          update_field($this->fieldUserMetaMap[$field->id], $value, 'user_' . $userID);
          break;

        case $this->geolocationFieldID:
          Location::updateUserGeolocationData($userID, json_decode($value, true));
          break;

        default:
          # code...
          break;
      }
    }

    // Update names
    if (!empty($userData[$this->firstNameMetaKey])) {
      $userData[$this->displayNameMetaKey] = $userData[$this->firstNameMetaKey];
      $userData[$this->nicknameMetaKey] = $userData[$this->firstNameMetaKey];

      if (!empty($userData[$this->lastNameMetaKey])) {
        $userData[$this->displayNameMetaKey] .= ' ' . $userData[$this->lastNameMetaKey];
      }
    }

    wp_update_user($userData);

    // set profile completed flag
    update_user_meta($userID, $this->profileCompletedMetaKey, true);

  }/* gform_after_submission__saveUserData() */

  public function gform__populateFields($form)
  {
    foreach ( $form['fields'] as &$field )
    {
      switch ($field->id) {
        case $this->interestsFieldID:
        case $this->industryFieldID:
        case $this->skillsFieldID:
          $field->choices = GravityForms::getTaxonomyFieldChoices($this->fieldTaxonomyMap[$field->id]);
          $field->inputs = GravityForms::getFieldInputs($field->id, $field->choices);
          break;

        default:
          # code...
          break;
      }
    }

    return $form;

  }/* gform__populateFields() */


  /**
   * validate the location field if they selected from a list
   * or just typed in a city
   *
   * @author Ynah Pantig
   * @param $result, $value, $form, $field
   * @return
   */

  public function gform_field_validation__validateLocationField( $result, $value, $form, $field )
  {

    if ( $value != '' && $result['is_valid'] ) {
      $geolocation = rgpost( 'input_' . $this->geolocationFieldID );
      $result = GravityForms::validateLocationField( $geolocation, $result );
    }

    return $result;

  }


} /* class ClassName() */
