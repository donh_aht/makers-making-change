<?php

namespace MMC;

class SignUp
{

  public static $formID = 6;

  // fields ids
  private $emailFieldID = 1;
  private $passwordFieldID = 6;
  private $termsFieldID = 8;
  private $privacyPolicyFieldID = 9;
  private $mailingListFieldID = 11;

  /**
   * construct
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function __construct(  )
  {

    // auto generate unique username
    add_filter( 'gform_username_' . static::$formID, array( &$this, 'gform_username__uniqueUsername' ), 10, 4 );
    add_action( 'gform_user_registered', array( &$this, 'gform_user_registered__optinCheckbox' ), 10, 4 );

  }

  public function gform_username__uniqueUsername($username, $feed, $form, $entry)
  {
    $email = rgar($entry, 1);
    $emailArr = explode('@', $email);
    $username = strtolower($emailArr[0]);

    $username = Utils::cleanString( $username );

    if ( username_exists( $username ) ) {
      $i = 2;

      while ( username_exists( $username . $i ) ) {
        $i++;
      }

      $username = $username . $i;
    };

    // error_log( 'USERNAME: ' . $username );

    return $username;
  }


  /**
   * save the value of the optin checkbox
   *
   * @author Ynah Pantig
   * @param $entry, $form
   * @return
   */

  public function gform_user_registered__optinCheckbox( $user_id, $feed, $entry, $user_pass )
  {

    $newsletter = $entry[ $this->mailingListFieldID . '.1' ];
    update_field( 'user_newsletter_subscription', $newsletter, 'user_' . $user_id );

  }



}
