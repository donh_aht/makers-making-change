<?php

namespace MMC;

class Tracking {

  private static $uploadsDir;
  private static $uploadsBaseUrl;

  public function __construct() {

    static::$uploadsDir = wp_upload_dir();
    static::$uploadsBaseUrl = trailingslashit( static::$uploadsDir[ 'baseurl' ] );

    if ( is_admin() ) {
      if ( isset( $_REQUEST[ 'run_cron' ] ) && $_REQUEST[ 'run_cron' ] == 'run' ) {
        $this->run_cron_daily__compileDataToCSV();
      }
    }

    add_action( 'wp_ajax_trigger_csv_build', array( $this, 'ajax__triggerCompileDataToCSV' ) );
    add_action( 'wp_ajax_nopriv_trigger_csv_build', array( $this, 'ajax__triggerCompileDataToCSV' ) );

    // add_action( 'init', array( $this, 'init__scheduleCron' ) );
    // add_action( 'run_cron_daily', array( &$this, 'run_cron_daily__compileDataToCSV' ) );

  }


  /**
   * schedule a cron to compile the data and save to CSV file every night
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function init__scheduleCron(  )
  {

    if ( !wp_next_scheduled( 'run_cron_daily' ) ) {
      wp_schedule_event( strtotime( '12:00am' ), 'daily', 'run_cron_daily' );
    }

  }


  /**
   * compile the data once the cron runs
   * by calling the data table functions
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function run_cron_daily__compileDataToCSV(  )
  {

    DataTables::buildCSV( 'users', DataTables::getUserData() );
    DataTables::buildCSV( 'requests', DataTables::getRequestData() );
    DataTables::buildCSV( 'ideas', DataTables::getIdeaData() );
    DataTables::buildCSV( 'projects', DataTables::getProjectsData() );
    DataTables::buildCSV( 'events', DataTables::getEventsData() );
    DataTables::buildCSV( 'connections', DataTables::getConnectionsData() );

  }


  /**
   * trigger a data compile to save as CSV
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function ajax__triggerCompileDataToCSV(  )
  {

    $file = $_REQUEST[ 'file' ];

    switch ( $file ) {

      case 'users':
        DataTables::buildCSV( 'users', DataTables::getUserData() );
        break;

      case 'requests':
        DataTables::buildCSV( 'requests', DataTables::getRequestData() );
        break;

      case 'ideas':
        DataTables::buildCSV( 'ideas', DataTables::getIdeaData() );
        break;

      case 'projects':
        DataTables::buildCSV( 'projects', DataTables::getProjectsData() );
        break;

      case 'events':
        DataTables::buildCSV( 'events', DataTables::getEventsData() );
        break;

      case 'connections':
        DataTables::buildCSV( 'connections', DataTables::getConnectionsData() );
        break;

      default:
        # code...
        break;
    }

    die();

  }


  /**
   * build out the tracking page in the backend
   *
   * @author Ynah Pantig
   * @param
   * @return $data
   */

  static public function buildPageMarkup()
  {

    $data = array(
      'users',
      'requests',
      'ideas',
      'projects',
      'events',
      'connections',
    );

    $markup = '';

    $markup .= '<div class="wrap">';
      $markup .= '<h1>Data Tracking</h1>';
      $markup .= '<div id="poststuff">';
        $markup .= '<table cellpadding="0" cellspacing="0" class="table-tracking wp-list-table widefat fixed striped">';
          $markup .= '<thead>';
            $markup .= '<tr>';
              $markup .= '<th class="manage-column table-col--data table-col">Data</th>';
              $markup .= '<th class="manage-column table-col--actions table-col">Actions</th>';
            $markup .= '</tr>';
          $markup .= '</thead>';
          $markup .= '<tbody>';

            foreach ( $data as $item ) {
              $markup .= '<tr>';
                $markup .= '<td class="table-col--data table-col">';
                  $markup .= ucfirst( $item );
                $markup .= '</td>';

                $markup .= '<td class="table-col--actions table-col">';
                  $markup .= '<button class="button button-primary button-large" data-file="' . static::$uploadsBaseUrl . 'tracking/' . $item . '.csv" download data-download="' . $item . '" class="btn">Download CSV</button><span class="spinner"></span>';
                $markup .= '</td>';
              $markup .= '</tr>';
            }


          $markup .= '</tbody>';
        $markup .= '</table>';
      $markup .= '</div>';
    $markup .= '</div>';

    echo $markup;

  }


}
