<?php

namespace MMC;

class ACF
{
  public function __construct(  )
  {

    add_filter( 'acf/settings/save_json', array( &$this, 'acf_save_json__loadJsonFiles' ) );
    add_filter( 'acf/settings/load_json', array( &$this, 'acf_load_json__loadJsonFiles' ) );

    add_filter( 'acf/fields/wysiwyg/toolbars', array( &$this, 'acf_wysiwyg__toolbars' ) );

    add_filter( 'tiny_mce_before_init', array( &$this, 'tiny_mce_before_init__removeSomeHeadings' ) );
    add_filter( 'tiny_mce_before_init', array( &$this, 'tiny_mce_before_init__addStyleFormats' ) );

    add_filter( 'mce_buttons_2', array( &$this, 'mce_buttons_2__addButtons' ) );

    add_filter( 'acf/settings/remove_wp_meta_box', '__return_false' );

    add_filter( 'acf/load_field/name=rating', array( &$this, 'acf_load_field__ratings' ) );
    add_filter( 'acf/load_field/name=user_industry', array( &$this, 'acf_load_field__userIndustry' ) );

  }/* __construct() */

  public function acf_save_json__loadJsonFiles( $path )
  {
    // update path
    $path = dirname( __DIR__ ) . '/acf-json';
    return $path;
  }

  public function acf_load_json__loadJsonFiles( $paths )
  {

    // remove original path (optional)
    unset($paths[0]);

    // append path
    $paths[] = dirname( __DIR__ ) . '/acf-json';
    return $paths;

  }

  /**
   * edit the ACF wysiwyg toolbar options
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $toolbars
   * @return
   */

  public function acf_wysiwyg__toolbars( $toolbars )
  {

    $toolbars[ 'Link' ] = array();
    $toolbars[ 'Link' ][1] = array( 'link', 'unlink' );


    $basic = $toolbars[ 'Basic' ][1];
    $toolbars[ 'Basic' ][1] = array_merge( $basic, array( 'styleselect' ) );

    return $toolbars;

  }


  /**
   * remove h1 from the editor
   *
   * @author Ynah Pantig <ynah@ynahpantig.com>
   * @package ypantig/package
   * @since 1.0.0
   */
  public function tiny_mce_before_init__removeSomeHeadings( $formats )
  {

    $formats[ 'block_formats' ] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Preformatted=pre;';

    return $formats;

  } /* tiny_mce_before_init__removeSomeHeadings() */


  /**
   * add buttons to tinymce
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $buttons
   * @return
   */

  public function mce_buttons_2__addButtons( $buttons )
  {

    $newButtons = array( 'styleselect' );
    $buttons = array_merge( $buttons, $newButtons );

    return $buttons;

  }/* mce_buttons_2__addButtons() */


  /**
   * add style formats
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $formats
   * @return
   */

  public function tiny_mce_before_init__addStyleFormats( $formats )
  {

    $styleFormats = array(
      // Each array child is a format with it's own settings
      array(
        'title' => 'Small',
        'inline' => 'small',
      ),
      // Each array child is a format with it's own settings
      array(
        'title' => 'Button: Outline Large',
        'selector' => 'a',
        'classes' => 'btn btn--outline btn--primary',
      ),
      // Each array child is a format with it's own settings
      array(
        'title' => 'Button: Outline',
        'selector' => 'a',
        'classes' => 'btn btn--outline btn--primary btn--outline--small',
      ),
      // Each array child is a format with it's own settings
      array(
        'title' => 'Button: Solid',
        'selector' => 'a',
        'classes' => 'btn btn--solid btn--primary',
      ),
    );

    $formats[ 'style_formats' ] = json_encode( $styleFormats );

    return $formats;

  }/* tiny_mce_before_init__addStyleFormats() */


  /**
   * load checkbox options from the skills field in the uoser options
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $field
   * @return
   */

  public function acf_load_field__roles( $field )
  {

    // reset the field
    $field[ 'choices' ] = array();

    $choices = Post::getTaxonomy( 'skill' );
    $choices = array_map( 'trim', $choices );

    if ( is_array( $choices ) )
    {
      foreach ( $choices as $key => $choice )
      {
        $field[ 'choices' ][ $key ] = $choice;
      }
    }

    return $field;

  }/* acf_load_field__roles() */


  /**
   * load checkbox options from the skills field in the uoser options
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $field
   * @return
   */

  public function acf_load_field__userIndustry( $field )
  {

    // reset the field
    $field[ 'choices' ] = array();
    $choices = Post::getTaxonomy( 'industry' );

    if ( is_array( $choices ) )
    {
      foreach ( $choices as $key => $choice )
      {
        $field[ 'choices' ][ $key ] = $choice;
      }
    }

    return $field;

  }/* acf_load_field__userIndustry() */

  /**
   * load radio button options for the ratings
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $field
   * @return
   */

  public function acf_load_field__ratings( $field )
  {

    // reset the field
    $field[ 'choices' ] = array();

    for ( $i = 1; $i <= 5; $i++ )
    {
      $field[ 'choices' ][ $i ] = '';
    }

    return $field;

  }/* acf_load_field__ratings() */


} /* class ACF() */
