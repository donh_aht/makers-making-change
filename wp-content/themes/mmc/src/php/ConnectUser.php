<?php

namespace MMC;

class ConnectUser
{
  /**
   * User data:
   * - First Name
   * - Last Name
   * - Nickname -> {First Name}
   * - Display Name -> {First Name} {Last Name}
   * - Roles
   *
   * User Meta:
   * - Location
   *
   * ACF:
   * - Skills (user_skills)
   */

  public static $formID = 12;

  // fields ids
  private $makerFieldID = 7;
  private $userFieldID = 8;
  private $userNameFieldID = 4;
  private $userEmailFieldID = 5;
  private $makerEmailFieldID = 3;
  private $makerNameFieldID = 6;
  private $connectionFieldID = 9;
  private $makerLocationFieldID = 10;
  private $userLocationFieldID = 11;

  // private $fieldUserMetaMap = array();
  // private $fieldTaxonomyMap = array();

  // public $skillsMetaKey = 'user_skills';
  // public $firstNameMetaKey = 'first_name';
  // public $lastNameMetaKey = 'last_name';
  // public $displayNameMetaKey = 'display_name';
  // public $nicknameNameMetaKey = 'nickname';
  // public $facebookMetaKey = 'user_facebook';
  // public $twitterMetaKey = 'user_twitter';
  // public $instagramMetaKey = 'user_instagram';
  // public $youtubeMetaKey = 'user_youtube';
  // public $thingiverseMetaKey = 'user_thingiverse';
  // public $githubMetaKey = 'user_github';

  public function __construct()
  {
    // field IDs <--> User meta map
    // $this->fieldUserMetaMap[$this->skillsFieldID] = $this->skillsMetaKey;
    // $this->fieldUserMetaMap[$this->firstNameFieldID] = $this->firstNameMetaKey;
    // $this->fieldUserMetaMap[$this->lastNameFieldID] = $this->lastNameMetaKey;
    // $this->fieldUserMetaMap[$this->facebookFieldID] = $this->facebookMetaKey;
    // $this->fieldUserMetaMap[$this->twitterFieldID] = $this->twitterMetaKey;
    // $this->fieldUserMetaMap[$this->instagramFieldID] = $this->instagramMetaKey;
    // $this->fieldUserMetaMap[$this->youtubeFieldID] = $this->youtubeMetaKey;
    // $this->fieldUserMetaMap[$this->thingiverseFieldID] = $this->thingiverseMetaKey;
    // $this->fieldUserMetaMap[$this->githubFieldID] = $this->githubMetaKey;

    // field IDs <--> taxonomy
    // $this->fieldTaxonomyMap[$this->skillsFieldID] = 'skill';

    add_filter( 'gform_validation_' . static::$formID, array( '\\MMC\\GravityForms', 'gform_validation__redirectToLogin' ) );

    add_filter( 'gform_pre_render_' . static::$formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
    add_filter( 'gform_pre_validation_' . static::$formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
    add_filter( 'gform_admin_pre_render_' . static::$formID, array( &$this, 'gform__populateFields' ) );

    // Note: this will allow for the labels to be used during the submission process in case values are enabled
    add_filter( 'gform_pre_submission_filter_' . static::$formID, array( &$this, 'gform__populateFields' ), 10 );

    add_action( 'gform_after_submission_' . static::$formID, array( &$this, 'gform_after_submission__connectUsers' ), 10, 2 );
    add_filter( 'gform_confirmation_' . static::$formID, array( &$this, 'gform_confirmation__dynamicallyChangeConfirmation' ), 10, 4 );

  } /* __construct() */


  /**
   * check for the user ID before showing anything
   *
   * @author Ynah Pantig
   * @param $form
   * @return
   */

  public function gform__populateFields( $form )
  {

    $userID = get_current_user_id();

    if (!$userID && rgar( $form, 'requireLogin' )) {
      $form['requireLoginBkp'] = true;
      $form['requireLogin'] = false;
      $form['requireLoginMessage'] = GravityForms::promptUserLogin();
      return $form;
    }

    return $form;

  }

  /**
   * change the confirmation message to add the user's name
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $confirmation, $form, $entry, $ajax
   * @return
   */

  public function gform_confirmation__dynamicallyChangeConfirmation( $confirmation, $form, $entry, $ajax )
  {

    if ( strpos( '{$user}', $confirmation ) > 0 )
    {
      $user = get_user_by_email( rgar( $entry, 3 ) );

      $confirmation = str_replace( '{$user}', $user->display_name, $confirmation );
      $confirmation = str_replace( '{$email}', rgar( $entry, 3 ), $confirmation );
    }

    return $confirmation;

  }/* gform_confirmation__dynamicallyChangeConfirmation() */

  /**
   * connect the users after submitting the connect to user form
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $entry, $form
   * @return
   */

  public function gform_after_submission__connectUsers( $entry, $form )
  {
    /**
     * $entry
     * 1 = subject
     * 2 = message
     * 3 = maker email
     * 4 = user name
     * 5 = user email
     * 6 = maker name
     * 7 = maker ID
     * 8 = user ID
     */

    $currentUserID = get_current_user_id();

    if ( !$currentUserID ) {
      return;
    }

    $connectUserID = rgar( $entry, 7 );
    $currentUserConnections = get_field( 'connection_users', 'user_' . $currentUserID );
    $currentConnectionList = array();

    if ( !empty( $currentUserConnections ) )
    {
      // check if the users are already connected
      foreach ( $currentUserConnections as $key => $value ) {

        if ( $value['ID'] == $connectUserID ) {
          return;
        } else {
          $currentConnectionList[] = $value['ID'];
        }
      }
    }

    $currentConnectionList[] = (int) $connectUserID;

    $connectUserConnections = get_field( 'connection_users', 'user_' . $connectUserID );
    $connectConnectionList = array();

    if ( !empty( $connectUserConnections ) )
    {
      // check if the users are already connected
      foreach ( $connectUserConnections as $key => $value ) {
        if ( $value['ID'] == $currentUserID ) {
          return;
        } else {
          $connectConnectionList[] = $value['ID'];
        }
      }
    }

    $connectConnectionList[] = (int) $currentUserID;

    update_field( 'connection_users', $currentConnectionList, 'user_' . $currentUserID );
    update_field( 'connection_users', $connectConnectionList, 'user_' . $connectUserID );

    // Forum Integration
    ForumIntegration::ConnectUser($entry, $currentUserID, $connectUserID, $entry[1], $entry[2]);

  }/* gform_after_submission__connectUsers() */

}
