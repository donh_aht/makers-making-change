<?php

  namespace MMC;

  /**
   * Timber extensions
   */
  class Site extends \TimberSite
  {

    /**
     * App root URL utility
     */
    public function appTemplateUrl()
    {
      return get_template_directory_uri();

    } /* appTemplateUrl() */


    /**
     * Add public site data
     */
    public function data()
    {
      // Check if cached data is available
      if ( !isset( $this->data_cache ) )
      {
        $data = array();

        if( defined( 'FACEBOOK_APP_ID' ) )
        {
          $data['facebookAppId'] = FACEBOOK_APP_ID;
        }

        // facebook channel URL
        $data['facebookChannelUrl'] = get_template_directory_uri() . '/facebook-channel.php';

        // url for ajax calls
        $data['ajaxUrl']  = admin_url('admin-ajax.php');

        $data['request']  = $_REQUEST;

        // output the current GMT offset
        $data['gmt_offset'] = get_option('gmt_offset');
        $data['today_year'] = date( 'Y' );

        // site.appTemplateUrl
        $data['appTemplateUrl'] = $this->appTemplateUrl();

        $data[ 'options' ] = get_fields( 'options' );
        $data[ 'src' ] = get_stylesheet_directory() . '/src/';

        $this->data_cache = $data;
      }

      return $this->data_cache;

    } /* data() */

    public function encode_email_address($email)
    {
      $output = '';
      for ($i = 0; $i < strlen($email); $i++)
      {
        $output .= '&#'.ord($email[$i]).';';
      }
      return $output;

    } /* encode_email_address() */

  } /* class Site() */
