<?php

namespace MMC;

class SignUpRegistration
{

  public static $formID = 15;

  // fields ids
  private $emailFieldID = 39;
  private $passwordFieldID = 45;
  private $termsFieldID = 46;
  private $privacyPolicyFieldID = 47;
  private $mailingListFieldID = 48;

  // fields ids
  private $firstNameFieldID = 8;
  private $lastNameFieldID = 9;
  private $geolocationFieldID = 38;
  private $rolesFieldID = 4;
  private $interestsFieldID = 3;
  private $industryFieldID = 49;
  private $skillsFieldID = 11;
  private $schoolFieldID = 37;
  private $locationFieldID = 10;

  private $fieldUserMetaMap = array();
  private $fieldTaxonomyMap = array();

  public $interestsMetaKey = 'user_interests';
  public $industryMetaKey = 'user_industry';
  public $skillsMetaKey = 'user_skills';
  public $schoolMetaKey = 'user_school';
  public $firstNameMetaKey = 'first_name';
  public $lastNameMetaKey = 'last_name';
  public $displayNameMetaKey = 'display_name';
  public $nicknameMetaKey = 'nickname';
  public $profileCompletedMetaKey = 'profile_completed';

  /**
   * construct
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function __construct()
  {
    if (  Utils::getEnv() != 'prod' ) {
      static::$formID = 13;
    }

    // field IDs <--> User meta map
    $this->fieldUserMetaMap[$this->interestsFieldID] = $this->interestsMetaKey;
    $this->fieldUserMetaMap[$this->industryFieldID] = $this->industryMetaKey;
    $this->fieldUserMetaMap[$this->skillsFieldID] = $this->skillsMetaKey;
    $this->fieldUserMetaMap[$this->schoolFieldID] = $this->schoolMetaKey;
    $this->fieldUserMetaMap[$this->firstNameFieldID] = $this->firstNameMetaKey;
    $this->fieldUserMetaMap[$this->lastNameFieldID] = $this->lastNameMetaKey;

    // field IDs <--> taxonomy
    $this->fieldTaxonomyMap[$this->interestsFieldID] = 'interest';
    $this->fieldTaxonomyMap[$this->industryFieldID] = 'industry';
    $this->fieldTaxonomyMap[$this->skillsFieldID] = 'skill';

    // add_action( 'gform_validation_' . static::$formID, array( &$this, 'gform_validation' ) );
    // add_action( 'gform_after_submission_' . static::$formID, array( &$this, 'gform_after_submission__saveUserData' ), 10, 2 );
    //
    add_filter( 'gform_pre_render_' . static::$formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_pre_validation_' . static::$formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_admin_pre_render_' . static::$formID, array( &$this, 'gform__populateFields' ) );
    add_filter( 'gform_pre_submission_filter_' . static::$formID, array( &$this, 'gform__populateFields' ), 10 );

    add_filter( 'gform_field_validation_' . static::$formID . '_' . $this->locationFieldID, array( &$this, 'gform_field_validation__validateLocationField' ), 10, 4 );

    // auto generate unique username
    add_filter( 'gform_username_' . static::$formID, array( &$this, 'gform_username__uniqueUsername' ), 10, 4 );

    add_action( 'gform_user_registered', array( &$this, 'gform_user_registered__optinCheckbox' ), 10, 4 );
    add_action( 'gform_user_registered', array( &$this, 'gform_user_registered__forumIntegration' ), 10, 4 );
  }

  public function gform_username__uniqueUsername($username, $feed, $form, $entry)
  {
    $email = rgar($entry, $this->emailFieldID);
    $emailArr = explode('@', $email);
    $username = strtolower($emailArr[0]);

    $username = Utils::cleanString( $username );

    if ( username_exists( $username ) ) {
      $i = 2;

      while ( username_exists( $username . $i ) ) {
        $i++;
      }

      $username = $username . $i;
    };

    return $username;
  }


  /**
   * save the value of the optin checkbox
   *
   * @author Ynah Pantig
   * @param $entry, $form
   * @return
   */

  public function gform_user_registered__optinCheckbox( $userID, $feed, $entry, $user_pass )
  {

    $newsletter = $entry[ $this->mailingListFieldID . '.1' ];
    update_field( 'user_newsletter_subscription', $newsletter, 'user_' . $userID );

    $userObj = new \WP_User($userID);
    $userData = array( 'ID' => $userID );
    $formID = $entry[ 'form_id' ];

    foreach ( $entry as $fieldID => $value ) {
      $intFieldID = (int) $fieldID;

      switch ( $intFieldID ) {
        case $this->firstNameFieldID:
        case $this->lastNameFieldID:
          $userData[$this->fieldUserMetaMap[$fieldID]] = $value;
          break;

        case $this->rolesFieldID:
          $field = \GFAPI::get_field( $formID, $intFieldID );
          $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
          $valueArr = explode(', ', $value);

          foreach ($field->choices as $choice) {
            if (in_array($choice['value'], $valueArr)) {
              $userObj->add_role($choice['value']);
            } else {
              $userObj->remove_role($choice['value']);
            }
          }
          break;

        case $this->industryFieldID:
        case $this->interestsFieldID:
        case $this->skillsFieldID:
          $field = \GFAPI::get_field( $formID, $intFieldID );
          $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
          $valueArr = explode(', ', $value);

          update_field($this->fieldUserMetaMap[$intFieldID], $valueArr, 'user_' . $userID);
          break;

        case $this->schoolFieldID:
          update_field($this->fieldUserMetaMap[$intFieldID], $value, 'user_' . $userID);
          break;

        case $this->geolocationFieldID:
          Location::updateUserGeolocationData($userID, json_decode($value, true));
          break;

        default:
          # code...
          break;
      }
    }

    // Update names
    if (!empty($userData[$this->firstNameMetaKey])) {
      $userData[$this->displayNameMetaKey] = $userData[$this->firstNameMetaKey];
      $userData[$this->nicknameMetaKey] = $userData[$this->firstNameMetaKey];

      if (!empty($userData[$this->lastNameMetaKey])) {
        $userData[$this->displayNameMetaKey] .= ' ' . $userData[$this->lastNameMetaKey];
      }
    }

    wp_update_user($userData);

    // set profile completed flag
    update_user_meta($userID, $this->profileCompletedMetaKey, true);

  }

  /**
   * Catch the user registration and trigger forum user creation through the forumintegration
   *
   * @author Ty Eggen
   * @param $userID, $feed, $entry, $user_pass
   * @return
   */
  public function gform_user_registered__forumIntegration( $userID, $feed, $entry, $user_pass )
  {
    $user_info = get_userdata($userID);

    $username = $user_info->user_login;

    // Forum Integration
    ForumIntegration::SignUpRegistration($entry, $username, $user_pass, $user_info);
  }

  function gform_validation( $validation_result ) {
    $userID = get_current_user_id();
    if (!$userID) {
      wp_redirect(site_url('/login/'));
      exit();
    }

    $profile_completed = get_user_meta($userID, $this->profileCompletedMetaKey, true);
    if ($profile_completed) {
      wp_redirect(site_url('/profile/'));
      exit();
    }

    return $validation_result;
  }

  public function gform__populateFields($form)
  {
    foreach ( $form['fields'] as &$field )
    {
      switch ($field->id) {
        case $this->interestsFieldID:
        case $this->industryFieldID:
        case $this->skillsFieldID:
          $field->choices = GravityForms::getTaxonomyFieldChoices($this->fieldTaxonomyMap[$field->id]);
          // $field->inputs = GravityForms::getFieldInputs($field->id, $field->choices);
          break;

        default:
          # code...
          break;
      }
    }

    return $form;

  }/* gform__populateFields() */


  /**
   * validate the location field if they selected from a list
   * or just typed in a city
   *
   * @author Ynah Pantig
   * @param $result, $value, $form, $field
   * @return
   */

  public function gform_field_validation__validateLocationField( $result, $value, $form, $field )
  {

    if ( $value != '' && $result['is_valid'] ) {
      $geolocation = rgpost( 'input_' . $this->geolocationFieldID );
      $result = GravityForms::validateLocationField( $geolocation, $result );
    }

    return $result;

  }



}
