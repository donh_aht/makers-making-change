<?php

  namespace MMC;

  class Bootstrap
  {

    /**
     * Store a 'loaded' flag, in case we are generating this class more than once.
     */
    static private $loaded = false;

    /**
     * @var (string) Plugin path
     */
    static public $path;

    /**
    * @var (string) Plugin version
    */
    static public $version;

    /**
     * Bootstrap App
     */
    public function __construct( $path, $version )
    {

      // check if we already loaded the app before.
      if ( static::$loaded )
        return;

      // set loaded flag to true so we don't load the app twice
      static::$loaded = true;

      // set plugin path and version passed to the constructor
      static::$path = $path;
      static::$version = $version;

      // Instantiate app classes
      new Setup();
      // new Social();
      new Enqueue();
      new Security();
      new Timber();
      new Menu();
      new Query();
      new Options();
      new CustomPostTypes();
      new CustomTaxonomies();
      new GravityForms();
      new ACF();
      new Admin();
      new Login();
      new Ajax();
      new Users();
      new UserImport();
      // new Registration();
      new EditProfile();
      new RequestBuild();
      new SubmitIdea();
      new ConnectUser();
      new CreateEditProject();
      new DataTables();
      new Tracking();
      new Cron();
      new LocationSearch();
      new Projects();
      new SignUpRegistration();
      new Reviews();
      // new SignUp();
      new ForumIntegration();
      new REST\REST();

    } /* __construct() */

  }/* class Bootstrap */
