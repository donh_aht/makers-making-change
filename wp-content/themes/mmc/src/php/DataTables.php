<?php

namespace MMC;

/**
* DataTables Class
*/
class DataTables
{

  private static $dateFormat = 'Y-m-d';

  private static $trackingDir = array();

  /**
   * construct
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function __construct()
  {

    $uploadsDir = wp_upload_dir();

    static::$trackingDir = array(
      'dir' => $uploadsDir[ 'basedir' ] . '/tracking/',
      'url' => $uploadsDir[ 'baseurl' ] . '/tracking/'
    );

    // add_action( 'init', array( &$this, 'init' ) );

  }

  public function init()
  {

    if ( is_admin() ) {
      return;
    }

    $userData = static::getUserData();
    $requestData = static::getRequestData();
    $ideaData = static::getIdeaData();
    $projectData = static::getProjectsData();
    $eventData = static::getEventsData();
    $connectionData = static::getConnectionsData();
    // Utils::pre( $userData );
    // die();

  }

  /**
   * get user data
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function getUserData( $return = 'array' )
  {

    $args = array(
      'number' => -1,
    );

    $fieldTaxonomyMap = array();
    $fieldPrefixMap = array();

    $skillsMetaKey = 'user_skills';
    $industryMetaKey = 'user_industry';
    $interestsMetaKey = 'user_interests';

    // field <--> prefix
    $fieldPrefixMap[$interestsMetaKey] = 'interest';
    $fieldPrefixMap[$industryMetaKey] = 'industry';
    $fieldPrefixMap[$skillsMetaKey] = 'skill';

    // field <--> taxonomy
    $fieldTaxonomyMap[$interestsMetaKey] = Post::getTaxonomy('interest');
    $fieldTaxonomyMap[$industryMetaKey] = Post::getTaxonomy('industry');
    $fieldTaxonomyMap[$skillsMetaKey] = Post::getTaxonomy('skill');

    $users = new \WP_User_Query( $args );
    $results = $users->get_results();

    $customFields = array(
      'user_newsletter_subscription',
      'last_login_date',
      'user_school',
      'user_facebook',
      'user_twitter',
      'user_instagram',
      'user_youtube',
      'user_thingiverse',
      'user_github',
    );

    $data = array();
    foreach ( $results as $user ) {
      $userID = $user->data->ID;
      $userFieldID = 'user_' . $userID;

      $userData = static::getUserBasicData( $user );

      // Meta
      $userData['registered'] = date( static::$dateFormat, strtotime( $user->data->user_registered ) );

      if ( strpos( date( static::$dateFormat, strtotime( $user->data->user_login ) ), '1970' ) > -1 ) {
        $login = '';
      } else {
        $login = date( static::$dateFormat, strtotime( $user->data->user_login ) );
      }

      $userData['login'] = $login;

      // ACF Meta
      foreach ( $customFields as $field ) {
        $userData[ $field ] = get_field( $field, $userFieldID );
      }

      if ( $userData[ 'last_login_date' ] == '' || empty( $userData[ 'last_login_date' ] ) ) {
        $userData[ 'user_activity' ] = 'inactive';
      } else {
        $userData[ 'user_activity' ] = 'active';
      }

      foreach ( $fieldTaxonomyMap as $field => $taxonomy ) {
        $item = get_field( $field, $userFieldID );

        // make sure that the taxonomy is not empty and is not an array
        // before turning it into an array
        if ( !empty( $item ) && !is_array( $item ) ) {
          $item = explode( ', ', $item );
        }

        foreach ( $taxonomy as $slug => $term ) {

          $isInArray = false;

          // need to check if the field is not empty and if it is in the array
          if ( !empty( $item ) && in_array( $term->term_id, $item ) ) {
            $isInArray = true;
          }

          $userData[ $fieldPrefixMap[ $field ] . '_' . $slug ] = $isInArray;

        }
      }

      $data[] = $userData;

    }

    return $data;

  }


  /**
   * get the request a build data
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function getRequestData( $return = 'array' )
  {

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'request',
    );

    $query = new \WP_Query( $args );
    $data = array();

    $metaFields = [
      'fulfilled_date',
      'cancel_date',
    ];

    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $postID = $query->post->ID;

      $item = static::getPostBasicData( $query->post );

      $projectID = get_post_meta( $postID, 'project_id', true );
      $item[ 'project' ] = get_the_title( $projectID );

      $item[ 'date_submitted' ] = get_the_date( static::$dateFormat );
      $item[ 'status' ] = get_field( 'status', $postID );

      foreach ( $metaFields as $field ) {
        $item[ $field ] = get_post_meta( $postID, $field, true );
      }

      $authorID = get_the_author_ID();
      $author = get_userdata( $authorID );

      $user = static::getUserBasicData( $author, 'user' );
      $item = array_merge( $item, $user );

      $item[ 'type' ] = 'request';

      $data[] = $item;

    endwhile; wp_reset_postdata(); endif;

    return $data;

  }


  /**
   * get the request a build data
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function getIdeaData( $return = 'array' )
  {

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'idea',
    );

    $query = new \WP_Query( $args );

    $data = array();

    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $postID = $query->post->ID;

      $item = static::getPostBasicData( $query->post );
      $item[ 'date_submitted' ] = get_the_date( static::$dateFormat );

      $authorID = get_the_author_ID();
      $author = get_userdata( $authorID );

      $user = static::getUserBasicData( $author, 'user' );
      $item = array_merge( $item, $user );

      $data[] = $item;

    endwhile; wp_reset_postdata(); endif;

    return $data;

  }


  /**
   * get connections from gravity forms
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function getConnectionsData()
  {

    $entries = \GFAPI::get_entries( ConnectUser::$formID );
    $data = array();

    foreach ( $entries as $entry ) {

      $item[ 'date_created' ] = $entry[ 'date_created' ];
      $item[ 'type' ] = $entry[ '9' ];
      $item[ 'content' ] = $entry[ '2' ];

      $makerData = get_userdata( (int) $entry[ '7' ] );
      $userData = get_userdata( (int) $entry[ '8' ] );

      $user = static::getUserBasicData( $userData, 'user' );
      $maker = static::getUserBasicData( $makerData, 'maker' );

      $item = array_merge( $item, $user );
      $item = array_merge( $item, $maker );

      $data[] = $item;
    }

    return $data;

  }


  /**
   * get the projects data
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function getProjectsData( $return = 'array' )
  {

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'project',
    );

    $query = new \WP_Query( $args );

    $data = array();

    $statusTax = 'stage';
    $skillTax = 'skill';
    $needTax = 'need';

    $fieldTaxonomyMap[ $statusTax ] = Post::getTaxonomy( $statusTax );
    $fieldTaxonomyMap[ $skillTax ] = Post::getTaxonomy( $skillTax );
    $fieldTaxonomyMap[ $needTax ] = Post::getTaxonomy( $needTax );

    $postTermArgs = array(
      'fields' => 'slugs',
    );

    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $postID = $query->post->ID;

      $item = static::getPostBasicData( $query->post );
      $item[ 'date_submitted' ] = get_the_date( static::$dateFormat );
      $item[ 'creative_commons' ] = get_field( 'creative_commons_license', $postID );

      $postStatus = $query->post->post_status;
      if ( $postStatus == 'publish' ) {
        $item[ 'stage' ] = 'approved';
      } else {
        $item[ 'stage' ] = $postStatus;
      }

      $item[ 'url' ] = get_permalink( $postID );

      $images = get_field( 'images', $postID );
      if ( !empty( $images ) )
      {
        $temp = [];

        foreach ( $images as $val ) {
          $temp[] = $val[ 'url' ];
        }

        $item[ 'images' ] = implode( ', ', $temp );
      } else {
        $item[ 'images' ] = '';
      }

      $downloads = get_field( 'project_downloadables', $postID );
      if ( !empty( $downloads[ 'downloadables' ] ) )
      {
        $temp = [];

        foreach ( $downloads[ 'downloadables' ] as $val ) {
          $temp[] = $val[ 'file' ][ 'url' ];
        }

        $item[ 'downloads' ] = implode( ', ', $temp );
      } else {
        $item[ 'downloads' ] = '';
      }

      $links = get_field( 'project_links', $postID );
      if ( !empty( $links[ 'links' ] ) )
      {
        $temp = [];

        foreach ( $links[ 'links' ] as $val ) {
          $temp[] = $val[ 'link' ][ 'url' ];
        }

        $item[ 'links' ] = implode( ', ', $temp );
      } else {
        $item[ 'links' ] = '';
      }

      $authorID = get_the_author_ID();
      $author = get_userdata( $authorID );

      foreach ( $fieldTaxonomyMap as $taxSlug => $taxonomy ) {
        $postTerms = wp_get_object_terms( $postID, $taxSlug, $postTermArgs );

        foreach ( $taxonomy as $tax ) {
          $hasTax = false;

          if ( !is_wp_error( $postTerms ) && in_array( $tax->slug, $postTerms ) ) {
            $hasTax = true;
          }

          $item[ $taxSlug . '_' . $tax->slug ] = $hasTax;
        }
      }

      $user = static::getUserBasicData( $author, 'user' );

      $item = array_merge( $item, $user );

      $data[] = $item;

    endwhile; wp_reset_postdata(); endif;

    return $data;

  }

  /**
   * get the events data
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function getEventsData( $return = 'array' )
  {

    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'event',
    );

    $query = new \WP_Query( $args );

    $data = array();

    if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      $postID = $query->post->ID;

      $item = static::getPostBasicData( $query->post );
      $item[ 'date_start' ] = get_field( 'date_start', $postID );
      $item[ 'date_end' ] = get_field( 'date_end', $postID );
      $item[ 'location_display' ] = get_field( 'location', $postID );
      $item[ 'location_city' ] = get_field( 'city', $postID );
      $fundersArr = get_field( 'logos', $postID );
      $funders = array();

      if ( !empty( $fundersArr ) ) {
        foreach ( $fundersArr as $funder ) {
          $funders[] = $funder[ 'title' ];
        }
      }

      $item[ 'funders' ] = implode( ', ', $funders );

      $data[] = $item;

    endwhile; wp_reset_postdata(); endif;

    return $data;

  }

  /**
   * get user data
   *
   * @author Ynah Pantig
   * @param $userID
   * @return
   */

  static private function getUserBasicData( $user, $prefix = '' )
  {

    $userID = $user->ID;
    $userFieldID = 'user_' . $userID;

    if ( $prefix != '' ) {
      $prefix .= '_';
    }

    // Roles
    $userRoles = Roles::getUserMMCRoles( $userID );
    $mmcRoles = Roles::getMMCRoles();

    $userData = array();
    $userData[ $prefix . 'first_name'] = get_user_meta ($userID, 'first_name', true );
    $userData[ $prefix . 'last_name'] = get_user_meta( $userID, 'last_name', true );
    $userData[ $prefix . 'email'] = $user->data->user_email;

    $userData[ $prefix . 'city'] = get_user_meta( $userID, 'geolocation_city', true );
    $userData[ $prefix . 'state'] = get_user_meta( $userID, 'geolocation_state', true );
    $userData[ $prefix . 'country'] = get_user_meta( $userID, 'geolocation_country', true );

    $geolocation = get_user_meta( $userID, 'geolocation', true );
    $postalCode = '';
    if ( !empty( $geolocation ) && !empty( $geolocation[ 'address_components' ]) ) {
      foreach ( $geolocation['address_components'] as $component ) {
        if ( in_array( 'postal_code', $component['types'] ) ) {
          $postalCode = $component['long_name'];
        }
      }
    }

    $userData[ $prefix . 'postal_code' ] = $postalCode;

    $userData[ $prefix . 'lat' ] = get_user_meta( $userID, 'geolocation_lat', true );
    $userData[ $prefix . 'lng' ] = get_user_meta( $userID, 'geolocation_lng', true );

    foreach ( $mmcRoles as $slug => $role ) {
      $userData[ $prefix . 'role_' . $slug ] = in_array( $slug, $userRoles );
    }

    return $userData;

  }

  /**
   * get the basics of a post
   *
   * @author Ynah Pantig
   * @param $postID
   * @return
   */

  static private function getPostBasicData( $post )
  {

    $item[ 'title' ] = get_the_title( $post->ID );
    $item[ 'content' ] = $post->post_content;

    return $item;

  }


  /**
   * build out a csv file based on the data
   *
   * @author Ynah Pantig
   * @param $data
   * @return $data
   */

  static public function buildCSV( $filename, $callback )
  {

    if ( is_callable( $callback ) ) {
      $data = $callback;
    } else {
      if ( is_array( $callback ) ) {
        $data = $callback;
      }
    }

    if ( !file_exists( static::$trackingDir['dir'] ) ) {
      mkdir( static::$trackingDir['dir'], 0777, true );
    }

    $fp = fopen( static::$trackingDir['dir'] . $filename . '.csv', 'w' );

    fputcsv( $fp, array_keys( $data[0] ) );

    foreach ( $data as $item ) {
      fputcsv( $fp, $item );
    }

    fclose( $fp );

  }


}
