<?php

namespace MMC;

/**
 * Social
 */
class CustomTaxonomies
{

  protected $taxonomies = [
    'skill' => [
      'post_type'         => array( 'project' ),
      'singular'          => 'Skill',
      'labels'            => [],
      'public'            => true,
      'show_in_nav_menus' => false,
      'show_admin_column' => true,
      'meta_box_cb'       => false,
      'hierarchical'      => false,
      'show_tagcloud'     => false,
      'show_ui'           => true,
      'query_var'         => true,
      'rewrite'           => false,
      'capabilities'      => array(),
    ],
    'need' => [
      'post_type'         => array( 'project' ),
      'singular'          => 'Need',
      'labels'            => [],
      'public'            => true,
      'show_in_nav_menus' => false,
      'show_admin_column' => true,
      'meta_box_cb'       => false,
      'hierarchical'      => false,
      'show_tagcloud'     => false,
      'show_ui'           => true,
      'show_in_menu'      => true,
      'query_var'         => true,
      'rewrite'           => false,
      'capabilities'      => array(),
    ],
    'industry' => [
      'post_type'         => 'user',
      'singular'          => 'Industry',
      'plural'            => 'Industries',
      'labels'            => [],
      'public'            => true,
      'show_in_nav_menus' => false,
      'show_admin_column' => false,
      'hierarchical'      => true,
      'show_tagcloud'     => false,
      'show_ui'           => true,
      'show_in_menu'      => true,
      'query_var'         => true,
      'rewrite'           => false,
      'capabilities'      => array(),
    ],
    'interest' => [
      'post_type'         => 'user',
      'singular'          => 'Interest',
      'plural'            => 'Interests',
      'labels'            => [],
      'public'            => true,
      'show_in_nav_menus' => false,
      'show_admin_column' => false,
      'hierarchical'      => true,
      'show_tagcloud'     => false,
      'show_ui'           => true,
      'show_in_menu'      => true,
      'query_var'         => true,
      'rewrite'           => false,
      'capabilities'      => array(),
    ],
    'stage' => [
      'post_type'         => array( 'project' ),
      'singular'          => 'Stage',
      'labels'            => [],
      'public'            => true,
      'show_in_nav_menus' => false,
      'meta_box_cb'       => false,
      'show_admin_column' => true,
      'hierarchical'      => true,
      'show_tagcloud'     => false,
      'show_ui'           => true,
      'show_in_menu'      => true,
      'query_var'         => true,
      'rewrite'           => false,
      'capabilities'      => array(),
    ],
  ];

  private $userTaxonomies = [];

  function __construct(  )
  {
    foreach ($this->taxonomies as $taxonomyName => $taxArgs) {
      if ($taxArgs['post_type'] == 'user') {
        $this->userTaxonomies[] = $taxonomyName;
      }
    }

    // register accommodation post type
    add_action( 'init', array( &$this, 'init__registerTaxonomies' ), 1 );

    // add Admin UI for user taxonomies
    // https://wordpress.stackexchange.com/questions/218523/custom-taxonomy-on-users-with-ui
    add_action( 'admin_menu', array( &$this, 'add_user_taxonomy_admin_page' ) );
    add_action( 'parent_file', array( &$this, 'filter_user_taxonomy_admin_page_parent_file' ) );

  }/* __construct() */


  /**
   * register accommodation post type
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function init__registerTaxonomies()
  {

    foreach ( $this->taxonomies as $slug => $args ) {
      $this->dynamicRegisterTaxonomy( $slug, $args );
    }

  }/* init__registerTaxonomies() */


  /**
   * dynamically create the post types based on the array
   *
   * @author Ynah Pantig
   * @package CustomTaxonomies.php
   * @since 1.0
   * @param $slug, $args
   * @return $data
   */

  public function dynamicRegisterTaxonomy( $slug, $args )
  {

    $postName = str_replace( '_', ' ', $slug );

    if ( !empty( $args[ 'plural' ] ) )
    {
      $plural = $args[ 'plural' ];
    }
    else
    {
      $plural = ucwords( $postName ) . 's';
    }

    if ( !empty( $args[ 'singular' ] ) )
    {
      $singular = $args[ 'singular' ];
    }
    else
    {
      $singular = ucfirst( $slug );
    }

    $labels = array(
      'name'                  => __( $plural, 'Taxonomy plural name', 'base-theme' ),
      'singular_name'         => __( $singular, 'Taxonomy singular name', 'base-theme' ),
      'search_items'          => __( 'Search ' . $plural, 'base-theme' ),
      'popular_items'         => __( 'Popular ' . $plural, 'base-theme' ),
      'all_items'             => __( 'All ' . $plural, 'base-theme' ),
      'parent_item'           => __( 'Parent ' . $singular . ' Name', 'base-theme' ),
      'parent_item_colon'     => __( 'Parent ' . $singular . ' Name', 'base-theme' ),
      'edit_item'             => __( 'Edit ' . $singular . ' Name', 'base-theme' ),
      'update_item'           => __( 'Update ' . $singular . ' Name', 'base-theme' ),
      'add_new_item'          => __( 'Add New ' . $singular . ' Name', 'base-theme' ),
      'new_item_name'         => __( 'New ' . $singular . ' Name Name', 'base-theme' ),
      'add_or_remove_items'   => __( 'Add or remove ' . $plural, 'base-theme' ),
      'choose_from_most_used' => __( 'Choose from most used base-theme', 'base-theme' ),
      'menu_name'             => __( $plural, 'base-theme' ),
    );

    if ( !empty( $args[ 'labels' ] ) )
    {
      $labels = array_merge( $labels, $args[ 'labels' ] );
    } else
    {
      $args[ 'labels' ] = $labels;
    }

    if ($args['post_type'] == 'user') {
      $args['update_count_callback'] = array( &$this, 'user_taxonomy_update_count_callback' );
    }

    register_taxonomy( $slug, $args[ 'post_type' ], $args );

  }/* dynamicRegisterTaxonomy() */


  function add_user_taxonomy_admin_page() {
    foreach ($this->userTaxonomies as $taxonomy) {
      $tax = get_taxonomy( $taxonomy );

      if (!is_object($tax) OR is_wp_error($tax)) {
        return;
      }

      add_users_page(
        esc_attr( $tax->labels->menu_name ),
        esc_attr( $tax->labels->menu_name ),
        $tax->cap->manage_terms,
        'edit-tags.php?taxonomy=' . $tax->name
      );
    }
  }

  function filter_user_taxonomy_admin_page_parent_file( $parent_file = '' ) {
      global $pagenow;

      /**
       * Only filter the parent if we are on a the taxonomy screen for
       */
      if ( ! empty( $_GET['taxonomy'] ) && (in_array($_GET['taxonomy'], $this->userTaxonomies)) && $pagenow == 'edit-tags.php' ) {
          $parent_file = 'users.php';
      }

      return $parent_file;
  }

  function user_taxonomy_update_count_callback( $terms, $taxonomy ) {
      global $wpdb;

      foreach ( (array) $terms as $term ) {
          $count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->term_relationships WHERE term_taxonomy_id = %d", $term ) );

          do_action( 'edit_term_taxonomy', $term, $taxonomy );
          $wpdb->update( $wpdb->term_taxonomy, compact( 'count' ), array( 'term_taxonomy_id' => $term ) );
          do_action( 'edited_term_taxonomy', $term, $taxonomy );
      }
  }

} /* class CustomTaxonomies() */
