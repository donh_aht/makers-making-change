<?php

  namespace MMC;

  class Setup
  {

    /**
     * Set up actions and filters
     */
    public function __construct()
    {

      // Scripts and styles
      add_action( 'after_setup_theme', array( &$this, 'after_setup_theme' ) );

      // Edit the default admin bar
      add_action( 'wp_before_admin_bar_render', array( &$this, 'wp_before_admin_bar_render' ), 999 );

      // hide admin bar on front end
      add_filter( 'show_admin_bar', '__return_false' );

      // add custom classes to nav items
      add_filter( 'wp_get_nav_menu_items', array( $this, 'wp_get_nav_menu_items__archiveMenuFilter'), 10, 3 );

      // add ACF options pages
      add_action( 'init', array( $this, 'init__addOptionsPages' ) );

      add_action( 'init', array( &$this, 'init__addImageSizes' ) );

      // set excerpt length
      add_filter( 'excerpt_more', array( $this, 'excerpt_more' ) );
      add_filter( 'excerpt_length', array( $this, 'excerpt_length' ) );

      // permalink for future/scheduled posts
      add_filter( 'post_link', array( &$this, 'post_link__modifyFuturePostPermalink' ), 10, 3 );
      add_filter( 'post_type_link', array( &$this, 'post_link__modifyFuturePostPermalink' ), 10, 3 );

      add_action( 'widgets_init', array( &$this, 'widgets_init__initializeWidgets' ) );

      add_filter( 'upload_mimes', array( &$this, 'upload_mimes__addCustomMimes' ) );

      // disable gutenberg
      add_filter( 'use_block_editor_for_post', '__return_false' );
      add_filter( 'use_block_editor_for_post_type', '__return_false' );

    }/* __construct() */


    /**
     * modify the future posts permalinks from
     * the default to pretty permalinks
     *
     * note: still uses the default permalink for future posts
     * even if the settings is set to use the pretty permalinks
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $permalink, $post, $leavename
     * @return
     */

    public function post_link__modifyFuturePostPermalink( $permalink, $post, $leavename )
    {

      /* for filter recursion (infinite loop) */
      static $recursing = false;

      if ( empty( $post->ID ) )
      {
        return $permalink;
      }

      if ( !$recursing )
      {
        if ( isset( $post->post_status ) && ( 'future' === $post->post_status ) )
        {
          // set the post status to publish to get the 'publish' permalink
          $post->post_status = 'publish';
          $recursing = true;
          $permalink = get_permalink( $post, $leavename ) ;
        }
      } else
      {
        $recursing = false;
      }

      return $permalink;

    }/* post_link__modifyFuturePostPermalink() */

    /**
     * add images sizes
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    public function init__addImageSizes(  )
    {

      add_image_size( 'fund_logo', '115', '115', false );
      add_image_size( 'card', '500', '350', true );
      add_image_size( 'gallery_thumb', '330', '230', true );
      add_image_size( 'gallery_large', '1005', '580', true );
      add_image_size( 'feature_half', '1000', '570', true );
      add_image_size( 'square_200', '200', '200', true );
      add_image_size( 'social', '500', '500', true );

    }/* init__addImageSizes() */


    public function wp_get_nav_menu_items__archiveMenuFilter( $items, $menu, $args )
    {
      foreach ( $items as $item )
      {
        if ( $item->type != 'post_type_archive' )
          continue;

        if ( get_query_var('post_type') == $item->object )
          $item->classes[] = 'current-menu-item';
      }
      return $items;
    }

    /**
     * Setup theme
     */

    public function after_setup_theme()
    {
      // register editor styles
      if ( file_exists( get_template_directory_uri() . '/src/styles/editor-style.css' ) )
      {
        add_editor_style( get_template_directory_uri() . '/src/styles/editor-style.css' );
      }

      // add theme supports
      add_theme_support('post-thumbnails');
      add_theme_support('menus');

    }/* after_setup_theme() */


    /**
     * Edit the default admin bar
     */
    public function wp_before_admin_bar_render()
    {
      global $wp_admin_bar;
      $wp_admin_bar->remove_node( 'wp-logo' );

    } /* wp_before_admin_bar_render() */


    public function init__addOptionsPages()
    {

      if( function_exists('acf_add_options_page') )
      {

        acf_add_options_page(array(
          'page_title'  => __( 'Site Options', 'mmc' ),
          'menu_title'  => __( 'Site Options', 'mmc' ),
          'menu_slug'   => 'site-options',
          'capability'  => 'edit_posts',
          'redirect'    => true
        ));

      }

      if ( function_exists( 'acf_add_options_sub_page' ) )
      {

        acf_add_options_sub_page(array(
          'page_title'  => __( 'Site Options', 'mmc' ),
          'menu_title'  => __( 'Site Options', 'mmc' ),
          'parent_slug' => 'site-options',
        ));

        acf_add_options_sub_page(array(
          'page_title'  => __( 'User Profile Options', 'mmc' ),
          'menu_title'  => __( 'User Profile Options', 'mmc' ),
          'parent_slug' => 'site-options',
        ));

        acf_add_options_sub_page(array(
          'page_title'  => __( 'Call to Actions', 'mmc' ),
          'menu_title'  => __( 'Call to Actions', 'mmc' ),
          'parent_slug' => 'site-options',
        ));

        // post
        acf_add_options_sub_page(array(
          'title'      => __( 'Post Archive Settings', 'mmc' ),
          'parent_slug'=> 'edit.php',
          'capability' => 'manage_options',
          'menu_slug'  => 'blog_settings',
        ));

        // event
        acf_add_options_sub_page(array(
          'title'      => __( 'Event Archive Settings', 'mmc' ),
          'parent_slug'=> 'edit.php?post_type=event',
          'capability' => 'manage_options',
          'menu_slug'  => 'event_settings',
        ));

        // project
        acf_add_options_sub_page(array(
          'title'      => __( 'Project Archive Settings', 'mmc' ),
          'parent_slug'=> 'edit.php?post_type=project',
          'capability' => 'manage_options',
          'menu_slug'  => 'project_settings',
        ));

        acf_add_options_sub_page(array(
          'title'      => __( 'Project Message Settings', 'mmc' ),
          'parent_slug'=> 'edit.php?post_type=project',
          'capability' => 'manage_options',
          'menu_slug'  => 'project_message_settings',
        ));

        // success story
        acf_add_options_sub_page(array(
          'title'      => __( 'Success Story Archive Settings', 'mmc' ),
          'parent_slug'=> 'edit.php?post_type=success-story',
          'capability' => 'manage_options',
          'menu_slug'  => 'successtory_settings',
        ));

      }

    } /* init__addOptionsPages() */


    public function excerpt_more($more)
    {
      return '';

    } /* excerpt_length() */

    public function excerpt_length($length)
    {
      return 40;

    } /* excerpt_length() */

    public function widgets_init__initializeWidgets()
    {
      register_sidebar( array(
        'name'          => 'Login Form',
        'id'            => 'login_form',
        'before_widget' => '<div class="login-form-widget">',
        'after_widget'  => '</div>',
      ) );

    } /* widgets_init__initializeWidgets() */


    /**
     * add custom mimes
     *
     * @author Ynah Pantig
     * @param $mimes
     * @return
     */

    public function upload_mimes__addCustomMimes( $mimes )
    {

      $newMimes = array(
        'dwg' => 'application/dwg',
        'zip' => 'application/zip',
        'stl' => 'application/stl',
        'iges' => 'application/iges',
        'dxf' => 'application/dxf',
        'sldprt' => 'application/sldprt',
      );

      $mimes = array_merge( $mimes, $newMimes );

      return $mimes;

    }/* upload_mimes__addCustomMimes() */


  }/* class Setup */


