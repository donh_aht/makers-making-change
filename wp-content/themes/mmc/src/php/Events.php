<?php

namespace MMC;

class Events {

  protected $postType = 'event';

  /**
   * get custom query for the archive
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function customQuery()
  {

    global $wp_query;
    $args = $wp_query->query;
    $args[ 'posts_per_page' ] = 12;

    $pageTitle = '';
    $today = date( 'Ymd' );

    $upcomingMetaQuery = array(
      array(
        'key' => 'date_start',
        'value' => $today,
        'compare' => '>=',
        'type' => 'DATE',
      )
    );

    $pastMetaQuery = array(
      array(
        'key' => 'date_start',
        'value' => $today,
        'compare' => '<',
        'type' => 'DATE',
      ),
      array(
        'key' => 'date_end',
        'value' => $today,
        'compare' => '<',
        'type' => 'DATE',
      )
    );

    if ( isset( $_REQUEST[ 'filter' ] ) && !empty( $_REQUEST[ 'filter' ] ) ) {

      $filter = $_REQUEST[ 'filter' ];

      if ( $filter == 'upcoming' )
      {
        $args[ 'orderby' ] = array( 'meta_value_num' => 'ASC', 'date' => 'ASC' );
        $args[ 'post_status' ] = array( 'publish', 'future' );
        $args[ 'meta_query' ] = $upcomingMetaQuery;

        $pageTitle = [
          'singular' => __( 'Upcoming Event' ),
          'plural' => __( 'Upcoming Events' ),
        ];

      } else
      {

        $args[ 'orderby' ] = array( 'meta_value' => 'ASC', 'date' => 'ASC' );
        $args[ 'meta_query' ] = array(
          array(
            'key' => 'city',
            'compare' => 'EXISTS'
          ),
        );

        $pageTitle = [
          'singular' => __( 'Event' ),
          'plural' => __( 'Events' ),
        ];

      }
    } else {

      $feature = get_field( 'event_feature_post', 'options' );
      $postsPerPage = Query::getPostsPerPage( 'event' );
      $upcomingArgs = array(
        'posts_per_page' => -1,
        'post_type' => 'event',
        'orderby' => array( 'meta_value_num' => 'ASC' ),
        'meta_query' => $upcomingMetaQuery,
        'post__not_in' => array( $feature ),
      );

      $pastArgs = array(
        'posts_per_page' => -1,
        'post_type' => 'event',
        'orderby' => array( 'meta_value_num' => 'DESC' ),
        'meta_query' => $pastMetaQuery,
        'post__not_in' => array( $feature ),
      );

      $upcoming = \Timber::get_posts( $upcomingArgs );
      $past = \Timber::get_posts( $pastArgs );

      $archive = array_merge( $upcoming, $past );

      $queryPosts = array();
      foreach ( $archive as $key => $value ) {
        $queryPosts[] = $value->ID;
      }

      $args[ 'orderby' ] = 'post__in';
      $args[ 'post__in' ] = $queryPosts;

    }

    $data = [
      'page_title' => $pageTitle,
      'query_args' => $args,
    ];

    return $data;

  }

  static public function getDateToday() {

    date_default_timezone_set( 'America/Vancouver' );
    $date = date( 'Y-m-d' );
    return $date;

  }
}
