<?php

  namespace MMC;

  class Security
  {
    /**
     * Set up actions and filters
     */

    public function __construct()
    {

      add_filter( 'xmlrpc_methods', array( $this, 'xmlrpc_methods__removePingBack' ) );

    }

    /**
     * Remove the pingback.ping method from XMLRPC
     * Source: http://blog.sucuri.net/2014/03/more-than-162000-wordpress-sites-used-for-distributed-denial-of-service-attack.html
     */

    public function xmlrpc_methods__removePingBack( $methods )
    {
      if ( isset( $methods['pingback.ping'] ) )
        unset( $methods['pingback.ping'] );

      return $methods;

    }/* xmlrpc_methods__removePingBack() */

  }/* class Security() */
