<?php

namespace MMC;

use GFAPI;
use GFCommon;
use WP_Error;

/**
 * HookHub interface class.
 *
 * @author Ty Eggen <tycho.eggen@gmail.com>
 */
class ForumIntegration
{
    public function __construct()
    {
        add_filter('wp_get_attachment_url', [&$this, 'httpsAttachmentURLWorkaround'], 10, 2);
    }

    /**
     * ConnectUser integration.
     *
     * @param [int]    $senderID
     * @param [int]    $recipientID
     * @param [string] $subject
     * @param [text]   $message
     * @param mixed    $entry
     */
    public static function ConnectUser($entry, $senderID, $recipientID, $subject, $message)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        // Forum Integration
        $hookhub_data = $entry;

        $senderUser = get_userdata($senderID);
        $hookhub_data['sender_username'] = $senderUser->user_login;
        $hookhub_data['sender_email'] = $senderUser->user_email;
        $hookhub_data['sender'] = [
            'username' => $senderUser->user_login,
            'user_email' => $senderUser->user_email,
        ];

        $recipientUser = get_userdata($recipientID);
        $hookhub_data['recipient_username'] = $recipientUser->user_login;
        $hookhub_data['recipient_email'] = $recipientUser->user_email;
        $hookhub_data['recipient'] = [
            'username' => $recipientUser->user_login,
            'user_email' => $recipientUser->user_email,
        ];

        $hookhub_data['message_subject'] = $subject;
        $hookhub_data['message_body'] = $message;

        HookHub::request($hookhub_data);
    }

    /**
     * CreateEditProject integration.
     *
     * @param [int]    $postID
     * @param [object] $postObj
     */
    public static function CreateEditProject($postID, $postObj = null)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        if (null === $postID && null === $postObj) {
            return;
        }
        if (null === $postID) {
            $postID = $postObj->ID;
        } elseif (null === $postObj) {
            $postObj = get_post($postID);
        }

        if ('publish' !== $postObj->post_status) {
            return ['result'=>'ERROR','message'=>'project-not-published'];
        }

        $postMeta = get_post_meta($postID);

        if (isset($postMeta['_gform-form-id']) && count($postMeta['_gform-form-id']) && 2 !== intval($postMeta['_gform-form-id'][0])) {
            return;
        }

        $entry = ['form_id' => self::getFormIDByName('Create/Edit a Project')];

        // Get Gravity Forms entry
        if (isset($postMeta['_gform-entry-id'])) {
            $entry_id = intval($postMeta['_gform-entry-id'][0]);
            $entry = GFAPI::get_entry($entry_id);
        }

        $acf_fields = get_fields($postID);

        // Load hookhub data from entry
        $hookhub_data = $entry;

        // User info
        $user_info = get_userdata($postObj->post_author);

        $hookhub_data['username'] = $user_info->user_login;
        $hookhub_data['user_email'] = $user_info->user_email;
        $hookhub_data['created_by'] = $user_info->display_name;

        // Post info
        $hookhub_data['post_url'] = get_permalink($postID);
        $hookhub_data['post_title'] = $postObj->post_title;
        $hookhub_data['post_image'] = get_the_post_thumbnail_url($postID);
        $hookhub_data['post_content'] = $postObj->post_content;

        // Discourse topic and post IDs
        $discourse_topic_id = $postObj->discourse_topic_id;
        $hookhub_data['discourse_topic_id'] = (is_string($discourse_topic_id) && is_numeric($discourse_topic_id)) ? $discourse_topic_id : 0;
        $discourse_post_id = $postObj->discourse_post_id;
        $hookhub_data['discourse_post_id'] = (is_string($discourse_post_id) && is_numeric($discourse_post_id)) ? $discourse_post_id : 0;

        // Stage info
        $post_stage = wp_get_post_terms($postID, 'stage');
        $hookhub_data['stage'] = $post_stage[0]->name;

        // Needs
        $hookhub_data['needs'] = [];
        $needs_result = wp_get_post_terms($postID, 'need');
        foreach ($needs_result as $need) {
            $hookhub_data['needs'][] = $need->name;
        }

        // Skills
        $hookhub_data['skills'] = [];
        if (isset($acf_fields['skills'])) {
            foreach ($acf_fields['skills'] as $skill) {
                $hookhub_data['skills'][] = $skill->name;
            }
        }

        // Images
        $hookhub_data['images'] = [];
        if (isset($postMeta['images'])) {
            if ('' !== $postMeta['images'][0]) {
                $images_set = $acf_fields['images'];
                if (isset($acf_fields['images'])) {
                    foreach ($images_set as $image_entry) {
                        $hookhub_data['images'][] = [
                            'title' => $image_entry['title'],
                            'url' => $image_entry['url'],
                        ];
                    }
                }
            }
        }

        // Downloads
        $hookhub_data['downloads'] = [];
        if (is_array($acf_fields['project_downloadables']['downloadables']) && count($acf_fields['project_downloadables']['downloadables'])) {
            foreach ($acf_fields['project_downloadables']['downloadables'] as $download_entry) {
                $hookhub_data['downloads'][] = [
                    'title' => $download_entry['file']['title'],
                    'url' => $download_entry['file']['url'],
                ];
            }
        }

        // Links
        $hookhub_data['links'] = [];
        if (is_array($acf_fields['project_links']['links']) && count($acf_fields['project_links']['links'])) {
            foreach ($acf_fields['project_links']['links'] as $project_link_entry) {
                $hookhub_data['links'][] = $project_link_entry['link'];
            }
        }

        // Other details
        $hookhub_data['time_to_complete'] = $postObj->time_to_complete;
        $hookhub_data['cost_to_build'] = $postObj->cost_to_build;
        $hookhub_data['license'] = $postObj->creative_commons_license;

        $hookhub_result = HookHub::request($hookhub_data);

        if (is_string($hookhub_result['result'])) {
            $hookhub_result['result'] = json_decode($hookhub_result['result'], true);
        }

        if ('OK' == $hookhub_result['result']['result'] && isset($hookhub_result['result']['data'])) {
            $topic_id = (isset($hookhub_result['result']['data']['topic_id'])) ? $hookhub_result['result']['data']['topic_id'] : 0;
            $post_id = (isset($hookhub_result['result']['data']['post_id'])) ? $hookhub_result['result']['data']['post_id'] : 0;
            $topic_slug = (isset($hookhub_result['result']['data']['topic_slug'])) ? $hookhub_result['result']['data']['topic_slug'] : '';

            if (0 !== $topic_id && 0 !== $post_id && !empty($topic_slug)) {
                $forum_link = sprintf('%s/t/%s/%d', FORUM_URL, $topic_slug, $topic_id);

                $metaresults = [];
                $metaresults[] = update_post_meta($postID, 'discourse_topic_id', $topic_id);
                $metaresults[] = update_post_meta($postID, 'discourse_post_id', $post_id);
                $metaresults[] = update_post_meta($postID, 'forum_link', $forum_link);

                return ['result' => 'OK', 'CreateEditProjectResult' => $hookhub_result, 'discourse_topic_id' => $topic_id, 'discourse_post_id' => $post_id, 'forum_link' => $forum_link];
            }
        } else {
            return ['result' => $hookhub_result['result']['result'], 'message' => $hookhub_result['result']['message'], 'CreateEditProjectResult' => $hookhub_result];
        }
    }

    /**
     * EditProfile integration.
     *
     * @param [array]  $entry
     * @param [object] $currentUser
     * @param mixed    $current_user
     */
    public static function EditProfile($current_user, $entry = [])
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        $userID = $current_user->ID;

        $hookhub_data = $entry;

        if (!isset($hookhub_data['form_id'])) {
            $hookhub_data['form_id'] = self::getFormIDByName('Edit Profile');
        }

        $hookhub_data['username'] = $current_user->user_login;
        $hookhub_data['user_email'] = $current_user->user_email;
        $hookhub_data['user_avatar'] = get_avatar_url($userID);

        $userMeta = get_user_meta($userID);

        $hookhub_data['user_fname'] = $current_user->first_name;
        $hookhub_data['user_lname'] = $current_user->last_name;

        $userFields = (array) get_fields('user_'.$userID);
        $hookhub_data = $hookhub_data + $userFields;

        $dict = self::getDict(['role', 'skill', 'interest']);

        foreach ($hookhub_data['user_skills'] as $user_skill) {
            if (isset($dict[$user_skill])) {
                $hookhub_data[$dict[$user_skill]] = true;
            }
        }

        return HookHub::request($hookhub_data);
    }

    /**
     * RequestBuild integration.
     *
     * @param [int]    $postID
     * @param [object] $postObj
     */
    public static function RequestBuild($postID, $postObj = null)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        if (null === $postID && null === $postObj) {
            return;
        }
        if (null === $postID) {
            $postID = $postObj->ID;
        } elseif (null === $postObj) {
            $postObj = get_post($postID);
        }

        if ('publish' !== $postObj->post_status) {
            return;
        }

        $postMeta = get_post_meta($postID);

        $form_id = intval($postMeta['_gform-form-id'][0]);

        if (1 !== $form_id) {
            return;
        }

        // Get Gravity Forms entry
        $entry_id = intval($postMeta['_gform-entry-id'][0]);
        $entry = GFAPI::get_entry($entry_id);

        // Load hookhub data from entry
        $hookhub_data = [];
        $hookhub_data['form_id'] = $form_id;

        // User info
        $user_info = get_userdata($postObj->post_author);

        // Requestor information
        $hookhub_data['username'] = $user_info->user_login;
        $hookhub_data['user_email'] = $user_info->user_email;
        $hookhub_data['name'] = $user_info->display_name;

        // Post info
        $hookhub_data['post_title'] = html_entity_decode($postObj->post_title);
        $hookhub_data['post_content'] = $postObj->post_content;

        $project_id = $postMeta['project_id'][0];

        $projectMeta = get_post_meta($project_id);

        $forum_link = $projectMeta['forum_link'][0];

        $hookhub_data['project_url'] = $forum_link;

        // Discourse topic and post IDs
        $discourse_topic_id = $postObj->discourse_topic_id;
        $hookhub_data['discourse_topic_id'] = (is_string($discourse_topic_id) && is_numeric($discourse_topic_id)) ? $discourse_topic_id : 0;
        $discourse_post_id = $postObj->discourse_post_id;
        $hookhub_data['discourse_post_id'] = (is_string($discourse_post_id) && is_numeric($discourse_post_id)) ? $discourse_post_id : 0;

        $hookhub_result = HookHub::request($hookhub_data);

        if (is_string($hookhub_result['result'])) {
            $hookhub_result['result'] = json_decode($hookhub_result['result'], true);
        }

        if ('OK' == $hookhub_result['result']['result'] && isset($hookhub_result['result']['data'])) {
            $topic_id = $hookhub_result['result']['data']['topic_id'];
            $post_id = $hookhub_result['result']['data']['post_id'];
            $topic_slug = $hookhub_result['result']['data']['topic_slug'];
            $forum_link = sprintf('%s/t/%s/%d', FORUM_URL, $topic_slug, $topic_id);

            $metaresults = [];
            $metaresults[] = update_post_meta($postID, 'discourse_topic_id', $topic_id);
            $metaresults[] = update_post_meta($postID, 'discourse_post_id', $post_id);
            $metaresults[] = update_post_meta($postID, 'forum_link', $forum_link);
        }
    }

    /**
     * SubmitIdea integration.
     *
     * @param [int]    $postID
     * @param [object] $postObj
     */
    public static function SubmitIdea($postID, $postObj = null)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));

        if (null === $postID && null === $postObj) {
            return;
        }
        if (null === $postID) {
            $postID = $postObj->ID;
        } elseif (null === $postObj) {
            $postObj = get_post($postID);
        }

        if ('publish' !== $postObj->post_status) {
            return ['result'=>'ERROR','message'=>'idea-not-published'];
        }

        $postMeta = get_post_meta($postID);

        $form_id = intval($postMeta['_gform-form-id'][0]);

        if (7 !== $form_id) {
            return;
        }

        // Get Gravity Forms entry
        $entry_id = intval($postMeta['_gform-entry-id'][0]);
        $entry = GFAPI::get_entry($entry_id);

        // Load hookhub data from entry
        $hookhub_data = [];
        $hookhub_data['form_id'] = $form_id;

        // User info
        $user_info = get_userdata($postObj->post_author);

        // Requestor information
        $hookhub_data['username'] = $user_info->user_login;
        $hookhub_data['user_email'] = $user_info->user_email;
        $hookhub_data['name'] = $user_info->display_name;

        // Post info
        $hookhub_data['post_title'] = $postObj->post_title;
        $hookhub_data['post_content'] = $postObj->post_content;

        // Discourse topic and post IDs
        $discourse_topic_id = $postObj->discourse_topic_id;
        $hookhub_data['discourse_topic_id'] = (is_string($discourse_topic_id) && is_numeric($discourse_topic_id)) ? $discourse_topic_id : 0;
        $discourse_post_id = $postObj->discourse_post_id;
        $hookhub_data['discourse_post_id'] = (is_string($discourse_post_id) && is_numeric($discourse_post_id)) ? $discourse_post_id : 0;

        $hookhub_result = HookHub::request($hookhub_data);

        if (is_string($hookhub_result['result'])) {
            $hookhub_result['result'] = json_decode($hookhub_result['result'], true);
        }

        if ('OK' == $hookhub_result['result']['result'] && isset($hookhub_result['result']['data'])) {
            $topic_id = $hookhub_result['result']['data']['topic_id'];
            $post_id = $hookhub_result['result']['data']['post_id'];
            $topic_slug = $hookhub_result['result']['data']['topic_slug'];
            $forum_link = sprintf('%s/t/%s/%d', FORUM_URL, $topic_slug, $topic_id);

            $metaresults = [];
            $metaresults[] = update_post_meta($postID, 'discourse_topic_id', $topic_id);
            $metaresults[] = update_post_meta($postID, 'discourse_post_id', $post_id);
            $metaresults[] = update_post_meta($postID, 'forum_link', $forum_link);

            return ['result' => 'OK', 'SubmitIdeaResult' => $hookhub_result, 'discourse_topic_id' => $topic_id, 'discourse_post_id' => $post_id, 'forum_link' => $forum_link];
        }

        return ['result' => $hookhub_result['result']['result'], 'message' => $hookhub_result['result']['message'], 'SubmitIdeaResult' => $hookhub_result];
    }

    /**
     * SignUpRegistration integration.
     *
     * @param mixed $entry
     * @param mixed $username
     * @param mixed $user_pass
     * @param mixed $user_info
     */
    public static function SignUpRegistration($entry, $username, $user_pass, $user_info)
    {
        GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Called'));
        GFCommon::log_debug(sprintf('%s[%d]: arguments: %s', __METHOD__, __LINE__, HookHub::safeJSON(['entry' => $entry, 'username' => $username])));

        // Pre-load form entry data
        $hookhub_data = $entry;

        $hookhub_data['user_info'] = $user_info;

        $hookhub_data['username'] = $username;
        $hookhub_data['password'] = $user_pass;

        $hookhub_result = HookHub::request($hookhub_data);

        if (is_string($hookhub_result['result'])) {
            $hookhub_result['result'] = json_decode($hookhub_result['result'], true);
        }

        if ('OK' == $hookhub_result['result']['result'] && isset($hookhub_result['result']['data'])) {
            GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, 'Processing result'));
            GFCommon::log_debug(sprintf('%s[%d]: %s', __METHOD__, __LINE__, json_encode($hookhub_result['result']['data'])));

            $discourse_sso_user_id = (isset($hookhub_result['result']['data']['discourse_sso_user_id'])) ? $hookhub_result['result']['data']['discourse_sso_user_id'] : 0;
            $discourse_username = (isset($hookhub_result['result']['data']['discourse_username'])) ? $hookhub_result['result']['data']['discourse_username'] : '';

            if (0 !== $discourse_sso_user_id && !empty($discourse_username)) {
                $metaresults = [];
                $metaresults[] = update_user_meta($user_info->ID, 'discourse_sso_user_id', $discourse_sso_user_id);
                $metaresults[] = update_user_meta($user_info->ID, 'discourse_username', $discourse_username);
            }

            return ['result' => 'OK', 'discourse_sso_user_id' => $discourse_sso_user_id, 'discourse_username' => $discourse_username, 'raw' => $hookhub_result];
        }

        return ['result' => 'ERROR', 'data' => $hookhub_result];
    }

    /**
     * HTTPS attachment URL workaround.
     *
     * @param mixed $url
     * @param mixed $post_id
     */
    public function httpsAttachmentURLWorkaround($url, $post_id)
    {
        //Skip file attachments
        if (!wp_attachment_is_image($post_id)) {
            return $url;
        }

        //Correct protocol for https connections
        list($protocol, $uri) = explode('://', $url, 2);

        if (is_ssl()) {
            if ('http' == $protocol) {
                $protocol = 'https';
            }
        } else {
            if ('https' == $protocol) {
                $protocol = 'http';
            }
        }

        return $protocol.'://'.$uri;
    }

    /**
     * getDict.
     *
     * @param mixed $taxonomy
     * @param mixed $taxonomies
     *
     * @return array
     */
    public static function getDict($taxonomies)
    {
        $resultSet = [];

        if (!is_array($taxonomies)) {
            $taxonomies = [$taxonomies];
        }

        foreach ($taxonomies as $taxonomy) {
            $taxonomy_query = [
                'taxonomy' => $taxonomy,
                'hide_empty' => false,
            ];
            $taxonomy_terms = get_terms($taxonomy_query);

            if (!is_a($taxonomy_terms, 'WP_Error')) {
                foreach ($taxonomy_terms as $idx => $entry) {
                    $resultSet[$entry->term_id] = $taxonomy.'_'.$entry->slug;
                }
            }
        }

        asort($resultSet);

        return $resultSet;
    }

    public static function getFormIDByName($form_title = '')
    {
        return intval(\RGFormsModel::get_form_id($form_title));
    }

    public static function fixArray($srcArray)
    {
        foreach ($srcArray as $dataKey => $dataVal) {
            if (is_array($srcArray[$dataKey]) && 1 == count($srcArray[$dataKey]) && !is_string(current(array_keys($srcArray[$dataKey])))) {
                $srcArray[$dataKey] = $srcArray[$dataKey][0];
            }
            if (is_string($srcArray[$dataKey]) && is_numeric($srcArray[$dataKey])) {
                $srcArray[$dataKey] = intval($srcArray[$dataKey]);
            }
        }

        return $srcArray;
    }
}
