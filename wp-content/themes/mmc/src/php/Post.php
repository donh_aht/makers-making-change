<?php

  namespace MMC;

  /**
   * Timber extension
   */
  class Post extends \TimberPost
  {

    /**
     * get the events based on the parameters
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $type = 'current'
     * @return
     */

    public function get_events( $type = '' )
    {

      $today = date( 'F j, Y' );

      switch ( $type ) {

        case 'current':

          $args = array(
            'post_status' => 'future',
            'orderby' => array( 'date' => 'ASC' ),
          );

          break;

        case 'past':

          $args = array(
            'posts_per_page' => -1,
            'orderby' => array( 'date' => 'DESC' ),
            'date_query' => array(
              array(
                'before' => $today,
              ),
            )
          );

          break;

        default:

          $args = array(
            'post_status' => array( 'publish', 'future' ),
            'orderby' => array( 'date' => 'DESC' ),
          );

          break;
      }

      $defaultArgs = array(
        'posts_per_page' => 4,
        'post_type' => 'event',
      );

      $args = array_merge( $defaultArgs, $args );

      $data = \Timber::get_posts( $args );

      foreach ( $data as $key => &$value )
      {
        $value->event_type = $this->checkEventType( $value );
      }

      return $data;

    }/* get_events() */


    /**
     * check the event type
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $post
     * @return
     */

    public function checkEventType( $post )
    {

      $today = date( 'F j, Y' );

      if ( strtotime( $post->post_date ) < strtotime( $today ) )
      {
        $eventType = 'past';
      } else
      {
        $eventType = 'future';
      }

      return $eventType;

    }/* checkEventType() */


    /**
     * get brand statements
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    static public function getBrandStatements()
    {

      $data = get_field( 'brand_statements', 'options' );

      return $data;

    }/* getBrandStatements() */


    /**
     * get call to actions
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $type
     * @return
     */

    static public function getCallToActions( $type )
    {

      $data = array(
        'title' => get_field( $type . '_title', 'options' ),
        'link' => get_field( $type . '_link', 'options' ),
        'background_colour' => get_field( $type . '_background_colour', 'options' ),
      );

      return $data;

    }/* getCallToActions() */


    /**
     * get testimonials
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    static public function getTestimonials(  )
    {

      $data = get_field( 'testimonials', 'options' );

      return $data;

    }/* getTestimonials() */


    /**
     * get connect post data
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $postType
     * @return
     */

    public function getConnectData( $post )
    {
      $postID = intval($post['id']);
      $content = '';
      $meta = '';
      $image = '';
      $btnClass = '';
      $title = '';
      $latitude = '';
      $longitude = '';

      $type = isset( $post[ 'type' ] ) ? $post[ 'type' ] : $post[ 'post_type' ];

      switch ( $type ) {
        case 'maker':
          $user = get_user_by('id', $postID);
          $postLabel = __( 'Maker: Ask me for help' );
          $icon = 'icon-marker-maker';
          $colour = 'red';
          $title = $user->display_name;
          $ariaLabel = __( 'Connect with ' . $user->display_name );
          $pin = get_stylesheet_directory_uri() . '/public/images/map/maker.svg';
          $btnClass = 'js-user-connect js-form-trigger-button';

          $skills = get_field( 'user_skills', 'user_' . $postID );
          $metaArr = array();

          if ( !empty( $skills ) )
          {
            foreach ( $skills as $skill ) {
              $term = get_term_by( 'id', $skill, 'skill' );
              $metaArr[] = $term->name;
            }

            $meta = implode( ', ', $metaArr );
          }

          $image = get_avatar_url( $postID, array(
            'size' => 180,
          ) );

          $btnLabel = __( 'Contact' );
          $connectPage = get_field( 'connect_with_user_page', 'options' );
          $link = array(
            'url' => $connectPage . '?user_id=' . $postID . '&type=connect',
            'attr' => 'data-connect_user="' . $postID . '" data-type="connect"',
          );

          $latitude = get_user_meta( $postID, 'geolocation_lat', true );
          $longitude = get_user_meta( $postID, 'geolocation_lng', true );

          break;

        case 'request':
          $postData = get_post($postID);
          $authorID = $postData->post_author;
          $author = get_user_by( 'id', $authorID );

          $postLabel = __( 'Request: Help me build this' );
          $icon = 'icon-marker-request';
          $btnLabel = __( 'Contact' );
          $btnClass = 'js-form-trigger-button';
          $colour = 'teal';
          $meta = $author->display_name;
          $title = $postData->post_title;
          $ariaLabel = __( 'Contact about ' . $postData->post_title );
          $pin = get_stylesheet_directory_uri() . '/public/images/map/request.svg';

          $connectPage = get_field( 'connect_with_user_page', 'options' );
          $image = get_avatar_url( $authorID, array(
            'size' => 180,
          ) );

          $latitude = get_user_meta( $authorID, 'geolocation_lat', true );
          $longitude = get_user_meta( $authorID, 'geolocation_lng', true );

          $link = array(
            'url' => $connectPage . '?user_id=' . $authorID . '&type=request&request_id=' . $postID,
            'attr' => 'data-type="request"',
          );
          break;

        case 'event':
          $postData = get_post($postID);

          $postLabel = __( 'Event' );
          $icon = 'icon-marker-event';
          $content = $postData->post_excerpt;
          $btnLabel = __( 'Read More' );
          $colour = 'darker-grey';
          $title = $postData->post_title;
          $ariaLabel = __( 'Read more about' ) . ' ' . self::eventAriaLabel( $post );
          $start = get_field( 'date_start', $postID );
          $end = get_field( 'date_end', $postID );
          $meta = Utils::get_date_range( $start, $end );
          $link = get_field( 'link_url', $postID );
          $pin = get_stylesheet_directory_uri() . '/public/images/map/event.svg';

          $geolocation = get_field( 'geolocation', $postID );
          $latitude = $geolocation['lat'];
          $longitude = $geolocation['lng'];

          if ( empty( $link[ 'url' ] ) )
          {
            $link = array(
              'url' => get_permalink( $postID ),
              'target' => '_self',
            );
          }

          $location = get_field( 'location', $postID );
          if ( !empty( $location ) )
          {
            if ( !empty( $meta ) )
            {
              $meta .= ' | ';
            }

            $meta .= $location;
          }
          break;

        case 'test':
          $postLabel = 'Maker';
          $icon = 'icon-marker-maker';
          $colour = 'primary';
          $title = 'Test Maker ' . $postID;
          $ariaLabel = __( 'Connect with ' . $title );
          $pin = get_stylesheet_directory_uri() . '/public/images/map/maker.svg';
          $btnClass = 'js-user-connect';

          $meta = '';

          $btnLabel = __( 'Connect' );
          $connectPage = get_field( 'connect_with_user_page', 'options' );
          $link = array(
            'url' => $connectPage . '?user_id=' . $postID . '&type=connect',
            'attr' => 'data-connect_user="' . $postID . '" data-type="connect"',
          );

          $latitude = $post['lat'];
          $longitude = $post['lng'];

          break;

        default:

          break;
      }

      $data = array(
        'title' => $title,
        'label' => $postLabel,
        'icon' => $icon,
        'image' => $image,
        'content' => $content,
        'colour' => $colour,
        'meta' => $meta,
        'pin' => $pin,
        'aria_label' => $ariaLabel,
        'latitude' => $latitude,
        'longitude' => $longitude,
        'button' => array(
          'label' => $btnLabel,
          'class' => $btnClass,
          'link' => $link,
        ),
      );

      return $data;

    }/* getConnectData() */

    /**
     * get event aria label
     *
     * @author Ynah Pantig
     * @param
     * @return
     */

    public function eventAriaLabel( $post )
    {

      if ( is_array( $post ) ) {
        $thisPost = $post;
        $post = (object) $thisPost;
      }

      $date = Utils::get_date_range( get_field( 'date_start', $post->id ), get_field( 'date_end', $post->id ) );

      $label = get_the_title( $post ) . '. ' . __( 'Event on' ) . ' ' . $date  . get_the_excerpt( $post ) . '. ' . __( 'Event at' ) . ' ' . get_field( 'location', $post->id );

      return $label;

    }


    /**
     * get single pagination
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $direction
     * @return
     */

    public function getSinglePagination( $direction )
    {

      switch ( $direction ) {
        case 'next':
          $post = get_next_post();
          $markup = '<a href="' . get_permalink( $post->ID ) . '" title="Go to the next page" class="pagination-nav pagination-nav--next">Next<i class="icon-arrow-right" aria-hidden="true"></i></a>';

          break;

        case 'prev':
          $post = get_previous_post();
          $markup = '<a href="' . get_permalink( $post->ID ) . '" title="Go to the previous page" class="pagination-nav pagination-nav--prev"><i class="icon-arrow-left" aria-hidden="true"></i>Previous</a>';

          break;

        default:
          # code...
          break;
      }

      if ( is_a( $post, 'WP_Post' ) )
      {
        return $markup;
      }

    }/* getSinglePagination() */

    /**
     * get the skills from the project post type and list
     *
     * @author Ynah Pantig
     * @package Project.php
     * @since 1.0
     * @param
     * @return $data
     */

    static public function getTaxonomy( $tax, $hideEmpty = false )
    {

      $data = array();
      $args = array(
        'hide_empty' => $hideEmpty,
        'taxonomy' => $tax,
      );

      $terms = new \WP_Term_Query( $args );

      foreach ( $terms->get_terms() as $term )
      {
        $data[ $term->slug ] = $term;
      }

      return $data;

    }/* getTaxonomy() */


    /**
     * get post terms
     *
     * @author Ynah Pantig
     * @param $postID, $taxonomy
     * @return
     */

    public function get_post_terms( $postID, $taxonomy )
    {

      $terms = get_the_terms( $postID, $taxonomy );
      return $terms;

    }


    /**
     * get twig comment
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $comment
     * @return
     */

    public function getTwigComment( $commentID )
    {

      $commentID = (int) $commentID;
      $comment = new \TimberComment( $commentID );

      $user = get_user_by( 'email', $comment->comment_author_email );
      $comment->comment_user_id = (string) $user->ID;

      return $comment;

    }/* getTwigComment() */


    /**
     * get latest community members
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    public function getLatestCommunity(  )
    {

      $users = Users::latestMembers();

      $latestNews = Post::getLatestPosts( array(
        'posts_per_page' => 1,
        'post_type' => 'post'
      ) );

      $latestSuccess = Post::getLatestPosts( array(
        'posts_per_page' => 1,
        'post_type' => 'success-story'
      ) );

      $data = array(
        $users[0],
        \Timber::get_post( $latestNews[0]->ID ),
        \Timber::get_post( $latestSuccess[0]->ID ),
        $users[1]
      );

      return $data;

    }/* getLatestCommunity() */


    /**
     * get latest news
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    static public function getLatestPosts( $args )
    {

      $query = new \WP_Query( $args );
      $data = $query->posts;

      return $data;

    }/* getLatestNews() */


    /**
     * get project filters
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    public function getProjectFilters( $type )
    {

      switch ( $type ) {
        case 'needs':
          $needs = static::getTaxonomy( 'need', true );
          $options = [];
          // $options = [
          //   'recent' => __( 'Most Recent Need' ),
          // ];

          foreach ( $needs as $item )
          {
            $options[ $item->slug ] = $item->name;
          }

          break;

        case 'stage':
          $stage = static::getTaxonomy( 'stage', true );

          $options = [
            'recent' => __( 'Most Recent' ),
          ];

          foreach ( $stage as $item )
          {
            $options[ $item->slug ] = __( 'Stage' ) . ': ' . $item->name;
          }

          break;

        default:
          # code...
          break;
      }

      return $options;

    }/* getProjectFilters() */


    /**
     * get social data
     *
     * @author Ynah Pantig
     * @param
     * @return
     */

    public function getProfileSocialData(  )
    {

      $data = array(
        array(
          'type' => 'Facebook',
          'class' => 'facebook',
          'label' => __( 'Like us on Facebook' ),
        ),
        array(
          'type' => 'Twitter',
          'class' => 'twitter',
          'label' => __( 'Follow us on Twitter' ),
        ),
        array(
          'type' => 'Instagram',
          'class' => 'instagram',
          'label' => __( 'Follow us on Instagram' ),
        ),
        array(
          'type' => 'Youtube',
          'class' => 'youtube',
          'label' => __( 'Subscribe to our channel on Youtube' ),
        ),
        array(
          'type' => 'Thingiverse',
          'class' => 'thingiverse',
          'label' => __( 'Follow us on Thingiverse' ),
        ),
        array(
          'type' => 'Github',
          'class' => 'github',
          'label' => __( 'Follow us on Github' ),
        ),
      );

      return $data;

    }/* getProfileSocialData() */


    /**
     * get image fields
     *
     * @author Ynah Pantig
     * @param $attachmentID
     * @return
     */

    public function getImageFields( $attachmentID )
    {

      $fields = get_fields( $attachmentID );
      return $fields;

    }


    /**
     * get the location and build the markup
     *
     * @author Ynah Pantig
     * @param
     * @return
     */

    public function getLocation(  )
    {

      $postID = get_the_ID();
      $geolocation = get_field( 'geolocation', $postID );
      $location = get_field( 'location', $postID );

      if ( empty( $geolocation ) ) {
        return $location;
      }

      $markup = '';
      $markup .= '<a href="https://www.google.com/maps/dir/current+location/' . $geolocation['lat'] . ',' . $geolocation['lng'] . '" target="_blank">' . $location . '</a>';

      return $markup;

    }

    /**
     * get project stage
     *
     * @author Ynah Pantig
     * @param
     * @return
     */

    public function getProjectStage( $terms )
    {

      $data = array();
      foreach ( $terms as $term ) {
        if ( $term->taxonomy == 'stage' ) {
          $data = [
            'label' => $term->name,
            'colour' => $term->slug,
          ];
        }
      }

      return $data;

    }


  } /* class Post() */
