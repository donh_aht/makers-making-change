<?php

  namespace MMC;

  class Utils
  {
    /**
     * Test variable
     */
    static public $testVar = true;


    private function __construct(){}


    /**
     * get the site's environment
     *
     * @author Ynah Pantig
     * @param
     * @return $data
     */

    static public function getEnv(  )
    {

      return (in_array(parse_url(home_url(), PHP_URL_HOST), ['makersmakingchange.com', 'www.makersmakingchange.com'])) ? 'prod' : 'dev';

    }

    /**
     * Determine if we're in debug mode or not
     */
    static public function isJsDebug()
    {
      $env = getenv('WP_ENV');
      if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG )
        return true;
      elseif( WP_ENV !== 'prod' && WP_ENV !== 'staging' )
        return true;
      else
        return false;

    }/* isJsDebug() */

    /**
     * Prints inline script to output the input parameter to the browser console.
     */
    public static function log($var)
    {
      $script = '<script>console.debug(' . json_encode($var) . ');</script>';
      if(function_exists('add_action')) {
        add_action('wp_print_footer_scripts',function() use($script) {
          echo $script;
        }, 9999);
        add_action('admin_print_footer_scripts',function() use($script) {
          echo $script;
        }, 9999);
      }else {
        echo $script;
      }

    }/* log() */


    /**
     * wrapper for the dump function
     */
    public static function pre($var, $label=null, $echo=true, $use_print_r=false)
    {
      static::dump($var, $label, $echo, $use_print_r);

    }/* pre() */

    /**
     * Debug helper function.  This is a wrapper for var_dump() that adds
     * the <pre /> tags, cleans up newlines and indents, and runs
     * htmlentities() before output.
     */
    public static function dump($var, $label=null, $echo=true, $use_print_r=false)
    {
      // format the label
      $label = ($label===null) ? '' : rtrim($label) . ' ';

      // var_dump the variable into a buffer and keep the output
      ob_start();
      if($use_print_r) {
        print_r($var);
      }else {
        var_dump($var);
      }
      $output = ob_get_clean();

      // neaten the newlines and indents
      $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
      if(!$use_print_r) $output = preg_replace("/  /m", "    ", $output);
      // if (self::getSapi() == 'cli') {
      //   $output = PHP_EOL . $label
      //       . PHP_EOL . $output
      //       . PHP_EOL;
      // } else {
        if(!extension_loaded('xdebug')) {
          $output = htmlspecialchars($output, ENT_QUOTES);
        }

        $output = '<pre>'
          . $label
          . $output
          . '</pre>';
      // }

      if ($echo)
        echo($output);

      return $output;

    }/* dump() */

    /**
     * file get contents with authorization
     *
     * @author Ynah Pantig
     * @package Utils.php
     * @since 1.0
     * @param
     * @return $data
     */

    static public function file_get_contents( $data )
    {

      if ( is_array( $data ) )
      {
        $data = $data[ 'url' ];
      }

      $ext = pathinfo( $data, PATHINFO_EXTENSION );

      if ( $ext == 'svg' )
      {

        $file = static::getAbspathUrl( $data );

        if ( defined( 'AUTH_USER' ) && defined( 'AUTH_PASS' ) )
        {

          $username = AUTH_USER;
          $password = AUTH_PASS;

          $context = stream_context_create(array(
            'http' => array(
              'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
          ));

          $return = file_get_contents( $file, false, $context );

        } else
        {
          $return = file_get_contents( $file, FILE_USE_INCLUDE_PATH );
        }

      }
      else
      {
        $return = '<img src="' . $data . '" />';
      }

      return $return;

    }/* file_get_contents() */

    /**
     * get the lat and long from google.
     * usage of Yahoo (function directly above this) is no longer working
     * where.yahooapis.com GeoLocation API has been deprecated
     *
     * @author Ynah Pantig
     * @package Utils.php
     * @since 1.0
     * @param $city, $state
     * @return $data
     */

    static public function googleGeolocate( $city = '', $postalCode = '', $coordinates = '' )
    {

      if ( empty( $postalCode ) && empty( $city ) && empty( $coordinates ) )
      {
        return;
      }

      $apiKey = 'AIzaSyBhxeSeyrYkPN6SJNI4VoOXMRJ98qVtJGg';

      global $wpdb;
      $tableName = 'mmc_lat_lon';

      if ( !empty( $city ) || !empty( $postalCode ) )
      {
        $query = "SELECT * FROM " . $tableName . " WHERE city=%s AND postal_code=%s";
        $queryParsed = $wpdb->prepare( $query, $city, $postalCode );

        if ( !empty( $postalCode ) )
        {
          $searchAddress = $postalCode;
        } else
        {
          $searchAddress = $city;
        }

        $search = 'address=' . urlencode( $searchAddress );
      }

      if ( !empty( $coordinates ) )
      {
        $query = "SELECT * FROM " . $tableName . " WHERE lat=%s AND lon=%s";
        $queryParsed = $wpdb->prepare( $query, $coordinates['lat'], $coordinates['lon'] );

        $search = 'latlng=' . implode( ',', $coordinates );
      }

      // get the results
      $results = $wpdb->get_results( $queryParsed );
      $rows = $wpdb->num_rows;

      if ( $rows <= 0 )
      {

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?' . $search . '&key=' . $apiKey;
        $request = wp_remote_get( $url );
        $response = json_decode( $request[ 'body' ] );

        if ( !empty( $response ) && !empty( $response->results ) )
        {
          $results = $response->results[0];

          if( isset( $results->geometry->location->lng ) && isset( $results->geometry->location->lat ) )
          {
            if ( empty( $city ) ) {
              foreach ( $results->address_components as $address ) {
                if ( in_array( 'locality', $address[ 'types' ] ) )
                {
                  $city = $address[ 'long_name' ];
                }
              }
            }

            $latlon = array(
              'city' => $city,
              'postal_code' => $postalCode,
              'lat' => $results->geometry->location->lat,
              'lon' => $results->geometry->location->lng,
            );

            $format = array( '%s', '%s', '%s', '%s' );
            $wpdb->insert( $tableName, $latlon, $format );
          }
        }

      } else
      {

        $result = $results[0];

        $latlon = array(
          'city' => $result->city,
          'postal_code' => $postalCode,
          'lat' => $result->lat,
          'lon' => $result->lon,
        );

      }

      if ( !empty( $latlon ) )
      {
        return $latlon;
      }

    }/* googleGeolocate() */


    /**
     * get the abspath url of the file
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $file
     * @return
     */

    static public function getAbspathUrl( $file )
    {
      $path = parse_url($file, PHP_URL_PATH);
      $file = ABSPATH . $path;

      return $file;

    }/* getAbspathUrl() */


    /**
     * convert to numeric
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $num
     * @return
     */

    static public function convertToNumeric( $num )
    {

      switch ( $num ) {
        case 'one':
          $num = 1;
          break;

        case 'two':
          $num = 2;
          break;

        case 'three':
          $num = 3;
          break;

        default:
          $num = 0;
          break;
      }

      return $num;

    }/* convertToNumeric() */

    /**
     * Create the image attachment and return the new media upload id.
     *
     * @since 1.0.0
     * @see http://codex.wordpress.org/Function_Reference/wp_insert_attachment#Example
     */
    static public function getAttachmentIDFromUrl( $image_url, $parent_post_id = null ) {

      if( !isset( $image_url ) )
      {
        return false;
      }

      // Cache info on the wp uploads dir
      $wp_upload_dir = wp_upload_dir();
      // get the file path
      $path = parse_url( $image_url, PHP_URL_PATH );

      // File base name
      $file_base_name = basename( $image_url );

      // Full path
      if( site_url() != home_url() ) {
        $home_path = dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
      } else {
        $home_path = dirname( dirname( dirname( dirname( __FILE__ ) ) ) );
      }

      $home_path = untrailingslashit( $home_path );
      $uploaded_file_path = $home_path . $path;
      // Check the type of file. We'll use this as the 'post_mime_type'.
      $filetype = wp_check_filetype( $file_base_name, null );

      // error check
      if( !empty( $filetype ) && is_array( $filetype ) ) {
        // Create attachment title
        $post_title = preg_replace( '/\.[^.]+$/', '', $file_base_name );

        // Prepare an array of post data for the attachment.
        $attachment = array(
          'guid'           => $wp_upload_dir['url'] . '/' . basename( $uploaded_file_path ),
          'post_mime_type' => $filetype['type'],
          'post_title'     => esc_attr( $post_title ),
          'post_content'   => '',
          'post_status'    => 'inherit'
        );

        // Set the post parent id if there is one
        if( !is_null( $parent_post_id ) )
          $attachment['post_parent'] = $parent_post_id;
        // Insert the attachment.
        $attach_id = wp_insert_attachment( $attachment, $uploaded_file_path );
        //Error check
        if( !is_wp_error( $attach_id ) ) {
          //Generate wp attachment meta data
          if( file_exists( ABSPATH . 'wp-admin/includes/image.php') && file_exists( ABSPATH . 'wp-admin/includes/media.php') ) {
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );
            $attach_data = wp_generate_attachment_metadata( $attach_id, $uploaded_file_path );
            wp_update_attachment_metadata( $attach_id, $attach_data );
          } // end if file exists check
        } // end if error check

        return $attach_id;

      } else {
        return false;
      } // end if $$filetype
    } // end function get_image_id


    /**
     * get user avatar
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $type, $value
     * @return
     */

    static public function get_user_avatar_by( $userID )
    {

      $avatarUrl = get_avatar_url( $userID, array(
        'size' => 120
      ));

      $profileImage = get_field( 'user_profile_image', 'user_' . $userID );
      if ( !empty( $profileImage ) ) {
        $avatarUrl = $profileImage;
      }

      return $avatarUrl;

    }/* get_user_avatar_by() */


    /**
     * get the date range based on the start and end dates
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param
     * @return
     */

    static public function get_date_range( $start, $end )
    {

      $startMonth = date( 'F', strtotime( $start ) );
      $startDay = date( 'j', strtotime( $start ) );
      $startYear = date( 'Y', strtotime( $start ) );

      $endMonth = date( 'F', strtotime( $end ) );
      $endDay = date( 'j', strtotime( $end ) );
      $endYear = date( 'Y', strtotime( $end ) );

      $builtRange = '';

      // different months, same year
      if ( $startMonth != $endMonth && $startYear == $endYear )
      {
        // Format: January 1 - February 1, 2018
        $builtRange = sprintf( '%s %d - %s %d, %d', $startMonth, $startDay, $endMonth, $endDay, $endYear );
      }

      // different years, different months
      if ( $startYear != $endYear && $startMonth != $endMonth )
      {
        // Format: December 29, 2018 - January 2, 2019
        $builtRange = sprintf( '%s %d, %d - %s %d, %d', $startMonth, $startDay, $startYear, $endMonth, $endDay, $endYear );
      }

      // same months, same years
      if ( $startMonth == $endMonth && $startYear == $endYear )
      {
        // Format: January 1 - 3, 2018
        $builtRange = sprintf( '%s %d - %d, %d', $startMonth, $startDay, $endDay, $endYear );
      }

      // same month, day, and year
      if ( $startMonth == $endMonth && $startDay == $endDay && $startYear == $endYear )
      {
        // Format: January 1, 2018
        $builtRange = sprintf( '%s %d, %d', $endMonth, $endDay, $endYear );
      }

      // all else fails
      if ( $builtRange == '' )
      {
        // Format: January 1, 2018 - February 1, 2019
        $builtRange = sprintf( '%s %d, %d - %s %d, %d', $startMonth, $startDay, $startYear, $endMonth, $endDay, $endYear );
      }

      if ( $start == '' )
      {
        $builtRange = sprintf( '%s %d, %d', $endMonth, $endDay, $endYear );
      }

      if ( $end == '' )
      {
        $builtRange = sprintf( '%s %d, %d', $startMonth, $startDay, $startYear );
      }

      return $builtRange;

    }/* get_date_range() */


    /**
     * truncate the text to a certain number
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $length = 30
     * @return
     */

    static public function truncate_text( $text, $length = 15 )
    {

      if ( strlen( $text ) > $length )
      {
        $text = wp_trim_words( $text, $length, '...' );
      }

      return $text;

    }/* truncate_text() */


    /**
     * decode json
     *
     * @author Ynah Pantig
     * @package
     * @since 1.0
     * @param $json
     * @return
     */

    static public function json_decode( $json )
    {

      error_log( $json );
      return json_decode( $json, true );

    }/* parse_json() */


    /**
     * clean the username generated for Gravity Forms
     * to remove special characters
     *
     * @author Ynah Pantig
     * @param
     * @return
     */

    public static function cleanString( $string, $replace = '' )
    {

       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9\-]/', $replace, $string); // Removes special chars.

    }


    /**
     * Recursive function to generate a unique username.
     *
     * If the username already exists, will add a numerical suffix which will increase until a unique username is found.
     *
     * @param string $username
     *
     * @return string The unique username.
     */
    static public function generate_unique_username( $username ) {
      static $i;
      if ( null === $i ) {
        $i = 1;
      } else {
        $i++;
      }
      if ( ! username_exists( $username ) ) {
        return $username;
      }
      $new_username = sprintf( '%s-%s', $username, $i );
      if ( ! username_exists( $new_username ) ) {
        return $new_username;
      } else {
        // return call_user_func( __FUNCTION__, $username );
        return static::generate_unique_username( $username );
      }
    }

    static public function getArchiveUrl( $postType ) {

      return get_post_type_archive_link( $postType );

    }

  }/* class Utils */
