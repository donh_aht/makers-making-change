<?php

namespace MMC;

/**
 * Social
 */
class CustomPostTypes
{

  protected $postTypes = [
    'project' => [
      'labels'              => [],
      'has_archive'         => true,
      'menu_icon'           => 'dashicons-awards',
      'supports'            => array(
        'title',
        'editor',
        'revisions',
        'excerpt',
        'thumbnail',
        'author',
        'comments',
      )
    ],
    'request' => [
      'labels'              => [],
      'menu_icon'           => 'dashicons-warning',
      'publicly_queryable'  => false,
      'supports'            => array(
        'title',
        'editor',
        'revisions',
        'author',
        'custom-fields'
      )
    ],
    'idea' => [
      'labels'              => [],
      'menu_icon'           => 'dashicons-lightbulb',
      'publicly_queryable'  => false,
      'exclude_from_search' => true,
      'supports'            => array(
        'title',
        'editor',
        'revisions',
        'author'
      )
    ],
    'event' => [
      'labels'              => [],
      'has_archive'         => true,
      'menu_icon'           => 'dashicons-tickets-alt',
      'supports'            => array(
        'title',
        'editor',
        'revisions',
        'excerpt',
        'thumbnail',
      )
    ],
    'success-story' => [
      'plural'              => 'Success Stories',
      'labels'              => [],
      'has_archive'         => true,
      'menu_icon'           => 'dashicons-megaphone',
      'supports'            => array(
        'title',
        'editor',
        'revisions',
        'thumbnail',
      )
    ],
    'testimonial' => [
      'labels'              => [],
      'menu_icon'           => 'dashicons-format-quote',
      'publicly_queryable'  => false,
      'exclude_from_search' => true,
      'supports'            => array(
        'title',
        'editor',
        'revisions',
        'thumbnail',
      )
    ],
  ];

  function __construct(  )
  {

    // register accommodation post type
    add_action( 'init', array( &$this, 'init__registerPostTypes' ), 1 );

    // rename the default post type (post) to blog
    add_action( 'admin_menu', array( &$this, 'admin_menu__renameDefaultPostPostType' ) );
    add_action( 'init', array( &$this, 'init__renameDefaultPostPostType' ) );

    add_filter( 'enter_title_here', array( &$this, 'enter_title_here__changePlaceholder' ) );

    add_action( 'init', array( &$this, 'init__changeThumbnailLabels' ) );

  }/* __construct() */

  protected $defaultPostTypeArgs = [
    'public' => true,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'hierarchical' => false,
    'has_archive' => false,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'capability_type' => 'post',
  ];

  // Default arguments to use for register_post_type
  protected $imageLabels = [
    'default' => array(
      'featured_image' => 'Grid Image',
      'set_featured_image' => 'Set grid image',
      'remove_featured_image' => 'Remove grid image',
      'use_featured_image' => 'Use as grid image',
    ),
    'testimonial' => array(
      'featured_image' => 'Testimonial Image',
      'set_featured_image' => 'Set testimonial image',
      'remove_featured_image' => 'Remove testimonial image',
      'use_featured_image' => 'Use as testimonial image',
    ),
  ];

  /**
   * change labels
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function init__changeThumbnailLabels(  )
  {

    global $wp_post_types;

    foreach ( $wp_post_types as $key => $postType )
    {
      $postTypeLabels = &$postType->labels;

      if ( !empty( $this->imageLabels ) ) {

        $postTypeSlug = $postType->name;

        if ( is_array( $this->imageLabels ) && array_key_exists( $postTypeSlug, $this->imageLabels ) ) {
          $labels = $this->imageLabels[ $postTypeSlug ];
        } else {
          $labels = $this->imageLabels[ 'default' ];
        }

        foreach ( $labels as $key => $label )
        {
          $postTypeLabels->{$key} = __( $label );
        }
      }
    }

  }/* init__changeThumbnailLabels() */


  /**
   * change the "enter title here" placeholder
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function enter_title_here__changePlaceholder( $title )
  {

     $screen = get_current_screen();

     switch ( $screen->post_type ) {
       case 'testimonial':

         $title = 'Enter name and position here (ex. Peter Dinklage, Maker)';

         break;

       default:
         $title = 'Enter title here';
         break;
     }

     return $title;


  }/* enter_title_here__changePlaceholder() */


  /**
   * change the label of the native "Post" post type into "Blog"
   * just in the sidebar
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function admin_menu__renameDefaultPostPostType() {

    global $menu, $submenu;

    $defaultName = 'Blog';

    echo '';

  }

  public function init__renameDefaultPostPostType() {

    global $wp_post_types;

    $defaultName = 'Blog';

    $labels = &$wp_post_types['post']->labels;
    $labels->name = $defaultName;
    $labels->singular_name = $defaultName;
    $labels->menu_name = $defaultName;
    $labels->name_admin_bar = $defaultName;

  }

  /**
   * register accommodation post type
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function init__registerPostTypes()
  {

    foreach ( $this->postTypes as $slug => $args ) {
      $this->dynamicRegisterPostType( $slug, $args );
    }

  }/* init__registerPostTypes() */


  /**
   * dynamically create the post types based on the array
   *
   * @author Ynah Pantig
   * @package CustomPostTypes.php
   * @since 1.0
   * @param $slug, $args
   * @return $data
   */

  public function dynamicRegisterPostType( $slug, $args )
  {

    $postName = str_replace( '-', ' ', $slug );
    $singular = ucwords( $postName );

    if ( !empty( $args[ 'plural' ] ) )
    {
      $plural = $args[ 'plural' ];
    }
    else
    {
      $plural = ucwords( $postName ) . 's';
    }

    if ( !empty( $args[ 'singular' ] ) )
    {
      $singular = $args[ 'singular' ];
    }

    $labels = array(
      'name'                => __( $plural, 'base-theme' ),
      'singular_name'       => __( $singular, 'base-theme' ),
      'add_new'             => _x( 'Add New ' . $singular, 'base-theme' ),
      'add_new_item'        => __( 'Add New ' . $singular, 'base-theme' ),
      'edit_item'           => __( 'Edit ' . $singular, 'base-theme' ),
      'new_item'            => __( 'New ' . $singular, 'base-theme' ),
      'view_item'           => __( 'View ' . $singular, 'base-theme' ),
      'search_items'        => __( 'Search ' . $plural, 'base-theme' ),
      'not_found'           => __( 'No ' . $plural . ' found', 'base-theme' ),
      'not_found_in_trash'  => __( 'No ' . $plural . ' found in Trash', 'base-theme' ),
      'parent_item_colon'   => __( 'Parent :' . $singular, 'base-theme' ),
      'menu_name'           => __( $plural, 'base-theme' ),
    );

    if ( !empty( $args[ 'labels' ] ) )
    {
      $labels = array_merge( $labels, $args[ 'labels' ] );
    }

    $args[ 'labels' ] = $labels;

    $args = array_merge( $this->defaultPostTypeArgs, $args);

    register_post_type( $slug, $args );

  }/* dynamicRegisterPostType() */

} /* class CustomPostTypes() */
