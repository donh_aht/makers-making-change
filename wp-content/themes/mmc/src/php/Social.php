<?php

namespace MMC;

class Social
{
  public function __construct() {

    add_filter( 'wpseo_opengraph_image', array( &$this, 'wpseo_opengraph_image__updateOGImage' ) );

  }


  /**
   * set the default image
   *
   * @author Ynah Pantig
   * @param
   * @return
   */

  public function wpseo_opengraph_image__updateOGImage( $image )
  {

    $postID = get_the_ID();

    // get the yoast open graph imageset for this page specifically
    $yoastImg = get_post_meta( $postID, '_yoast_wpseo_opengraph-image', true );

    // get the default from the options page
    $defaultImage = get_field( 'default_seo_image', 'options' );

    if ( empty( $defaultImage ) ) {

      if ( !empty( $yoastImg ) ) {
        $image = $yoastImg;
      } else {
        $image = $GLOBALS[ 'wpseo_og' ]->options[ 'og_default_image' ];
      }

    } else {

      $defaultImage = wp_get_attachment_image_src( $defaultImage, 'full' );
      $image = $defaultImage[0];

    }

    return $image;

  }

}
