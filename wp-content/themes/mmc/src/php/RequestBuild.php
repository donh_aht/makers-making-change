<?php

namespace MMC;

class RequestBuild
{
  /**
   * User data:
   * - First Name
   * - Last Name
   * - Nickname -> {First Name}
   * - Display Name -> {First Name} {Last Name}
   * - Roles
   *
   * User Meta:
   * - Location
   *
   * ACF:
   * - Skills (user_skills)
   */

  private $formID = 1;

  // fields ids
  private $titleFieldID = 1;
  private $messageFieldID = 2;
  private $emailFieldID = 3;
  private $nameFieldID = 5;
  private $geolocationFieldID = 6;
  private $projectTitleFieldID = 7;

  private $fieldUserMetaMap = array();

  public $titleMetaKey = 'post_title';
  public $messageMetaKey = 'post_content';
  public $emailMetaKey = 'user_email';
  public $nameMetaKey = 'display_name';
  public $geolocationMetaKey = 'geolocation';

  public function __construct()
  {
    // field IDs <--> User meta map
    $this->fieldUserMetaMap[$this->titleFieldID] = $this->titleMetaKey;
    $this->fieldUserMetaMap[$this->messageFieldID] = $this->messageMetaKey;
    $this->fieldUserMetaMap[$this->emailFieldID] = $this->emailMetaKey;
    $this->fieldUserMetaMap[$this->nameFieldID] = $this->nameMetaKey;
    $this->fieldUserMetaMap[$this->geolocationFieldID] = $this->geolocationMetaKey;

    // add_filter( 'gform_validation_' . $this->formID, array( '\\MMC\\GravityForms', 'gform_validation__redirectToLogin' ) );
    add_filter( 'gform_pre_render_' . $this->formID, array( '\\MMC\\GravityForms', 'gform_validation__redirectToLogin' ) );

    add_filter( 'gform_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
    add_filter( 'gform_pre_validation_' . $this->formID, array( &$this, 'gform__populateFields' ) );

    // Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
    add_filter( 'gform_admin_pre_render_' . $this->formID, array( &$this, 'gform__populateFields' ) );

    // Note: this will allow for the labels to be used during the submission process in case values are enabled
    add_filter( 'gform_pre_submission_filter_' . $this->formID, array( &$this, 'gform__populateFields' ), 10 );

    add_action( 'gform_after_submission_' . $this->formID, array( &$this, 'gform_after_submission__setProjectMeta' ), 10, 2 );

    add_filter( 'publish_request', array(&$this, 'wp_publish_request_hook'), 99, 2 );
  } /* __construct() */

  // function gform_validation( $validation_result ) {

  //   $userID = get_current_user_id();
  //   if (!$userID) {
  //     wp_redirect(site_url('/login/'));
  //     exit();
  //   }

  //   return $validation_result;
  // }

  /**
   * populate the project title field
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param
   * @return
   */

  public function gform__populateFields( $form )
  {

    $userID = get_current_user_id();
    if (!$userID) {
      $form['requireLoginMessage'] = GravityForms::promptUserLogin();
      return $form;
    }

    $userObj = new \WP_User($userID);
    $userData = array( 'ID' => $userID );
    $posts = array();

    foreach ( $form['fields'] as &$field )
    {

      switch ( $field->id ) {
        case $this->titleFieldID:

          $args = array(
            'posts_per_page' => -1,
            'post_type' => 'project',
            'post_status' => 'publish',
          );

          $posts = get_posts( $args );
          $choices = array();
          $field->choices = array();

          foreach ( $posts as $post )
          {
            $choices[] = array(
              'text' => $post->post_title,
              'value' => $post->ID,
            );
          }

          $field->choices = $choices;
          $field->placeholder = 'Project Title';

          break;

        case $this->emailFieldID:

          $field->defaultValue = $userObj->data->user_email;

          break;

        case $this->nameFieldID:

          $field->defaultValue = $userObj->data->display_name;

          break;

        case $this->geolocationFieldID:

          $field->defaultValue = json_encode( get_usermeta( $userID, 'geolocation' ) );

          break;

        case $this->projectTitleFieldID:

          // Utils::pre( $_REQUEST );
          // if ( !empty( $posts ) ) {
          //   # code...
          // }
          // $field->defaultValue = json_encode( get_usermeta( $userID, 'geolocation' ) );

          break;

        default:
          # code...
          break;
      }
    }

    return $form;

  }/* gform_field__projectTitle() */


  /**
   * set the project data outside of the native fields from WP
   *
   * @author Ynah Pantig
   * @package
   * @since 1.0
   * @param $entry, $form
   * @return
   */

  public function gform_after_submission__setProjectMeta( $entry, $form )
  {

    /**
     * $entry
     * 1 = project title dropdown
     * 2 = message
     * 3 = user email
     * 4 = user name
     */

    $projectTitle = rgar( $entry, 1 );
    $postTitle = get_the_title( $entry[ 1 ] );

    $postData = array(
      'post_title' => __( 'Request for' ) . ' ' . $postTitle,
      'post_type' => 'request',
      'post_content' => rgar( $entry, 2 ),
      'post_status' => 'publish',
    );

    $postID = wp_insert_post( $postData );
    $user = get_user_by( 'email', rgar( $entry, 3 ) );

    $location = get_field( 'user_location', 'user_' . $user->ID );
    $latlon = Utils::googleGeolocate( $location );

    if ( !empty( $latlon ) )
    {
      update_field( 'latitude', $latlon[ 'lat' ], $postID );
      update_field( 'longitude', $latlon[ 'lon' ], $postID );
    }

    update_field( 'location', $location, $postID );
    update_field( 'city', $latlon[ 'city' ], $postID );
    update_field( 'status', 'none', $postID );
    update_post_meta( $postID, 'project_id', $projectTitle );

    update_post_meta($postID, '_gform-form-id', $form['id']);
    update_post_meta($postID, '_gform-entry-id', $entry['id']);

    ForumIntegration::RequestBuild($postID);
  }/* gform_after_submission__setProjectMeta() */

  public function wp_publish_request_hook($postID, $postObj)
  {
    ForumIntegration::RequestBuild($postId, $postObj);
  }
} /* class ClassName() */
