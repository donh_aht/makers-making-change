<?php

namespace MMC;

class Roles
{

  private function __construct(){}

  public static $excludedRoles = array(
    'administrator',
    'author',
    'contributor',
    'editor',
    'subscriber',
    'wpseo_editor',
    'wpseo_manager',
  );

  private static $mmcRolesCache;

  public static function getMMCRoles() {
    if (is_null(self::$mmcRolesCache)) {
      global $wp_roles;

      $all_roles = $wp_roles->roles;
      $editable_roles = apply_filters('editable_roles', $all_roles);

      self::$mmcRolesCache = array();

      foreach ($editable_roles as $key => $role) {
        if (in_array($key, static::$excludedRoles)) {
          continue;
        }

        self::$mmcRolesCache[$key] = $role;
      }
    }

    return self::$mmcRolesCache;
  }

  public static function getUserMMCRoles($userID) {
    $user = get_userdata($userID);
    $userRoles = (array) $user->roles;
    $mmcRoles = array_keys(static::getMMCRoles());
    return array_intersect($userRoles, $mmcRoles);
  }

}
