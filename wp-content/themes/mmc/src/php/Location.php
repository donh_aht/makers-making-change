<?php

namespace MMC;

class Location
{

  private function __construct(){}

  public static $addressComponentTypesToSave = array(
    'locality' => 'city',
    'administrative_area_level_1' => 'state',
    'country' => 'country',
  );

  private static function prepareGeolocationMetaData($data) {
    // save geolocation data object for later use
    $metaData = array('geolocation' => $data);

    // save coordinates
    $location = $data['geometry']['location'];
    $metaData['geolocation_lat'] = $location['lat'];
    $metaData['geolocation_lng'] = $location['lng'];

    // save formatted address
    $metaData['geolocation_formatted_address'] = $data['formatted_address'];

    // loop through address_components
    foreach ($data['address_components'] as $comp) {
      foreach (static::$addressComponentTypesToSave as $type => $metaKey) {
        if (!in_array($type, $comp['types'])) {
          continue;
        }

        $metaData['geolocation_' . $metaKey] = $comp['long_name'];
      }
    }

    return $metaData;
  }

  public static function updateUserGeolocationData($userID, $data) {
    $metaData = static::prepareGeolocationMetaData($data);
    foreach ($metaData as $key => $value) {
      update_user_meta($userID, $key, $value);
    }
  }

  public static function updatePostGeolocationData($postID, $data) {
    $metaData = static::prepareGeolocationMetaData($data);
    foreach ($metaData as $key => $value) {
      update_post_meta($postID, $key, $value);
    }
  }
}
