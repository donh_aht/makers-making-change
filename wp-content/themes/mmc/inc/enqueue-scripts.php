<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package gssi
 */

function gssi_scripts() {
    $ver = '2.0';
    $jQueryVer = '3.4.1';
	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'gssi-main', get_stylesheet_directory_uri() . '/gs-dist/css/main.css', array(), $ver );
	wp_enqueue_style( 'gssi-style', get_stylesheet_uri(), array(), $ver );

	// Enqueue scripts
	wp_dequeue_script('jquery');
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/'.$jQueryVer.'/jquery.min.js', true, $jQueryVer);
    wp_enqueue_script('jquery');
    
	wp_enqueue_script( 'picturefill', get_template_directory_uri() . '/gs-dist/js/lib/picturefill-2.3.1.min.js', array(), $ver, true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/gs-dist/js/lib/slick.min.js', array(), $ver, true );
	wp_enqueue_script( 'enquire', get_template_directory_uri() . '/gs-dist/js/lib/enquire.min.js', array(), $ver, true );
	wp_enqueue_script( 'gssi-site-functions', get_template_directory_uri() . '/gs-dist/js/site-functions.min.js', array('jquery','slick'), $ver, true );

	// Add the comment-reply library on pages where it is necessary
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Icons
	wp_enqueue_style( 'style', get_template_directory_uri()."/gs-dist/fontello.css" );
  	wp_enqueue_style( 'icon', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'gssi_scripts' );