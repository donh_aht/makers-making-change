<?php

  // Don't allow the theme to be loaded directly
  if ( ! function_exists( 'add_action' ) ) {
    echo 'Please enable this theme from the WordPress admin area.';
    exit;
  }

  $rootDir = dirname( dirname( dirname( dirname( __FILE__ ) ) ) );

  // Initialize Composer Autoload
  if ( file_exists( $rootDir . '/vendor/autoload.php' ) )
    require_once( $rootDir . '/vendor/autoload.php' );

  // load environment variables
  if ( file_exists( $rootDir . '/.env' ) )
    Dotenv::load( $rootDir );

  // Initialize plugin
  $version = '0.0.1';
  $bootstrapClass = 'MMC\\Bootstrap';

  if( class_exists( $bootstrapClass ) )
    new $bootstrapClass( __FILE__, $version );


/**
 * Enqueue scripts
 */
require_once( 'inc/enqueue-scripts.php' );