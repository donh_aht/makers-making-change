function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function getUserData() {
  // check if user data cookie exists
  var userDataCookie = getCookie('mmc_user_data');
  if (!userDataCookie || userDataCookie === '') {
    return null;
  }

  // check user data is properly formatted
  var userDataArr = decodeURIComponent(userDataCookie).split('|');
  if (!userDataArr || userDataArr.length === 0) {
    return null;
  }

  // check user data has session expiration time
  var expirationUtc = parseInt(userDataArr[1], 10) * 1000;
  if (!expirationUtc || isNaN(expirationUtc)) {
    return null;
  }

  // check if session has expired
  if (expirationUtc < new Date().getTime()) {
    return null;
  }

  var userDataArr = userDataArr.slice(3);

  // return user data
  return {
    userLogin: userDataArr[0],
    displayName: userDataArr[1],
    avatarUrl: userDataArr[2],
    roles: (userDataArr[3] || '').split(','),
    email: userDataArr[4],
    userID: userDataArr[5],
    profileCompleted: userDataArr[6],
    firstName: userDataArr[7],
    lastName: userDataArr[8],
  };
}

function setLoggedInClasses(userData) {
  // set logged-in class
  var classes = ' logged-in';

  // set classes for each user role
  for (var i = 0; i < userData.roles.length; i++) {
    var role = userData.roles[i];
    if (role && role !== '') {
      classes += ' logged-in-' + role;
    }
  }

  // add classes to <html> element
  document.documentElement.className += classes;
}

function loginCheckInit() {
  // check user data is available
  var userData = getUserData();
  if (!userData) {
    document.documentElement.className += ' logged-out';
    return;
  }

  // cache user data object
  window.userData = userData;

  // edit page for logged in user
  setLoggedInClasses(userData);
}

loginCheckInit();
