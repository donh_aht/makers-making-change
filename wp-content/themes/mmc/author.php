<?php

  /**
   * The template for displaying Author Archive pages
   *
   * @package  alebiavati/gestaltbologna.it
   * @since    1.0.0
   */

  global $wp_query;

  if ( isset( $wp_query->query_vars['author'] ) )
  {
    $author = new TimberUser( $wp_query->query_vars['author'] );
    $context['author'] = $author;
    $context['title'] = 'Author Archives: ' . $author->name();
  }

  $context['posts'] = Timber::get_posts(false, 'MMC\\Post');
  $context['pagination'] = Timber::get_pagination();

  \MMC\Template::render(array('author.twig', 'archive.twig'), $context );
