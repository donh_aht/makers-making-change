<?php

$post = Timber::get_post('MMC\\Post');
$postID = get_the_ID();
$context['post'] = $post;

$commentList = get_comments( array(
  'post_id' => $postID,
  'status' => 'approve',
));

$successStories = get_field( 'success_stories', $postID );

$context[ 'success_stories' ] = $successStories;
$context[ 'comments_list' ] = $commentList;
$context[ 'comments_open' ] = comments_open() && post_type_supports( get_post_type(), 'comments' );
$context[ 'comment_form_args' ] = array(
  'label_submit' => 'Submit',
  'comment_field' => '<p class="comment-form-comment"><label for="comment" id="review">' . _x( 'Review', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" aria-labelledby="review"></textarea></p>',
  'title_reply' => __( 'Add a review' ),
);

if ( !post_password_required( $post->ID ) ) {
  \MMC\Template::render( array( 'comments.twig' ), $context );
}
