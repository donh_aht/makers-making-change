<?php
  /**
   * This plugin changhes the regular WP cookie names so that WP Engine's cache
   * system is tricked into serving cached pages to logged-in users.
   */

  $cookie_prefix = 'mmc';

  /**
   * Used to guarantee unique hash cookies
   *
   * @since 1.5.0
   */
  if ( !defined( 'COOKIEHASH' ) ) {
    $siteurl = get_site_option( 'siteurl' );
    if ( $siteurl )
      define( 'COOKIEHASH', md5( $siteurl ) );
    else
      define( 'COOKIEHASH', '' );
  }

  /**
   * @since 2.0.0
   */
  if ( !defined('USER_COOKIE') )
    define('USER_COOKIE', $cookie_prefix . 'user_' . COOKIEHASH);

  /**
   * @since 2.0.0
   */
  if ( !defined('PASS_COOKIE') )
    define('PASS_COOKIE', $cookie_prefix . 'pass_' . COOKIEHASH);

  /**
   * @since 2.5.0
   */
  if ( !defined('AUTH_COOKIE') )
    define('AUTH_COOKIE', $cookie_prefix . '_' . COOKIEHASH);

  /**
   * @since 2.6.0
   */
  if ( !defined('SECURE_AUTH_COOKIE') )
    define('SECURE_AUTH_COOKIE', $cookie_prefix . '_sec_' . COOKIEHASH);

  /**
   * @since 2.6.0
   */
  if ( !defined('LOGGED_IN_COOKIE') )
    define('LOGGED_IN_COOKIE', $cookie_prefix . '_logged_in_' . COOKIEHASH);

  /**
   * @since 2.3.0
   */
  if ( !defined('TEST_COOKIE') )
    define('TEST_COOKIE', $cookie_prefix . '_test_cookie');
